vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xil_defaultlib

vmap xil_defaultlib questa_lib/msim/xil_defaultlib

vlog -work xil_defaultlib -64 "+incdir+../../../ipstatic" "+incdir+../../../ipstatic" \
"../../../../loopman.srcs/sources_1/ip/clkGen/clkGen_clk_wiz.v" \
"../../../../loopman.srcs/sources_1/ip/clkGen/clkGen.v" \


vlog -work xil_defaultlib \
"glbl.v"

