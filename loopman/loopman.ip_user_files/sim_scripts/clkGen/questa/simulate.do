onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib clkGen_opt

do {wave.do}

view wave
view structure
view signals

do {clkGen.udo}

run -all

quit -force
