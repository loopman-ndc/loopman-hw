vlib work
vlib activehdl

vlib activehdl/xil_defaultlib

vmap xil_defaultlib activehdl/xil_defaultlib

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../ipstatic" "+incdir+../../../ipstatic" \
"../../../../loopman.srcs/sources_1/ip/clkGen/clkGen_clk_wiz.v" \
"../../../../loopman.srcs/sources_1/ip/clkGen/clkGen.v" \


vlog -work xil_defaultlib \
"glbl.v"

