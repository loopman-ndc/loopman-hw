vlib work
vlib activehdl

vlib activehdl/xil_defaultlib

vmap xil_defaultlib activehdl/xil_defaultlib

vlog -work xil_defaultlib  -v2k5 \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/clocking/mig_7series_v4_1_clk_ibuf.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/clocking/mig_7series_v4_1_infrastructure.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/clocking/mig_7series_v4_1_iodelay_ctrl.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/clocking/mig_7series_v4_1_tempmon.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_arb_mux.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_arb_row_col.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_arb_select.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_bank_cntrl.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_bank_common.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_bank_compare.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_bank_mach.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_bank_queue.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_bank_state.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_col_mach.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_mc.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_rank_cntrl.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_rank_common.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_rank_mach.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/controller/mig_7series_v4_1_round_robin_arb.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ecc/mig_7series_v4_1_ecc_buf.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ecc/mig_7series_v4_1_ecc_dec_fix.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ecc/mig_7series_v4_1_ecc_gen.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ecc/mig_7series_v4_1_ecc_merge_enc.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ecc/mig_7series_v4_1_fi_xor.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ip_top/mig_7series_v4_1_memc_ui_top_std.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ip_top/mig_7series_v4_1_mem_intfc.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_byte_group_io.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_byte_lane.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_calib_top.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_if_post_fifo.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_mc_phy.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_mc_phy_wrapper.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_of_pre_fifo.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_4lanes.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ck_addr_cmd_delay.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_dqs_found_cal.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_dqs_found_cal_hr.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_init.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ocd_cntlr.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ocd_data.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ocd_edge.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ocd_lim.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ocd_mux.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ocd_po_cntlr.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_ocd_samp.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_oclkdelay_cal.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_prbs_rdlvl.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_rdlvl.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_tempmon.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_wrcal.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_wrlvl.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_wrlvl_off_delay.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_prbs_gen.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_poc_cc.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_poc_edge_store.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_poc_meta.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_poc_pd.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_poc_tap_base.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_poc_top.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ui/mig_7series_v4_1_ui_cmd.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ui/mig_7series_v4_1_ui_rd_data.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ui/mig_7series_v4_1_ui_top.v" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ui/mig_7series_v4_1_ui_wr_data.v" \

vcom -work xil_defaultlib -93 \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/phy/mig_7series_v4_1_ddr_phy_top.vhd" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ddr_mig_sim.vhd" \
"../../../../loopman.srcs/sources_1/ip/ddr/ddr/user_design/rtl/ddr.vhd" \

vlog -work xil_defaultlib \
"glbl.v"

