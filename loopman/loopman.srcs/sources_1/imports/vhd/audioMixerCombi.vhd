-- audioMixerCombi.vhd -----------------------------------------------------------------------
--
-- Combinational two input digital audio mixer. Uses one single adder with saturation control.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity audioMixerCombi is
  port (
    -- Control
    clipping_o  : out std_logic;
    -- Audio
    input0_i    : in  int_sample(0 downto 0);
    input1_i    : in  int_sample(0 downto 0);
    mix_o       : out int_sample(0 downto 0)
  );
end audioMixerCombi;

architecture syn of audioMixerCombi is
  
  constant max_clip : std_logic_vector(31 downto 0) :=  "01111111111111111111111111111111";
  constant min_clip : std_logic_vector(31 downto 0) :=  "10000000000000000000000000000000";

  signal input0_aux   : std_logic_vector(32 downto 0);
  signal input1_aux   : std_logic_vector(32 downto 0);
  signal added_sample : std_logic_vector(32 downto 0);
  
begin

  -- Extend input samples from 32 to 33 bits
  input0_aux <= '1' & input0_i(0) when input0_i(0)(31) = '1' else
                '0' & input0_i(0);
  input1_aux <= '1' & input1_i(0) when input1_i(0)(31) = '1' else
                '0' & input1_i(0);

  -- Add. Will never overflow, as samples are 32 bits wide
  added_sample <= std_logic_vector(signed(input0_aux) +  signed(input1_aux));

  -- Clipping mechanism (to preserve output level) 32 bit output
  mix_o(0) <= max_clip when added_sample(32) = '0' and added_sample(32 downto 31) /= "00" else
              min_clip when added_sample(32) = '1' and added_sample(32 downto 31) /= "11" else
              added_sample(31 downto 0);
                  
  -- Check if sample is clipped
  clipping_o <= '1' when added_sample(32) = '0' and added_sample(32 downto 31) /= "00" else
                '1' when added_sample(32) = '1' and added_sample(32 downto 31) /= "11" else
                '0';

end syn;
