-- audioMixer.vhd ----------------------------------------------------------------------------
--
-- Digital audio mixer. Multicycle design, only uses one adder with saturation control.
-- Implements hard clipping.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity audioMixer is
  generic ( 
    INPUTS : natural := 2
  );
  port (
    -- System
    rst_i       : in std_logic;
    clk_i       : in std_logic;
    -- Control
    sel_i       : in  std_logic_vector(INPUTS-1 downto 0);
    clipping_o  : out std_logic;
    -- Audio
    input_i     : in  int_sample(INPUTS-1 downto 0);
    input_rdy_i : in  std_logic;
    mix_o       : out int_sample(0 downto 0);
    mix_rdy_o   : out std_logic
  );
end audioMixer;

architecture syn of audioMixer is

  constant max_clip : std_logic_vector(31 downto 0) :=  "01111111111111111111111111111111";
  constant min_clip : std_logic_vector(31 downto 0) :=  "10000000000000000000000000000000";

  -- Having 8 (32 bit) inputs as max, result wont exceed 35 bits wide
  signal sample_to_mix     : std_logic_vector(31 downto 0); -- Comes from component input
  signal sample_to_mix_aux : std_logic_vector(34 downto 0); -- Extended component input
  signal mixed_sample      : std_logic_vector(34 downto 0); -- Accumulated addition
  signal added_sample      : std_logic_vector(34 downto 0); -- Addition (combinational output)
  signal sat_control       : std_logic_vector(31 downto 0); -- Goes to component output
  signal sample_clipped    : std_logic;                     -- Sanple clipped (combinational output) 

begin

  process (clk_i, rst_i, sel_i, input_i)
    type states is (idle, mix, ready);
    variable state : states;
    variable sample_sel : natural range 0 to INPUTS-1;
  begin
    -- Ready bit
    mix_rdy_o <= '0';

    -- Selected sample
    if sel_i(sample_sel) = '1' then
      sample_to_mix <= input_i(sample_sel);
    else
      sample_to_mix <= (others=>'0');
    end if;
    
    case state is
      when idle =>
      when mix =>
      when ready =>
        mix_rdy_o <= '1';
      when others =>
    end case;
    
    if rst_i = '1' then
      sample_sel := 0;
      sample_to_mix <= (others=>'0');
      mixed_sample <= (others=>'0');
      mix_o(0) <= (others=>'0');
      clipping_o <= '0';
      
      state := idle;
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          sample_sel := 0;
          
          if input_rdy_i = '1' then
            -- Leave output loaded as is, especially clipping_o (if ends up
            -- connected to a LED... it might be useful)
            mixed_sample <= (others=>'0');
            mix_o(0) <= (others=>'0');
            
            state := mix;
          end if;
        when mix =>
          if sample_sel = INPUTS-1 then -- Copy last mix to output
            -- Checks if sample is clipped after mixing last sample, as while mixing,
            -- overflow may appear and disappear depending on samples value
            clipping_o <= sample_clipped;
            
            -- Copy final result to output
            mix_o(0) <= sat_control;
            -- Reset accumulation
            mixed_sample <= (others=>'0');
            
            state := ready;
          else
            -- Simply add
            mixed_sample <= added_sample;
            sample_sel := sample_sel + 1;
          end if;
          
        when ready =>
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
  -- Extend input sample from 32 to 35 bits
  sample_to_mix_aux <= "111" & sample_to_mix when sample_to_mix(31) = '1' else
                       "000" & sample_to_mix;

  -- Add. Will never overflow, as samples are 32 bits wide
  added_sample <= std_logic_vector(signed(sample_to_mix_aux) +  signed(mixed_sample));

  -- Clipping mechanism (to preserve output level) 32 bit output
  sat_control <= max_clip when added_sample(34) = '0' and added_sample(34 downto 31) /= "0000" else
                 min_clip when added_sample(34) = '1' and added_sample(34 downto 31) /= "1111" else
                 added_sample(31 downto 0);
                  
  -- Check if sample is clipped
  sample_clipped <= '1' when added_sample(34) = '0' and added_sample(34 downto 31) /= "0000" else
                    '1' when added_sample(34) = '1' and added_sample(34 downto 31) /= "1111" else
                    '0';

end syn;
