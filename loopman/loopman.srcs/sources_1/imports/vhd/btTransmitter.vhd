-- btTransmitter.vhd -------------------------------------------------------------------------
--
-- Creates frames to send via Bluetooth containing system status information.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity btTransmitter is
  generic (
    OUTPUTS : natural := 2;
    INPUTS  : natural := 2;
    FX_NO   : natural := 4
  );
  port (
    -- System
    clk_i                  : in  std_logic;
    rst_i                  : in  std_logic;
    -- Bluetooth
    bt_tx_o                : out std_logic;
    -- System status
    -- Tempo
    tempo_value_i          : in  std_logic_vector(15 downto 0);
    t_ch_tempo_value_i     : in  std_logic;
    t_tempo_i              : in  std_logic;
    -- Master
    st_master_vol_i        : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
    t_ch_master_vol_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    st_master_comp_i       : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
    t_ch_master_comp_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
    st_master_outlevel_i   : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
    st_master_clipping_i   : in  std_logic_vector(OUTPUTS-1 downto 0);
    t_ch_master_outlevel_i : in  std_logic_vector(OUTPUTS-1 downto 0);
    -- Track status
    st_rec_i               : in  std_logic_vector(7 downto 0);
    st_play_i              : in  std_logic_vector(7 downto 0);
    st_mute_i              : in  std_logic_vector(7 downto 0);
    st_solo_i              : in  std_logic_vector(7 downto 0);
    st_sync_i              : in  std_logic_vector(7 downto 0);
    st_auto_rec_i          : in  std_logic_vector(7 downto 0);
    t_ch_status_i          : in  std_logic_vector(7 downto 0);
    st_beat_i              : in  std_logic_vector((8*5)-1 downto 0);
    t_ch_beat_i            : in  std_logic_vector(7 downto 0);
    st_looplen_i           : in  std_logic_vector((8*5)-1 downto 0);
    t_ch_looplen_i         : in  std_logic_vector(7 downto 0);
    st_vol_i               : in  std_logic_vector((8*4)-1 downto 0);
    t_ch_vol_i             : in  std_logic_vector(7 downto 0);
    st_odub_i              : in  std_logic_vector((8*4)-1 downto 0);
    t_ch_odub_i            : in  std_logic_vector(7 downto 0);
    st_outlevel_i          : in  std_logic_vector((8*4)-1 downto 0);
    t_ch_outlevel_i        : in  std_logic_vector(7 downto 0);
    in_sel_i               : in  std_logic_vector((8*INPUTS)-1 downto 0);
    t_ch_in_sel_i          : in  std_logic_vector(7 downto 0);
    out_sel_i              : in  std_logic_vector((8*OUTPUTS)-1 downto 0);
    t_ch_out_sel_i         : in  std_logic_vector(7 downto 0);
    -- FX status
    st_on_i                : in  std_logic_vector((8*FX_NO)-1 downto 0);  
    st_queued_i            : in  std_logic_vector((8*FX_NO)-1 downto 0);  
    st_fx_i                : in  std_logic_vector(8*(FX_NO*4)-1 downto 0);
    t_ch_fx_i              : in  std_logic_vector((8*FX_NO)-1 downto 0)   
  );
end btTransmitter;

architecture syn of btTransmitter is

  component rs232Transmitter is
    generic (
      FREQ     : natural;
      BAUDRATE : natural 
    );
    port (
      -- host side
      rst_i      : in  std_logic;
      clk_i      : in  std_logic; 
      data_rdy_i : in  std_logic; 
      data_i     : in  std_logic_vector (7 downto 0);
      busy_o     : out std_logic;
      -- RS232 side
      txd_o      : out std_logic
    );
  end component;

  -- Frame generation

  type t_st_status   is array (integer range <>) of std_logic_vector(8-1 downto 0); -- 8 values per track
  signal st_status           : t_st_status(8-1 downto 0);                           -- 8 tracks
  
  type t_st_fx       is array (integer range <>) of std_logic_vector(8-1 downto 0); -- 8 values per fx
  type t_st_track_fx is array (integer range <>) of t_st_fx(FX_NO-1 downto 0);      -- FX_NO fx per track
  signal st_track_fx         : t_st_track_fx(8-1 downto 0);                         -- 8 tracks

  signal flag_ch_tempo_value      : std_logic;
  signal flag_ch_master_vol       : std_logic_vector(OUTPUTS-1 downto 0);
  signal flag_ch_master_comp      : std_logic_vector(OUTPUTS-1 downto 0);
  signal flag_ch_master_outlevel  : std_logic_vector(OUTPUTS-1 downto 0);
  signal flag_ch_status           : std_logic_vector(7 downto 0);
  signal flag_ch_beat             : std_logic_vector(7 downto 0);
  signal flag_ch_looplen          : std_logic_vector(7 downto 0);
  signal flag_ch_vol              : std_logic_vector(7 downto 0);
  signal flag_ch_odub             : std_logic_vector(7 downto 0);
  signal flag_ch_outlevel         : std_logic_vector(7 downto 0);
  signal flag_ch_in_sel           : std_logic_vector(7 downto 0);
  signal flag_ch_out_sel          : std_logic_vector(7 downto 0);
  signal flag_ch_fx               : std_logic_vector((8*FX_NO)-1 downto 0); 
  
  signal group_ch_master_vol      : std_logic;
  signal group_ch_master_comp     : std_logic;
  signal group_ch_master_outlevel : std_logic;
  signal group_ch_status          : std_logic;
  signal group_ch_beat            : std_logic;
  signal group_ch_looplen         : std_logic;
  signal group_ch_vol             : std_logic;
  signal group_ch_odub            : std_logic;
  signal group_ch_outlevel        : std_logic;
  signal group_ch_in_sel          : std_logic;
  signal group_ch_out_sel         : std_logic;
  signal group_ch_fx              : std_logic;
  
  signal done_ch_tempo_value      : std_logic;                              
  signal done_ch_master_vol       : std_logic_vector(OUTPUTS-1 downto 0);                              
  signal done_ch_master_comp      : std_logic_vector(OUTPUTS-1 downto 0);
  signal done_ch_master_outlevel  : std_logic_vector(OUTPUTS-1 downto 0);                              
  signal done_ch_status           : std_logic_vector(7 downto 0);           
  signal done_ch_beat             : std_logic_vector(7 downto 0);           
  signal done_ch_looplen          : std_logic_vector(7 downto 0);           
  signal done_ch_vol              : std_logic_vector(7 downto 0);           
  signal done_ch_odub             : std_logic_vector(7 downto 0);           
  signal done_ch_outlevel         : std_logic_vector(7 downto 0);           
  signal done_ch_in_sel           : std_logic_vector(7 downto 0);           
  signal done_ch_out_sel          : std_logic_vector(7 downto 0);           
  signal done_ch_fx               : std_logic_vector((8*FX_NO)-1 downto 0);
  
  signal frame_to_fifo            : std_logic_vector(23 downto 0); 
  signal frame_to_fifo_rdy        : std_logic; 
  
  -- Fifo and transmitter
  signal frame_to_send            : std_logic_vector(23 downto 0); 
  signal frame_to_send_req        : std_logic;
  signal data_to_send             : std_logic_vector(7 downto 0); 
  signal data_to_send_rdy         : std_logic;
  signal fifo_empty               : std_logic;
  signal bt_busy                  : std_logic; 

  -- Serial data to send
  signal bt_tx           : std_logic; 

begin

  bt_tx_o <= bt_tx;

  -- Track status
  track_status : process
  begin 
    for i in 0 to 7 loop -- For each track
      st_status(i) <= (others=>'0');
      
      st_status(i)(BTF_TS_rec)      <= st_rec_i(i);
      st_status(i)(BTF_TS_play)     <= st_play_i(i);
      st_status(i)(BTF_TS_mute)     <= st_mute_i(i);
      st_status(i)(BTF_TS_solo)     <= st_solo_i(i);
      st_status(i)(BTF_TS_sync)     <= st_sync_i(i);
      st_status(i)(BTF_TS_auto_rec) <= st_auto_rec_i(i);
    end loop;
  end process;
  
  -- FX status
  fx_status : process
    variable index : natural := 0;
  begin 
    for tr in 0 to 7 loop -- For each track
      for fx in 0 to FX_NO-1 loop -- For each fx
        index := (tr*FX_NO)+fx;
      
        st_track_fx(tr)(fx)             <= (others=>'0');
        st_track_fx(tr)(fx)(0)          <= st_on_i(index);
        st_track_fx(tr)(fx)(1)          <= st_queued_i(index);
        st_track_fx(tr)(fx)(7 downto 4) <= st_fx_i(((index+1)*4)-1 downto 4*index);
      end loop;
    end loop;
  end process;
  
  -- Group signals to speed up flag search
  group_ch_master_vol      <= '1' when flag_ch_master_vol      /= (flag_ch_master_vol'range=>'0')      else '0';
  group_ch_master_comp     <= '1' when flag_ch_master_comp     /= (flag_ch_master_comp'range=>'0')     else '0';
  group_ch_master_outlevel <= '1' when flag_ch_master_outlevel /= (flag_ch_master_outlevel'range=>'0') else '0';
  
  group_ch_status          <= '1' when flag_ch_status          /= (flag_ch_status'range=>'0')          else '0';
  group_ch_beat            <= '1' when flag_ch_beat            /= (flag_ch_beat'range=>'0')            else '0';
  group_ch_looplen         <= '1' when flag_ch_looplen         /= (flag_ch_looplen'range=>'0')         else '0';
  group_ch_vol             <= '1' when flag_ch_vol             /= (flag_ch_vol'range=>'0')             else '0';
  group_ch_odub            <= '1' when flag_ch_odub            /= (flag_ch_odub'range=>'0')            else '0';
  group_ch_outlevel        <= '1' when flag_ch_outlevel        /= (flag_ch_outlevel'range=>'0')        else '0';
  group_ch_in_sel          <= '1' when flag_ch_in_sel          /= (flag_ch_in_sel'range=>'0')          else '0';
  group_ch_out_sel         <= '1' when flag_ch_out_sel         /= (flag_ch_out_sel'range=>'0')         else '0';
  
  group_ch_fx              <= '1' when flag_ch_fx              /= (flag_ch_fx'range=>'0')              else '0';
  
  
  -- Group signals to speed up flag search
  flag_set : process (clk_i, rst_i)
  begin
    if rst_i = '1' then
      flag_ch_tempo_value     <= '0';                
      flag_ch_master_vol      <= (others=>'0');                
      flag_ch_master_comp     <= (others=>'0');                
      flag_ch_master_outlevel <= (others=>'0');                
      flag_ch_status          <= (others=>'0');   
      flag_ch_beat            <= (others=>'0');   
      flag_ch_looplen         <= (others=>'0');   
      flag_ch_vol             <= (others=>'0');   
      flag_ch_odub            <= (others=>'0');   
      flag_ch_outlevel        <= (others=>'0');   
      flag_ch_in_sel          <= (others=>'0');   
      flag_ch_out_sel         <= (others=>'0');   
      flag_ch_fx              <= (others=>'0');
      
    elsif rising_edge(clk_i) then
      if t_ch_tempo_value_i = '1' then
        flag_ch_tempo_value <= '1';
      elsif done_ch_tempo_value = '1' then
        flag_ch_tempo_value <= '0';
      end if;
      
      for i in 0 to OUTPUTS-1 loop
        if t_ch_master_vol_i(i) = '1' then
          flag_ch_master_vol(i) <= '1';
        elsif done_ch_master_vol(i) = '1' then
          flag_ch_master_vol(i) <= '0';
        end if;
      
        if t_ch_master_comp_i(i) = '1' then
          flag_ch_master_comp(i) <= '1';
        elsif done_ch_master_comp(i) = '1' then
          flag_ch_master_comp(i) <= '0';
        end if;
        
        if t_ch_master_outlevel_i(i) = '1' then
          flag_ch_master_outlevel(i) <= '1';
        elsif done_ch_master_outlevel(i) = '1' then
          flag_ch_master_outlevel(i) <= '0';
        end if;
      end loop;
      
      for i in 0 to 7 loop
        if t_ch_status_i(i) = '1' then
          flag_ch_status(i) <= '1';
        elsif done_ch_status(i) = '1' then
          flag_ch_status(i) <= '0';
        end if;
        
        if t_ch_beat_i(i) = '1' then
          flag_ch_beat(i) <= '1';
        elsif done_ch_beat(i) = '1' then
          flag_ch_beat(i) <= '0';
        end if;
 
        if t_ch_looplen_i(i) = '1' then
          flag_ch_looplen(i) <= '1';
        elsif done_ch_looplen(i) = '1' then
          flag_ch_looplen(i) <= '0';
        end if;
        
        if t_ch_vol_i(i) = '1' then
          flag_ch_vol(i) <= '1';
        elsif done_ch_vol(i) = '1' then
          flag_ch_vol(i) <= '0';
        end if;
        
        if t_ch_odub_i(i) = '1' then
          flag_ch_odub(i) <= '1';
        elsif done_ch_odub(i) = '1' then
          flag_ch_odub(i) <= '0';
        end if;
                
        if t_ch_outlevel_i(i) = '1' then
          flag_ch_outlevel(i) <= '1';
        elsif done_ch_outlevel(i) = '1' then
          flag_ch_outlevel(i) <= '0';
        end if;
                        
        if t_ch_in_sel_i(i) = '1' then
          flag_ch_in_sel(i) <= '1';
        elsif done_ch_in_sel(i) = '1' then
          flag_ch_in_sel(i) <= '0';
        end if;   
                             
        if t_ch_out_sel_i(i) = '1' then
          flag_ch_out_sel(i) <= '1';
        elsif done_ch_out_sel(i) = '1' then
          flag_ch_out_sel(i) <= '0';
        end if;
                                     
        for j in 0 to FX_NO-1 loop
          if t_ch_fx_i(j+(i*FX_NO)) = '1' then
            flag_ch_fx(j+(i*FX_NO)) <= '1';
          elsif done_ch_fx(j+(i*FX_NO)) = '1' then
            flag_ch_fx(j+(i*FX_NO)) <= '0';
          end if;
        end loop;
      end loop;
    end if;
  end process;

  -- Waits for changes in system status and builds frames
  frame_builder : process
    type frame_frame_builder is (reset,
                                 idle,
                                 search_master,
                                 search_track,
                                 search_fx,
                                 search_tempo,
                                 frame_out);
    variable state : frame_frame_builder;
    
    variable current_group        : std_logic;
    variable current_output_group : std_logic_vector(OUTPUTS-1 downto 0);
    variable current_track_group  : std_logic_vector(7 downto 0);
    variable current_fx_group     : std_logic_vector(FX_NO-1 downto 0);
    
    variable group_index          : natural range 0 to 12;      -- Index to select whole groups
    variable output_group_index   : natural range 0 to OUTPUTS-1; -- Index to select output inside groups (Master only)
    variable track_group_index    : natural range 0 to 7;       -- Index to select track inside groups
    variable fx_group_index       : natural range 0 to FX_NO-1; -- Index to select fx inside track
    
    variable frame_mode           : std_logic_vector(4 downto 0);
    variable frame_track          : std_logic_vector(2 downto 0);
    variable frame_option         : std_logic_vector(7 downto 0);
    variable frame_value          : std_logic_vector(7 downto 0);
                                 
  begin
    frame_to_fifo_rdy   <= '0';
    current_group       := '0';
    current_output_group := (others=>'0');
    current_track_group := (others=>'0');
    current_fx_group    := (others=>'0');
    frame_mode          := (others=>'0');
    frame_track         := (others=>'0');
    frame_option        := (others=>'0');
    frame_value         := (others=>'0');
    
    -- Content select for frames and group multiplexor
    case group_index is
      when 0 => -- Master vol
        current_group := group_ch_master_vol;
        current_output_group := flag_ch_master_vol;
        
        frame_mode   := BTF_mode_master;
        frame_track  := std_logic_vector(to_unsigned(output_group_index, 3));
        frame_option := BTF_opt_set_vol;
        frame_value  := "0000" & st_master_vol_i(((output_group_index+1)*4)-1 downto 4*output_group_index);
        
      when 1 => -- Master comp
        current_group := group_ch_master_comp;
        current_output_group := flag_ch_master_comp;
        
        frame_mode   := BTF_mode_master;
        frame_track  := std_logic_vector(to_unsigned(output_group_index, 3));
        frame_option := BTF_opt_set_comp;
        frame_value  := "0000" & st_master_comp_i(((output_group_index+1)*4)-1 downto 4*output_group_index);
        
      when 2 => -- Master outlevel
        current_group := group_ch_master_outlevel;
        current_output_group := flag_ch_master_outlevel;
        
        frame_mode   := BTF_mode_master;
        frame_track  := std_logic_vector(to_unsigned(output_group_index, 3));
        frame_option := BTF_opt_outlevel;
        frame_value  := st_master_clipping_i(output_group_index) & "000" & st_master_outlevel_i(((output_group_index+1)*4)-1 downto 4*output_group_index);

      when 3 => -- Track status
        current_group := group_ch_status;
        current_track_group := flag_ch_status;
        
        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_track_status;
        frame_value  := st_status(track_group_index);
        
      when 4 => -- Beat
        current_group := group_ch_beat;
        current_track_group := flag_ch_beat;
        
        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_beat;
        frame_value  := "000" & st_beat_i(((track_group_index+1)*5)-1 downto 5*track_group_index);
        
      when 5 => -- Looplen
        current_group := group_ch_looplen;
        current_track_group := flag_ch_looplen;
        
        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_set_looplen;
        frame_value  := "000" & st_looplen_i(((track_group_index+1)*5)-1 downto 5*track_group_index);
        
      when 6 => -- Vol
        current_group := group_ch_vol;
        current_track_group := flag_ch_vol;
        
        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_set_vol;
        frame_value  := "0000" & st_vol_i(((track_group_index+1)*4)-1 downto 4*track_group_index);
        
      when 7 => -- Odub
        current_group := group_ch_odub;
        current_track_group := flag_ch_odub;

        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_set_odub;
        frame_value  := "0000" & st_odub_i(((track_group_index+1)*4)-1 downto 4*track_group_index);
        
      when 8 => -- Outlevel
        current_group := group_ch_outlevel;
        current_track_group := flag_ch_outlevel;

        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_outlevel;
        frame_value  := "0000" & st_outlevel_i(((track_group_index+1)*4)-1 downto 4*track_group_index);
        
      when 9 => -- Input selection
        current_group := group_ch_in_sel;
        current_track_group := flag_ch_in_sel;

        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_input_sel;
        frame_value  := (others=>'0');
        frame_value(INPUTS-1 downto 0) := in_sel_i(((track_group_index+1)*INPUTS)-1 downto INPUTS*track_group_index);
        
      when 10 => -- Output selection
        current_group := group_ch_out_sel;
        current_track_group := flag_ch_out_sel;
        
        frame_mode   := BTF_mode_track;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := BTF_opt_output_sel;
        frame_value  := (others=>'0');
        frame_value(OUTPUTS-1 downto 0) := out_sel_i(((track_group_index+1)*OUTPUTS)-1 downto OUTPUTS*track_group_index);
        
      when 11 => -- FX
        current_group := group_ch_fx;
        current_fx_group := flag_ch_fx(((track_group_index+1)*FX_NO)-1 downto FX_NO*track_group_index); -- Selects fx group for current track
        
        frame_mode   := BTF_mode_fx;
        frame_track  := std_logic_vector(to_unsigned(track_group_index, 3));
        frame_option := std_logic_vector(to_unsigned(fx_group_index, 8));
        frame_value  := st_track_fx(track_group_index)(fx_group_index);
      
      when 12 => -- Tempo
        current_group := flag_ch_tempo_value;
        current_output_group := (others=>'0'); -- Tempo applies to all, not used
        
        frame_mode   := BTF_mode_tempo;
        frame_track  := std_logic_vector(to_unsigned(0, 3)); -- Tempo applies to all, not used
        frame_option := tempo_value_i(15 downto 8);
        frame_value  := tempo_value_i(7 downto 0);         
        
      when others =>
      
    end case;

    case state is
      when reset => 
      when idle =>
      when search_master =>
      when search_track =>
      when search_fx =>
      when search_tempo =>
      when frame_out =>
        frame_to_fifo_rdy <= '1';
      when others =>
    end case;

    if rst_i = '1' then
      frame_to_fifo           <= (others=>'0');
      output_group_index      := 0;
      track_group_index       := 0;
      group_index             := 0;
      fx_group_index          := 0;
      done_ch_tempo_value     <= '0';
      done_ch_master_vol      <= (others=>'0');
      done_ch_master_comp     <= (others=>'0');
      done_ch_master_outlevel <= (others=>'0');
      done_ch_status          <= (others=>'0');
      done_ch_beat            <= (others=>'0');
      done_ch_looplen         <= (others=>'0');
      done_ch_vol             <= (others=>'0');
      done_ch_odub            <= (others=>'0');
      done_ch_outlevel        <= (others=>'0');
      done_ch_in_sel          <= (others=>'0');
      done_ch_out_sel         <= (others=>'0');
      done_ch_fx              <= (others=>'0');
      
      state := reset;
    elsif rising_edge(clk_i) then
      done_ch_tempo_value     <= '0';
      done_ch_master_vol      <= (others=>'0');
      done_ch_master_comp     <= (others=>'0');
      done_ch_master_outlevel <= (others=>'0');
      done_ch_status          <= (others=>'0');
      done_ch_beat            <= (others=>'0');
      done_ch_looplen         <= (others=>'0');
      done_ch_vol             <= (others=>'0');
      done_ch_odub            <= (others=>'0');
      done_ch_outlevel        <= (others=>'0');
      done_ch_in_sel          <= (others=>'0');
      done_ch_out_sel         <= (others=>'0');
      done_ch_fx              <= (others=>'0');
    
      case state is
        when reset => 
          -- Builds initial reset frame, to reset bluetooth receiver
          frame_to_fifo <= BTF_mode_reset & "000" & "00000000" & "00000000";
          
          state := frame_out;
          
        when idle =>
          -- Find which groups have to send anything
          if current_group = '1' then
            if group_index < 3 then -- 0, 1 and 2 -> Master
              state := search_master;
            elsif group_index = 11 then -- 11 -> Fx
              state := search_fx;
            elsif group_index = 12 then -- 11 -> Tempo
              state := search_tempo;
            else                       -- 3-10 -> Tracks
              state := search_track;
            end if;
          elsif group_index < 12 then
            output_group_index := 0;
            track_group_index := 0;
            fx_group_index := 0;
            group_index := group_index + 1;
          else
            output_group_index := 0;
            track_group_index := 0;
            fx_group_index := 0;
            group_index := 0;
          end if;
          
        when search_master =>
          -- Find index to send
          if current_output_group(output_group_index) = '1' then
            -- Build frame
            frame_to_fifo <= frame_mode & frame_track & frame_option & frame_value;
            -- Clean flag
            case group_index is
              when 0 => -- Master vol
                done_ch_master_vol(output_group_index) <= '1';
              when 1 => -- Master comp
                done_ch_master_comp(output_group_index) <= '1';
              when 2 => -- Master outlevel
                done_ch_master_outlevel(output_group_index) <= '1';
              when others =>
            end case;
            
            state := frame_out;
          elsif output_group_index < OUTPUTS then
            -- Check next
            output_group_index := output_group_index + 1;
          else
            -- All groups checked
            output_group_index := 0;
            state := idle;
          end if;
          
        when search_track =>
          -- Find index to send
          if current_track_group(track_group_index) = '1' then
            -- Build frame
            frame_to_fifo <= frame_mode & frame_track & frame_option & frame_value;
            
            -- Clean flag
            case group_index is
              when 3 => -- Track status
                done_ch_status(track_group_index) <= '1';
              when 4 => -- Beat
                done_ch_beat(track_group_index) <= '1';
              when 5 => -- Looplen
                done_ch_looplen(track_group_index) <= '1';
              when 6 => -- Vol
                done_ch_vol(track_group_index) <= '1';
              when 7 => -- Odub
                done_ch_odub(track_group_index) <= '1';
              when 8 => -- Outlevel
                done_ch_outlevel(track_group_index) <= '1';
              when 9 => -- Input selection
                done_ch_in_sel(track_group_index) <= '1';
              when 10 => -- Output selection
                done_ch_out_sel(track_group_index) <= '1';
              when others =>
            end case;
            
            state := frame_out;
          elsif track_group_index < 8 then
            -- Check next
            track_group_index := track_group_index + 1;
          else
            -- All groups checked
            track_group_index := 0;
            state := idle;
          end if;
          
        when search_fx =>
          if current_fx_group /= (FX_NO=>'0') then
            if current_fx_group(fx_group_index) = '1' then
              -- Build frame
              frame_to_fifo <= frame_mode & frame_track & frame_option & frame_value;
              done_ch_fx(fx_group_index+(track_group_index*FX_NO)) <= '1';
              
              state := frame_out;
              
            elsif fx_group_index < FX_NO-1 then
              -- Check next
              fx_group_index := fx_group_index + 1;
            else
              -- FX group checked
              fx_group_index := 0;
              track_group_index := track_group_index + 1;
            end if;
            
          elsif track_group_index < 8 then
            -- Check next
            track_group_index := track_group_index + 1;
            fx_group_index := 0;
          else
            -- All groups checked
            track_group_index := 0;
            state := idle;
          end if;
          
        when search_tempo => 
          -- Build frame
          frame_to_fifo <= frame_mode & frame_track & frame_option & frame_value;
          -- Clean flag
          done_ch_tempo_value <= '1';
          
          state := frame_out;
        
        when frame_out =>
          -- Sets frame ready bit combinationally
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
  send_fifo : fifo
  generic map (
    WIDTH => 24,
    DEPTH => 128
  )
  port map (
    rst     => rst_i,
    clk     => clk_i,
    wrE     => frame_to_fifo_rdy,
    dataIn  => frame_to_fifo,
    rdE     => frame_to_send_req,
    dataOut => frame_to_send,
    full    => open,
    empty   => fifo_empty
  );
  
  bt_rs232 : rs232transmitter 
  generic map (
    FREQ => 75_000,
    BAUDRATE => 115200
  )
  port map (
    -- host side
    rst_i      => rst_i,
    clk_i      => clk_i,
    data_rdy_i => data_to_send_rdy,
    data_i     => data_to_send,
    busy_o     => bt_busy,
    txd_o      => bt_tx
  );
  
  fifo_manager : process (clk_i, rst_i)
    type fifo_manager_state is (idle,
                                send,
                                send_rdy);
    variable state       : fifo_manager_state;
    variable send_buffer : std_logic_vector(23 downto 0);
    variable shifter     : natural;
  begin
      data_to_send_rdy <= '0';
  
    case state is
      when idle =>
      when send => 
      when send_rdy => 
        if bt_busy = '0' then
          data_to_send_rdy <= '1';
        end if;
      when others => 
    end case;
    
    if rst_i = '1' then
      send_buffer := (others=>'0');
      frame_to_send_req <= '0';
      shifter := 0;
      
      state := idle;
      
    elsif rising_edge(clk_i) then
      frame_to_send_req <= '0';
      
      case state is
        when idle =>
          -- If fifo has something, send it and request more
          if fifo_empty = '0' then
            send_buffer := frame_to_send;
            shifter := 0;
            frame_to_send_req <= '1';
            
            state := send;
          end if;
          
        when send =>
          -- 24 bits sent in packs of 8 (plus 1 stop bit, no parity)
          if bt_busy = '0' then
            data_to_send <= send_buffer(((shifter+1)*8)-1 downto 8*shifter);
            state := send_rdy;
          end if;
          
        when send_rdy =>
          -- data to send is ready
          if shifter >= 3 then
            shifter := 0;
            state := idle;
          else
            shifter := shifter + 1;
            state   := send;
          end if;
          
        when others => 
          state := idle;
      end case;
    end if;
  end process;

end syn;
