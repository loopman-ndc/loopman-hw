-- fxProcessor.vhd ----------------------------------------------------------------------------
--
-- Controls each track effects. To make it work, in first place effects have to be enqueued in
-- the desired order. To dequeue effects a "clean" operation has to be performed.
-- Once pedalboard is ready, effects can be enabled and its parameters set.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxProcessor is
  generic (
    FX_NO    : natural := 2;
    TRACK_ID : natural := 0
  );
  port (
    -- System
    rst_i         : in  std_logic;
    clk_i         : in  std_logic;
    -- Audio output
    output_o      : out int_sample;
    output_rdy_o  : out std_logic;
    -- Audio input
    input_i       : in  int_sample;
    input_rdy_i   : in  std_logic;
    -- Control
    t_toggle_i    : in  std_logic_vector(FX_NO-1 downto 0);
    t_enqueue_i   : in  std_logic_vector(FX_NO-1 downto 0);
    t_inc_fx_i    : in  std_logic_vector(FX_NO-1 downto 0);
    t_dec_fx_i    : in  std_logic_vector(FX_NO-1 downto 0);
    t_set_fx_i    : in  std_logic_vector(FX_NO-1 downto 0);
    value_fx_i    : in  std_logic_vector(3 downto 0);
    t_clear_i     : in  std_logic;
    -- Status
    st_on_o       : out std_logic_vector(FX_NO-1 downto 0);
    st_queued_o   : out std_logic_vector(FX_NO-1 downto 0);
    st_fx_o       : out std_logic_vector((FX_NO*4)-1 downto 0);
    t_ch_fx_o     : out std_logic_vector(FX_NO-1 downto 0)
  );
end fxProcessor;

architecture syn of fxProcessor is

  component fxUnitDelay is
    generic (
      BRAM_SIZE  : natural := (2**14)
    );
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitOverdrive is
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitJamon is
    generic (
      BRAM_SIZE  : natural := (2**14)
    );
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitFuzz is
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitCompressor is
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitTremolo is
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitHiCut is
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitLowCut is
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitFlanger is
    generic (
      BRAM_SIZE    : natural := (2**7)
    );
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitLPF is
    generic (
      ENABLED : boolean := true
    );
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;
  
  component fxUnitHPF is
    generic (
      ENABLED : boolean := true
    );
    port (
      -- System
      rst_i        : in  std_logic;
      clk_i        : in  std_logic;
      -- Audio output
      output_o     : out int_sample(0 downto 0);
      output_rdy_o : out std_logic;
      -- Audio input
      input_i      : in  int_sample(0 downto 0);
      input_rdy_i  : in  std_logic;
      -- Control
      st_fx_i      : in  std_logic_vector(3 downto 0)
    );
  end component;

  constant default_fx_val : unsigned(3 downto 0) := "1000";
  constant LEN_FX_NO : natural := log2(FX_NO);
  
  constant LPF_ENABLED : boolean := TRACK_ID < 8;
  constant HPF_ENABLED : boolean := TRACK_ID < 8;
  
  type fx_queue_slot is array (integer range <>) of std_logic_vector (LEN_FX_NO-1 downto 0);
  
  signal fx_queue      : fx_queue_slot(FX_NO-1 downto 0);

  signal st_on         : std_logic_vector(FX_NO-1 downto 0);
  signal st_queued     : std_logic_vector(FX_NO-1 downto 0);
  signal st_fx         : unsigned((FX_NO*4)-1 downto 0);
  
  signal queued_count  : natural := 0; -- Total enqueued effects
  
  signal fx_buffer     : int_sample(0 downto 0);             -- Effects pedal input
  signal fx_buffer_rdy : std_logic_vector(FX_NO-1 downto 0); -- Effects pedal input ready
  signal fx_out        : int_sample(FX_NO-1 downto 0);       -- Effects pedal output
  signal fx_out_rdy    : std_logic_vector(FX_NO-1 downto 0); -- Effects pedal output ready
  
  type int_sample_aux is array (integer range <>) of int_sample(0 downto 0);
  signal fx_out_aux    : int_sample_aux(FX_NO-1 downto 0);   -- Aux signal for VHDL
    
begin

  st_queued_o <= st_queued;
  st_on_o <= st_on;
  st_fx_o <= std_logic_vector(st_fx);
  
  enable_control : process (rst_i, clk_i)
  begin
    if rst_i = '1' then
      st_on <= (others=>'0');
      for i in 0 to FX_NO-1 loop
        st_fx(((i+1)*4)-1 downto i*4) <= default_fx_val;
      end loop;
    elsif rising_edge(clk_i) then
     
      for i in 0 to FX_NO-1 loop
        t_ch_fx_o(i) <= '0';
      
        if t_clear_i = '1' then
          st_on(i) <= '0';
          t_ch_fx_o(i) <= '1';
        elsif st_queued(i) = '1' then
          -- Only queued effects can be enabled/operated
          if t_toggle_i(i) = '1' then
            st_on(i) <= not st_on(i);
            t_ch_fx_o(i) <= '1';
          elsif t_inc_fx_i(i) = '1' and st_fx(((i+1)*4)-1 downto i*4) < "1111" then
            st_fx(((i+1)*4)-1 downto i*4) <= st_fx(((i+1)*4)-1 downto i*4) + 1;
            t_ch_fx_o(i) <= '1';
          elsif t_dec_fx_i(i) = '1' and st_fx(((i+1)*4)-1 downto i*4) > "0000" then
            st_fx(((i+1)*4)-1 downto i*4) <= st_fx(((i+1)*4)-1 downto i*4) - 1;
            t_ch_fx_o(i) <= '1';
          elsif t_set_fx_i(i) = '1' then
            st_fx(((i+1)*4)-1 downto i*4) <= unsigned(value_fx_i);
            t_ch_fx_o(i) <= '1';
          end if;
        elsif t_enqueue_i(i) = '1' and st_queued(i) = '0' then
          t_ch_fx_o(i) <= '1';
        end if;
      end loop;

    end if;
  end process;

  queue_control : process (rst_i, clk_i)
    variable fx_code : std_logic_vector(LEN_FX_NO-1 downto 0);
  begin
    if rst_i = '1' then
      st_queued <= (others=>'0');
      queued_count <= 0;
      for i in 0 to FX_NO-1 loop
        fx_queue(i) <= (others=>'0');
      end loop;
    elsif rising_edge(clk_i) then
      for i in 0 to FX_NO-1 loop
        fx_code := std_logic_vector(to_unsigned(i, LEN_FX_NO));
      
        if t_clear_i = '1' then
          st_queued(i) <= '0';
          queued_count <= 0;
          for i in 0 to FX_NO-1 loop
            fx_queue(i) <= (others=>'0');
          end loop;
        elsif t_enqueue_i(i) = '1' and st_queued(i) = '0' then
          st_queued(i) <= '1';
          fx_queue(queued_count) <= fx_code;
          queued_count <= queued_count + 1;
        end if;
      end loop;
    end if;
  end process;
  
  main_control : process (rst_i, clk_i, fx_queue)
    type fx_proc_states is (idle,   -- Waits for audio input
                            fx,     -- Applies fx
                            next_fx,-- Gets next fx from queue
                            done);  -- Alright!
    variable state : fx_proc_states;
    
    variable fx_index : natural;
    variable queue_index : natural;
  begin
    
    -- Queue to fx translation
    fx_index := to_integer(unsigned(fx_queue(queue_index)));
    
    case state is
      when idle =>
      when fx =>
      when next_fx => 
      when done => 
      when others => 
    end case;
  
    if rst_i = '1' then
      queue_index := 0;
      fx_buffer(0) <= (others=>'0');
      fx_buffer_rdy <= (others=>'0');
      output_o(0) <= (others=>'0');
      output_rdy_o <= '0';
      
      state := idle;
    elsif rising_edge(clk_i) then
      fx_buffer_rdy <= (others=>'0');  
      output_rdy_o <= '0';
      
      case state is
        when idle =>
          -- Waits for new audio sample
          queue_index := 0;
          
          if input_rdy_i = '1' then
            -- First case loads clean input
            fx_buffer <= input_i;
            
            if st_on = (st_on'range=>'0') or st_queued = (st_queued'range=>'0') then
              -- NOFX yet, skip audio processing
              state := done;
            else
              -- There are queued effects
              state := next_fx;
            end if;
          end if;
          
        when next_fx =>
          -- Checks if all queued effects have been processed 
          if queue_index < queued_count and st_on(fx_index) = '1' then
            -- Not all queued fx processed yet, and current fx enabled
            -- Applies this fx
            fx_buffer_rdy(fx_index) <= '1';
            
            state := fx;
          elsif queue_index < queued_count then
            -- Not all queued fx processed yet, and current fx disabled
            -- Skips this fx
            queue_index := queue_index + 1;
            
            state := next_fx;
          else
            -- All queued fx processed
            state := done;
          end if;
          
        when fx =>
          -- Waits for fx to end processing
          if fx_out_rdy(fx_index) = '1' then
            fx_buffer(0) <= fx_out(fx_index);
            
            queue_index := queue_index + 1;
            
            state := next_fx;
          end if;
        
        when done =>
          -- All effects applied
          output_o <= fx_buffer;
          output_rdy_o <= '1';
          
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;

  -- FX
  fx_out_aux_connect : process
  begin
    for i in 0 to FX_NO-1 loop
      fx_out(i) <= fx_out_aux(i)(0);
    end loop;
  end process;

  -- 0. DELAY MAN
  delay_man : fxUnitDelay
  generic map (
    BRAM_SIZE => (2**13)
  )
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(0),
    output_rdy_o => fx_out_rdy(0),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(0),
    -- Control
    st_fx_i   => std_logic_vector(st_fx(((0+1)*4)-1 downto 0*4))
  );

  -- 1. OVERDRIVE MAN
  overdrive_man : fxUnitOverdrive
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(1),
    output_rdy_o => fx_out_rdy(1),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(1),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((1+1)*4)-1 downto 1*4))
  );
  
  -- 2. JAMON MAN
  jamon_man : fxUnitJamon
  generic map (
    BRAM_SIZE => (2**12)
  )
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(2),
    output_rdy_o => fx_out_rdy(2),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(2),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((2+1)*4)-1 downto 2*4))
  );
  
  -- 3. FUZZ MAN
  fuzz_man : fxUnitFuzz
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(3),
    output_rdy_o => fx_out_rdy(3),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(3),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((3+1)*4)-1 downto 3*4))
  );
  
  -- 4. COMPRESSOR MAN
  compressor_man : fxUnitCompressor
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(4),
    output_rdy_o => fx_out_rdy(4),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(4),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((4+1)*4)-1 downto 4*4))
  );
  
  -- 5. TREMOLO MAN
  tremolo_man : fxUnitTremolo
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(5),
    output_rdy_o => fx_out_rdy(5),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(5),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((5+1)*4)-1 downto 5*4))
  );
  
  -- 6. FLANGER MAN
  flanger_man : fxUnitFlanger
  generic map (
    BRAM_SIZE => (2**7)
  )
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(6),
    output_rdy_o => fx_out_rdy(6),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(6),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((6+1)*4)-1 downto 6*4))
  );
  
  -- 7. HI CUT MAN
  hi_cut_man : fxUnitLPF
  generic map (
    ENABLED => LPF_ENABLED
  )
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(7),
    output_rdy_o => fx_out_rdy(7),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(7),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((7+1)*4)-1 downto 7*4))
  );
  
  -- 8. LO CUT MAN
  lo_cut_man : fxUnitHPF
  generic map (
    ENABLED => HPF_ENABLED
  )
  port map (
    -- System
    rst_i        => rst_i,
    clk_i        => clk_i,
    -- Audio output
    output_o     => fx_out_aux(8),
    output_rdy_o => fx_out_rdy(8),
    -- Audio input
    input_i      => fx_buffer,
    input_rdy_i  => fx_buffer_rdy(8),
    -- Control
    st_fx_i      => std_logic_vector(st_fx(((8+1)*4)-1 downto 8*4))
  );

end syn;

