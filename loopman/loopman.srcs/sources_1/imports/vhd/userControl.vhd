-- userControl.vhd ----------------------------------------------------------------------------
--
-- Wrapper for control components.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity userControl is
  generic (
    OUTPUTS : natural := 2;
    INPUTS  : natural := 2;
    FX_NO   : natural := 2
  );
  port (
    -- System
    rst_i                : in  std_logic;
    clk_i                : in  std_logic;
    -- PS2
    ps2_clk_i            : in  std_logic;
    ps2_data_i           : in  std_logic;
    -- Bluetooth          
    bt_rx_i              : in  std_logic;
    -- HW switches
    pedal_rec_encoder_i  : in  std_logic_vector(2 downto 0);
    pedal_rec_gs_i       : in  std_logic;
    pedal_play_encoder_i : in  std_logic_vector(2 downto 0);
    pedal_play_gs_i      : in  std_logic;
    -- Control
    t_reset_req_o        : out std_logic;
    -- Master
    t_inc_master_comp_o  : out std_logic_vector(OUTPUTS-1 downto 0);
    t_dec_master_comp_o  : out std_logic_vector(OUTPUTS-1 downto 0);
    t_set_master_comp_o  : out std_logic_vector(OUTPUTS-1 downto 0);
    value_master_comp_o  : out std_logic_vector(3 downto 0);
    t_inc_master_vol_o   : out std_logic_vector(OUTPUTS-1 downto 0);
    t_dec_master_vol_o   : out std_logic_vector(OUTPUTS-1 downto 0);
    t_set_master_vol_o   : out std_logic_vector(OUTPUTS-1 downto 0);
    value_master_vol_o   : out std_logic_vector(3 downto 0);
    -- Tracks
    t_rec_o              : out std_logic_vector(7 downto 0);           
    t_play_o             : out std_logic_vector(7 downto 0);           
    t_mute_o             : out std_logic_vector(7 downto 0);           
    t_solo_o             : out std_logic_vector(7 downto 0);           
    t_clear_o            : out std_logic_vector(7 downto 0);           
    t_sync_o             : out std_logic_vector(7 downto 0);
    t_auto_rec_tg_o      : out std_logic_vector(7 downto 0);         
    t_inc_looplen_o      : out std_logic_vector(7 downto 0);           
    t_dec_looplen_o      : out std_logic_vector(7 downto 0);           
    t_set_looplen_o      : out std_logic_vector(7 downto 0);
    value_looplen_o      : out std_logic_vector(4 downto 0);           
    t_inc_vol_o          : out std_logic_vector(7 downto 0);           
    t_dec_vol_o          : out std_logic_vector(7 downto 0);           
    t_set_vol_o          : out std_logic_vector(7 downto 0);
    value_vol_o          : out std_logic_vector(3 downto 0);           
    t_inc_odub_o         : out std_logic_vector(7 downto 0);           
    t_dec_odub_o         : out std_logic_vector(7 downto 0);                          
    t_set_odub_o         : out std_logic_vector(7 downto 0);
    value_odub_o         : out std_logic_vector(3 downto 0);                          
    t_input_sel_o        : out std_logic_vector((8*INPUTS)-1 downto 0); 
    t_output_sel_o       : out std_logic_vector((8*OUTPUTS)-1 downto 0);
    track_select_o       : out std_logic_vector(7 downto 0); -- for VGA only
    -- Tempo                                                           
    t_hit_tempo_o        : out std_logic;       
    t_set_tempo_o        : out std_logic;       
    tempo_value_o        : out std_logic_vector(15 downto 0);
    -- FX
    t_fx_toggle_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_enqueue_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_inc_fx_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_dec_fx_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_set_fx_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
    value_fx_o           : out std_logic_vector((8*4)-1 downto 0);    
    t_fx_clear_o         : out std_logic_vector(7 downto 0)          
  );
end userControl;

architecture syn of userControl is

  component ps2Controller is
    generic (
      OUTPUTS         : natural := 2;
      INPUTS          : natural := 2;
      FX_NO           : natural := 2
    );
    port (
      clk_i      : in  std_logic;
      rst_i      : in  std_logic;
      -- PS2
      ps2_clk_i  : in  std_logic;
      ps2_data_i : in  std_logic;
      -- Master
      t_inc_master_vol_o : out std_logic_vector(OUTPUTS-1 downto 0);
      t_dec_master_vol_o : out std_logic_vector(OUTPUTS-1 downto 0);
      -- Tracks
      t_rec_o         : out std_logic_vector(7 downto 0);
      t_play_o        : out std_logic_vector(7 downto 0);
      t_mute_o        : out std_logic_vector(7 downto 0);
      t_solo_o        : out std_logic_vector(7 downto 0);
      t_clear_o       : out std_logic_vector(7 downto 0);
      t_sync_o        : out std_logic_vector(7 downto 0);
      t_inc_looplen_o : out std_logic_vector(7 downto 0);
      t_dec_looplen_o : out std_logic_vector(7 downto 0);
      t_inc_vol_o     : out std_logic_vector(7 downto 0);
      t_dec_vol_o     : out std_logic_vector(7 downto 0);
      t_inc_odub_o    : out std_logic_vector(7 downto 0);
      t_dec_odub_o    : out std_logic_vector(7 downto 0);               
      t_input_sel_o   : out std_logic_vector((8*INPUTS)-1 downto 0); 
      t_output_sel_o  : out std_logic_vector((8*OUTPUTS)-1 downto 0);
      track_select_o  : out std_logic_vector(7 downto 0);
      -- Tempo                                                      
      t_hit_tempo_o   : out std_logic;
      -- FX
      t_fx_toggle_o   : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_enqueue_o  : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_inc_fx_o   : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_dec_fx_o   : out std_logic_vector((8*FX_NO)-1 downto 0);  
      t_fx_clear_o    : out std_logic_vector(7 downto 0)
    );
  end component;
  
  component btReceiver is
    generic (
      OUTPUTS : natural := 2;
      INPUTS  : natural := 2;
      FX_NO   : natural := 2
    );
    port (
      -- System
      rst_i               : in std_logic;
      clk_i               : in std_logic;
      -- Bluetooth        
      bt_rx_i             :  in std_logic;
      -- Control
      t_reset_req_o       : out std_logic;
      -- Master
      t_inc_master_comp_o : out std_logic_vector(OUTPUTS-1 downto 0);
      t_dec_master_comp_o : out std_logic_vector(OUTPUTS-1 downto 0);
      t_set_master_comp_o : out std_logic_vector(OUTPUTS-1 downto 0);
      value_master_comp_o : out std_logic_vector(3 downto 0);
      t_inc_master_vol_o  : out std_logic_vector(OUTPUTS-1 downto 0);
      t_dec_master_vol_o  : out std_logic_vector(OUTPUTS-1 downto 0);
      t_set_master_vol_o  : out std_logic_vector(OUTPUTS-1 downto 0);
      value_master_vol_o  : out std_logic_vector(3 downto 0);
      -- Tracks
      t_rec_o             : out std_logic_vector(7 downto 0);           
      t_play_o            : out std_logic_vector(7 downto 0);           
      t_mute_o            : out std_logic_vector(7 downto 0);           
      t_solo_o            : out std_logic_vector(7 downto 0);           
      t_clear_o           : out std_logic_vector(7 downto 0);           
      t_sync_o            : out std_logic_vector(7 downto 0);
      t_auto_rec_tg_o     : out std_logic_vector(7 downto 0);          
      t_inc_looplen_o     : out std_logic_vector(7 downto 0);           
      t_dec_looplen_o     : out std_logic_vector(7 downto 0);           
      t_set_looplen_o     : out std_logic_vector(7 downto 0);           
      value_looplen_o     : out std_logic_vector(4 downto 0);           
      t_inc_vol_o         : out std_logic_vector(7 downto 0);           
      t_dec_vol_o         : out std_logic_vector(7 downto 0);           
      t_set_vol_o         : out std_logic_vector(7 downto 0);           
      value_vol_o         : out std_logic_vector(3 downto 0);           
      t_inc_odub_o        : out std_logic_vector(7 downto 0);           
      t_dec_odub_o        : out std_logic_vector(7 downto 0);                          
      t_set_odub_o        : out std_logic_vector(7 downto 0);                          
      value_odub_o        : out std_logic_vector(3 downto 0);                          
      t_input_sel_o       : out std_logic_vector((8*INPUTS)-1 downto 0); 
      t_output_sel_o      : out std_logic_vector((8*OUTPUTS)-1 downto 0);
      -- Tempo                                                          
      t_set_tempo_o       : out std_logic;       
      tempo_value_o       : out std_logic_vector(15 downto 0);
      -- FX
      t_fx_toggle_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_enqueue_o      : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_inc_fx_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_dec_fx_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_set_fx_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
      value_fx_o          : out std_logic_vector((8*4)-1 downto 0);    
      t_fx_clear_o        : out std_logic_vector(7 downto 0)     
    );
  end component;
  
  component pedalController is
   port (
      -- System
      clk_i          : in std_logic;
      rst_i          : in std_logic;
      -- Input
      rec_encoder_i  : in std_logic_vector(2 downto 0);
      rec_gs_i       : in std_logic;
      play_encoder_i : in std_logic_vector(2 downto 0);
      play_gs_i      : in std_logic;
      aux_encoder_i  : in std_logic_vector(2 downto 0);
      aux_gs_i       : in std_logic;
      -- Control ticks
      t_rec_o        : out std_logic_vector(7 downto 0);
      t_play_o       : out std_logic_vector(7 downto 0);
      t_aux_o        : out std_logic_vector(7 downto 0)
    );
  end component;

  
  -- Reset
  signal                        t_reset_req_bt       : std_logic;   
  -- Master
  signal t_inc_master_comp_ps2, t_inc_master_comp_bt : std_logic_vector(OUTPUTS-1 downto 0);      
  signal t_dec_master_comp_ps2, t_dec_master_comp_bt : std_logic_vector(OUTPUTS-1 downto 0);      
  signal                        t_set_master_comp_bt : std_logic_vector(OUTPUTS-1 downto 0);     
  signal                        value_master_comp_bt : std_logic_vector(3 downto 0);     
  signal t_inc_master_vol_ps2 , t_inc_master_vol_bt  : std_logic_vector(OUTPUTS-1 downto 0);   
  signal t_dec_master_vol_ps2 , t_dec_master_vol_bt  : std_logic_vector(OUTPUTS-1 downto 0);   
  signal                        t_set_master_vol_bt  : std_logic_vector(OUTPUTS-1 downto 0);   
  signal                        value_master_vol_bt  : std_logic_vector(3 downto 0);  
  -- Tracks 
  signal t_rec_ps2            , t_rec_bt             , t_rec_pedal  : std_logic_vector(7 downto 0);            
  signal t_play_ps2           , t_play_bt            , t_play_pedal : std_logic_vector(7 downto 0);            
  signal t_mute_ps2           , t_mute_bt            : std_logic_vector(7 downto 0);            
  signal t_solo_ps2           , t_solo_bt            : std_logic_vector(7 downto 0);            
  signal t_clear_ps2          , t_clear_bt           : std_logic_vector(7 downto 0);            
  signal t_sync_ps2           , t_sync_bt            : std_logic_vector(7 downto 0);            
  signal t_auto_rec_tg_ps2    , t_auto_rec_tg_bt     : std_logic_vector(7 downto 0);            
  signal t_inc_looplen_ps2    , t_inc_looplen_bt     : std_logic_vector(7 downto 0);            
  signal t_dec_looplen_ps2    , t_dec_looplen_bt     : std_logic_vector(7 downto 0);            
  signal                        t_set_looplen_bt     : std_logic_vector(7 downto 0);            
  signal                        value_looplen_bt     : std_logic_vector(4 downto 0);            
  signal t_inc_vol_ps2        , t_inc_vol_bt         : std_logic_vector(7 downto 0);            
  signal t_dec_vol_ps2        , t_dec_vol_bt         : std_logic_vector(7 downto 0);
  signal                        t_set_vol_bt         : std_logic_vector(7 downto 0);             
  signal                        value_vol_bt         : std_logic_vector(3 downto 0);             
  signal t_inc_odub_ps2       , t_inc_odub_bt        : std_logic_vector(7 downto 0);            
  signal t_dec_odub_ps2       , t_dec_odub_bt        : std_logic_vector(7 downto 0); 
  signal                        t_set_odub_bt        : std_logic_vector(7 downto 0);            
  signal                        value_odub_bt        : std_logic_vector(3 downto 0);            
  signal t_input_sel_ps2      , t_input_sel_bt       : std_logic_vector((8*INPUTS)-1 downto 0); 
  signal t_output_sel_ps2     , t_output_sel_bt      : std_logic_vector((8*OUTPUTS)-1 downto 0);
  -- Tempo                                                       
  signal t_hit_tempo_ps2                             : std_logic;                         
  signal                        t_set_tempo_bt       : std_logic;                         
  signal                        tempo_value_bt       : std_logic_vector(15 downto 0);
  -- FX
  signal t_fx_toggle_ps2      , t_fx_toggle_bt       : std_logic_vector((8*FX_NO)-1 downto 0);
  signal t_fx_enqueue_ps2     , t_fx_enqueue_bt      : std_logic_vector((8*FX_NO)-1 downto 0);
  signal t_fx_inc_fx_ps2      , t_fx_inc_fx_bt       : std_logic_vector((8*FX_NO)-1 downto 0);
  signal t_fx_dec_fx_ps2      , t_fx_dec_fx_bt       : std_logic_vector((8*FX_NO)-1 downto 0);
  signal                        t_fx_set_fx_bt       : std_logic_vector((8*FX_NO)-1 downto 0);
  signal                        value_fx_bt          : std_logic_vector((8*4)-1 downto 0);
  signal t_fx_clear_ps2       , t_fx_clear_bt        : std_logic_vector(7 downto 0);
                    
begin
  
  -- Signal assignment
  t_reset_req_o       <=                           t_reset_req_bt;
  
  t_inc_master_comp_o <=  t_inc_master_comp_ps2 or t_inc_master_comp_bt;
  t_dec_master_comp_o <=  t_dec_master_comp_ps2 or t_dec_master_comp_bt;
  t_set_master_comp_o <=                           t_set_master_comp_bt;
  value_master_comp_o <=                           value_master_comp_bt;
  t_inc_master_vol_o  <=  t_inc_master_vol_ps2  or t_inc_master_vol_bt ;
  t_dec_master_vol_o  <=  t_dec_master_vol_ps2  or t_dec_master_vol_bt ;
  t_set_master_vol_o  <=                           t_set_master_vol_bt ;
  value_master_vol_o  <=                           value_master_vol_bt ;

  t_rec_o             <= t_rec_ps2              or t_rec_bt            or t_rec_pedal;
  t_play_o            <= t_play_ps2             or t_play_bt           or t_play_pedal;
  t_mute_o            <= t_mute_ps2             or t_mute_bt           ;
  t_solo_o            <= t_solo_ps2             or t_solo_bt           ;
  t_clear_o           <= t_clear_ps2            or t_clear_bt          ;
  t_sync_o            <= t_sync_ps2             or t_sync_bt           ;
  t_auto_rec_tg_o     <= t_auto_rec_tg_ps2      or t_auto_rec_tg_bt    ;
  t_inc_looplen_o     <= t_inc_looplen_ps2      or t_inc_looplen_bt    ;
  t_dec_looplen_o     <= t_dec_looplen_ps2      or t_dec_looplen_bt    ;
  t_set_looplen_o     <=                           t_set_looplen_bt    ;
  value_looplen_o     <= value_looplen_bt;
  t_inc_vol_o         <= t_inc_vol_ps2          or t_inc_vol_bt        ;
  t_dec_vol_o         <= t_dec_vol_ps2          or t_dec_vol_bt        ;
  t_set_vol_o         <=                           t_set_vol_bt        ;
  value_vol_o         <= value_vol_bt;
  t_inc_odub_o        <= t_inc_odub_ps2         or t_inc_odub_bt       ;
  t_dec_odub_o        <= t_dec_odub_ps2         or t_dec_odub_bt       ;
  t_set_odub_o        <=                           t_set_odub_bt       ;
  value_odub_o        <= value_odub_bt;
  t_input_sel_o       <= t_input_sel_ps2        or t_input_sel_bt      ;
  t_output_sel_o      <= t_output_sel_ps2       or t_output_sel_bt     ;
       
  t_hit_tempo_o       <= t_hit_tempo_ps2;
  t_set_tempo_o       <=                           t_set_tempo_bt      ;
  tempo_value_o       <=                           tempo_value_bt      ;
  
  t_fx_toggle_o       <= t_fx_toggle_ps2        or t_fx_toggle_bt      ;
  t_fx_enqueue_o      <= t_fx_enqueue_ps2       or t_fx_enqueue_bt     ;
  t_fx_inc_fx_o       <= t_fx_inc_fx_ps2        or t_fx_inc_fx_bt      ;
  t_fx_dec_fx_o       <= t_fx_dec_fx_ps2        or t_fx_dec_fx_bt      ;
  t_fx_set_fx_o       <=                           t_fx_set_fx_bt      ;
  value_fx_o          <=                           value_fx_bt         ;
  t_fx_clear_o        <= t_fx_clear_ps2         or t_fx_clear_bt       ;
  
  ps2 : ps2Controller
  generic map (
    OUTPUTS => OUTPUTS,
    INPUTS  => INPUTS,
    FX_NO   => FX_NO
  )
  port map (
    clk_i => clk_i,
    rst_i => rst_i,
    -- PS2
    ps2_clk_i  => ps2_clk_i,
    ps2_data_i => ps2_data_i,
    -- Master
    t_inc_master_vol_o => t_inc_master_vol_ps2,
    t_dec_master_vol_o => t_dec_master_vol_ps2,
    -- Tracks
    t_rec_o         => t_rec_ps2,        
    t_play_o        => t_play_ps2,       
    t_mute_o        => t_mute_ps2,       
    t_solo_o        => t_solo_ps2,       
    t_clear_o       => t_clear_ps2,      
    t_sync_o        => t_sync_ps2,       
    t_inc_looplen_o => t_inc_looplen_ps2,
    t_dec_looplen_o => t_dec_looplen_ps2,
    t_inc_vol_o     => t_inc_vol_ps2,    
    t_dec_vol_o     => t_dec_vol_ps2,    
    t_inc_odub_o    => t_inc_odub_ps2,   
    t_dec_odub_o    => t_dec_odub_ps2,                
    t_input_sel_o   => t_input_sel_ps2,  
    t_output_sel_o  => t_output_sel_ps2,
    track_select_o  => track_select_o,
    -- Tempo            
    t_hit_tempo_o   => t_hit_tempo_ps2,
    -- FX
    t_fx_toggle_o  => t_fx_toggle_ps2,
    t_fx_enqueue_o => t_fx_enqueue_ps2,
    t_fx_inc_fx_o  => t_fx_inc_fx_ps2,
    t_fx_dec_fx_o  => t_fx_dec_fx_ps2,
    t_fx_clear_o   => t_fx_clear_ps2
  );
  
  bluetooth : btReceiver
  generic map (
    OUTPUTS => OUTPUTS,
    INPUTS  => INPUTS,
    FX_NO   => FX_NO
  )
  port map (
    -- System
    rst_i => rst_i,
    clk_i => clk_i,
    -- Bluetooth
    bt_rx_i => bt_rx_i,
    -- Control
    t_reset_req_o       => t_reset_req_bt,
    -- Master
    t_inc_master_comp_o => t_inc_master_comp_bt,
    t_dec_master_comp_o => t_dec_master_comp_bt,
    t_set_master_comp_o => t_set_master_comp_bt,
    value_master_comp_o => value_master_comp_bt,
    t_inc_master_vol_o  => t_inc_master_vol_bt,
    t_dec_master_vol_o  => t_dec_master_vol_bt,
    t_set_master_vol_o  => t_set_master_vol_bt,
    value_master_vol_o  => value_master_vol_bt,
    -- Tracks
    t_rec_o             => t_rec_bt,            
    t_play_o            => t_play_bt,            
    t_mute_o            => t_mute_bt,            
    t_solo_o            => t_solo_bt,           
    t_clear_o           => t_clear_bt,           
    t_sync_o            => t_sync_bt,            
    t_auto_rec_tg_o     => t_auto_rec_tg_bt,            
    t_inc_looplen_o     => t_inc_looplen_bt,
    t_dec_looplen_o     => t_dec_looplen_bt,
    t_set_looplen_o     => t_set_looplen_bt,
    value_looplen_o     => value_looplen_bt,
    t_inc_vol_o         => t_inc_vol_bt,
    t_dec_vol_o         => t_dec_vol_bt,
    t_set_vol_o         => t_set_vol_bt,
    value_vol_o         => value_vol_bt,
    t_inc_odub_o        => t_inc_odub_bt,
    t_dec_odub_o        => t_dec_odub_bt,              
    t_set_odub_o        => t_set_odub_bt,              
    value_odub_o        => value_odub_bt,              
    t_input_sel_o       => t_input_sel_bt,
    t_output_sel_o      => t_output_sel_bt,
    -- Tempo                                                          
    t_set_tempo_o       => t_set_tempo_bt,       
    tempo_value_o       => tempo_value_bt,
    -- FX               
    t_fx_toggle_o       => t_fx_toggle_bt,
    t_fx_enqueue_o      => t_fx_enqueue_bt,
    t_fx_inc_fx_o       => t_fx_inc_fx_bt,
    t_fx_dec_fx_o       => t_fx_dec_fx_bt,
    t_fx_set_fx_o       => t_fx_set_fx_bt,
    value_fx_o          => value_fx_bt,
    t_fx_clear_o        => t_fx_clear_bt
  );
  
   pedalboard : pedalController
   port map (
    -- System
    clk_i          => clk_i,
    rst_i          => rst_i,
    -- Input
    rec_encoder_i  => pedal_rec_encoder_i,
    rec_gs_i       => pedal_rec_gs_i,
    play_encoder_i => pedal_play_encoder_i,
    play_gs_i      => pedal_play_gs_i,
    aux_encoder_i  => "000",
    aux_gs_i       => '1',
    -- Control ticks
    t_rec_o        => t_rec_pedal,
    t_play_o       => t_play_pedal,
    t_aux_o        => open
  );

end syn;
