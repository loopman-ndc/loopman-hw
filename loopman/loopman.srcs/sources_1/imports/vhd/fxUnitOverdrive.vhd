-- fxUnitOverdrive.vhd -----------------------------------------------------------------------
--
-- Overdrive effect.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitOverdrive is
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitOverdrive;

architecture syn of fxUnitOverdrive is

  constant threshold_max  : natural := 32_000_000;
  constant threshold_step : natural := 1_000_000;
  
  constant max_clip : std_logic_vector(31 downto 0) :=  "01111111111111111111111111111111";
  constant min_clip : std_logic_vector(31 downto 0) :=  "10000000000000000000000000000000";

  signal st_fx     : natural;
  
  signal pure_overdrive : int_sample(0 downto 0);

  signal over_threshold : std_logic;
  signal clip_value     : int_sample(0 downto 0);
  signal boosted_out    : int_sample(0 downto 0);
  signal boosted_out_combi : int_sample(0 downto 0);
  
  signal boost_control : std_logic_vector(3 downto 0);
  
begin

  -- True bypass
  output_o <= boosted_out when st_fx_i /= (st_fx_i'range=>'0') else input_i;
  
  -- Value control
  st_fx <= to_integer(unsigned(st_fx_i));
  boost_control <= '0' & st_fx_i(3 downto 1);
  
  booster : audioBooster 
  port map (
    -- Control
    volume_i    => boost_control,
    -- Audio
    input_i     => pure_overdrive,
    output_o    => boosted_out_combi
  );
  
  -- Threshold preset constant easy assignment to detect signals over threshold. Combinational
  threshold_manager : process
    type natural_vector is array (integer range <>) of natural;
    variable threshold_preset : natural_vector(15 downto 0);
    variable input_abs : natural;
    
    variable difference : natural;
    variable difference_signed, soft : signed(31 downto 0);
    
  begin
    over_threshold <= '0';
    if input_i(0)(31) = '0' then
      input_abs := to_integer(signed(input_i(0)));
    else
      input_abs := to_integer(-signed(input_i(0)));
    end if;
  
    for i in 0 to 15 loop
      threshold_preset(i) := threshold_max-(i*threshold_step);
      difference := input_abs - threshold_preset(i);
      difference_signed := to_signed(difference, 32);
      soft := "00000" & difference_signed(31 downto 5);
      
      if st_fx = i then
        if input_i(0)(31) = '0' and input_abs > threshold_preset(i) then
          over_threshold <= '1';
          clip_value(0) <= std_logic_vector(to_signed(threshold_preset(i), 32)+soft);
        elsif input_i(0)(31) = '1' and input_abs > threshold_preset(i) then
          over_threshold <= '1';
          clip_value(0) <= std_logic_vector(-to_signed(threshold_preset(i), 32)-soft);
        end if;
      end if;
    end loop;
  end process;
  
  pure_overdrive_assign : process (clk_i, rst_i)
    type fx_states is (idle,      -- Waits for input adding 1 cycle delay
                       overdrive, -- Soft-clips
                       amp,       -- Amplifies
                       done);     -- Sends out
    variable state : fx_states;
  begin
    output_rdy_o <= '0';
    
    case state is
      when idle =>
      when overdrive =>
      when amp =>
      when done =>
        output_rdy_o <= '1';
      when others =>
    end case;
  
    if rst_i = '1' then
      pure_overdrive(0) <= (others=>'0');
      boosted_out(0) <= (others=>'0');

      state := idle;
      
    elsif rising_edge(clk_i) then
    
      case state is
        when idle =>
          if input_rdy_i = '1' then
            if st_fx_i /= (st_fx_i'range=>'0') then
              state := overdrive;
            else
              state := done;
            end if;
          end if;
          
        when overdrive =>
          -- Overdriving
          if over_threshold = '1' then
            -- Check for saturation
            if input_i(0)(31) /= clip_value(0)(31) then
              if input_i(0)(31) = '0' then
                pure_overdrive(0) <= max_clip;
              else
                pure_overdrive(0) <= min_clip;
              end if;
            else
              pure_overdrive <= clip_value;
            end if;

          else
            pure_overdrive <= input_i;
          end if;
          
          state := amp;
        
        when amp =>
          boosted_out <= boosted_out_combi;
        
          state := done;
        
        when done =>
          state := idle;
          
        when others =>
      end case;

    end if;
  end process;

end syn;

