-- fifo.vhd ----------------------------------------------------------------------------------
--
-- Fifo buffer. Implemented as a bank register. When fifo is full, new data is ignored; if
-- its empty, readings return non valid values.
--
-- (c) J.M. Mendias
-- Dise�o Autom�tico de Sistemas
-- Facultad de Inform�tica. Universidad Complutense de Madrid
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity fifo is
  generic (
    WIDTH : natural;   -- fifo word width
    DEPTH : natural    -- number of words in fifo
  );
  port (
    rst     : in  std_logic;   -- async reset
    clk     : in  std_logic;   -- clock
    wrE     : in  std_logic;   -- to write, set to one for one cycle
    dataIn  : in  std_logic_vector(WIDTH-1 downto 0);   -- data to write
    rdE     : in  std_logic;   -- to read, set to one for one cycle
    dataOut : out std_logic_vector(WIDTH-1 downto 0);   -- dato a leer
    full    : out std_logic;   -- full fifo indicator
    empty   : out std_logic    -- empty fifo indicator
  );
end fifo;

-------------------------------------------------------------------

library ieee;
use ieee.numeric_std.all;
use work.common.all;

architecture syn of fifo is

  constant maxValue : natural := DEPTH-1;

  type regFileType is array (0 to maxValue) of std_logic_vector(WIDTH-1 downto 0);

  -- Registros
  signal regFile : regFileType;
  signal wrPointer, rdPointer : natural range 0 to maxValue;
  signal isFull : std_logic;
  signal isEmpty : std_logic;
  -- Se�ales  
  signal nextWrPointer, nextRdPointer : natural range 0 to maxValue;
  signal rdFifo  : std_logic;
  signal wrFifo : std_logic;
  
begin

  registerFile :
  process (rst, clk, rdPointer, regFile)
  begin
    dataOut <= regFile(rdPointer);
    if rst='1' then
      regFile <= (others => (others => '0'));
    elsif rising_edge(clk) then
      if wrFifo='1' then
        regFile(wrPointer) <= dataIn;
      end if;
    end if;
  end process;
 
  wrFifo <= wrE and not isFull;
  rdFifo <= rdE and not isEmpty;
  
  nextWrPointer <= 0 when wrPointer=maxValue else wrPointer+1;
  nextRdPointer <= 0 when rdPointer=maxValue else rdPointer+1;
  
  fsmd :
  process (rst, clk) 
  begin     
    if rst='1' then
      wrPointer <= 0;
      rdPointer <= 0;
      isFull    <= '0';
      isEmpty   <= '1';
    elsif rising_edge(clk) then
      if wrFifo='1' then
        isEmpty <= '0';
        wrPointer <= nextWrPointer;
        if nextWrPointer=rdPointer and rdFifo='0' then
          isFull <= '1';
        end if;
      end if;
      if rdFifo='1' then
        isFull <= '0';
        rdPointer <= nextRdPointer;
        if nextRdPointer=wrPointer and wrFifo='0' then
          isEmpty <= '1';
        end if;
      end if;
    end if;
  end process;
 
  full <= isFull;
  empty <= isEmpty;
  
end syn;


