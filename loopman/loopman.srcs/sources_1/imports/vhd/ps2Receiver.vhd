-- ps2Receiver.vhd ---------------------------------------------------------------------------
--
-- PS/2 serial to parallel converter, 1 cycle strobe protocol.
--
-- (c) J.M. Mendias
-- Dise�o Autom�tico de Sistemas
-- Facultad de Inform�tica. Universidad Complutense de Madrid
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity ps2Receiver is
  generic (
    REGOUTPUTS : boolean   -- registered outputs
  );
  port (
    -- host side
    rst        : in  std_logic;   -- reset
    clk        : in  std_logic;   -- clock
    dataRdy    : out std_logic;   -- sets to one for a cycle when data is received
    data       : out std_logic_vector (7 downto 0);  -- received data
    -- PS2 side
    ps2Clk     : in  std_logic;   -- PS/2 interface input clock 
    ps2Data    : in  std_logic    -- PS/2 interface data input
  );
end ps2Receiver;

-------------------------------------------------------------------

use work.common.all;

architecture syn of ps2Receiver is

  signal ps2ClkSync, ps2DataSync, ps2ClkFall: std_logic; 
  signal ps2DataShf: std_logic_vector(10 downto 0);
  signal lastBit, parityOK: std_logic;

begin

  ps2ClkSynchronizer : synchronizer
    generic map ( STAGES => 2, init => '1' )
    port map ( rst => rst, clk => clk, x => ps2Clk, xSync => ps2ClkSync );
    
  ps2DataSynchronizer : synchronizer
    generic map ( STAGES => 2, init => '1' )
    port map ( rst => rst, clk => clk, x => ps2Data, xSync => ps2DataSync );
    
  ps2ClkEdgeDetector : edgeDetector
    port map ( rst => rst, clk => clk, x_n => ps2ClkSync, xFall => ps2ClkFall, xRise => open );
    
  ps2DataShifter:
  process (rst, clk)
  begin
    if rst='1' then
      ps2DataShf <= (others =>'1');    
    elsif rising_edge(clk) then
      if lastBit = '1' then
        ps2DataShf <= (others =>'1');
      elsif ps2ClkFall = '1' then
        ps2DataShf <= ps2DataSync & ps2DataShf(10 downto 1);
      end if;
    end if;
  end process;

  -- XOR between pairs, then XNOT with parity bit
  oddParityCheker :
  parityOK <= ps2DataShf(1) xor ps2DataShf(2) xor ps2DataShf(3) xor ps2DataShf(4) xor 
              ps2DataShf(5) xor ps2DataShf(6) xor ps2DataShf(7) xor ps2DataShf(8)
              xor ps2DataShf(9);

  lastBitCheker :
  lastBit <= not ps2DataShf(0);  
  
  outputRegisters:
  if REGOUTPUTS generate
    process (rst, clk)
    begin
      if rst='1' then
        dataRdy <= '0';
        data <= (others=>'0');
      elsif rising_edge(clk) then
        if parityOk = '1' and lastBit = '1' then
          data <= ps2DataShf(8 downto 1);
          dataRdy <= '1';
        else
          dataRdy <= '0';
        end if;
      end if;
    end process;
  end generate;
 
  outputSignals:
  if not REGOUTPUTS generate
    dataRdy <= parityOk and lastBit;
    data    <= ps2DataShf(8 downto 1);
  end generate;

end syn;
