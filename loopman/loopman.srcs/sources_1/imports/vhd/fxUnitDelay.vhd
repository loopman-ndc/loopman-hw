-- fxUnitDelay.vhd -------------------------------------------------------------------------
--
-- Delay effect.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitDelay is
  generic (
    BRAM_SIZE  : natural := (2**14)
  );
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitDelay;

architecture syn of fxUnitDelay is

  constant BRAM_ADDRESS_LEN  : natural := log2(BRAM_SIZE);
  
  type   ram_type is array (0 to BRAM_SIZE-1) of std_logic_vector(31 downto 0);
  signal br : ram_type;
  attribute ram_style : string;
  attribute ram_style of br : signal is "block";
  
  signal st_fx     : natural;
  signal st_fx_aux : unsigned(BRAM_ADDRESS_LEN-1 downto 0);
  
  signal delay         : int_sample(0 downto 0);
  signal mix_out       : int_sample(0 downto 0);
  signal mix_out_combi : int_sample(0 downto 0);
  
  signal half_delay    : int_sample(0 downto 0);
  signal half_input    : int_sample(0 downto 0);
  
  signal br_write   : std_logic;
  signal br_write_aux : std_logic_vector(0 downto 0);
  signal br_ena     : std_logic;
  signal br_addr    : std_logic_vector(BRAM_ADDRESS_LEN-1 downto 0);
  signal br_dout    : std_logic_vector(31 downto 0);
  
  signal rd_pointer : natural range 0 to BRAM_SIZE-1;
  signal wr_pointer : natural range 0 to BRAM_SIZE-1;
  signal br_pointer : natural range 0 to BRAM_SIZE-1;
  
begin
  
  output_o <= mix_out when st_fx_i /= (st_fx_i'range=>'0') else input_i;
  
  -- Value control
  st_fx_aux(BRAM_ADDRESS_LEN-5 downto 0) <= (others=>'1');
  st_fx_aux(BRAM_ADDRESS_LEN-1 downto BRAM_ADDRESS_LEN-4) <= unsigned(st_fx_i);
  st_fx <= to_integer(st_fx_aux);
  
  br_pointer <= wr_pointer when br_write = '1' else rd_pointer;
  
  block_ram_access : process(clk_i, rst_i)
  begin
    if rising_edge(clk_i) then
      if br_ena = '1' then
        if br_write = '1' then
          br(br_pointer) <= input_i(0);--mix_out(0);
        end if;
        br_dout <= br(br_pointer);
      end if;
    end if;
  end process;
  
  mixer : audioMixerCombi
    port map (
      -- Control
      clipping_o  => open,
      -- Audio
      input0_i    => input_i,
      input1_i    => half_delay,
      mix_o       => mix_out_combi
    );
    
  half_delay(0) <= "0" & delay(0)(31 downto 1) when delay(0)(31) = '0' else
                   "1" & delay(0)(31 downto 1);
    
    
  fx_main_control : process(clk_i, rst_i, input_rdy_i)
    type fx_mix_states is (idle, -- Waits for input adding 1 cycle delay
                           wr,   -- Writes ram
                           rd,   -- Reads ram
                           mix); -- Mixes 
    variable state : fx_mix_states;

  begin
    output_rdy_o <= '0';
    br_write     <= '0';
    br_ena       <= '0';
    
    case state is
      when idle =>
        if input_rdy_i = '1' and st_fx_i = (st_fx_i'range=>'0') then
          output_rdy_o <= '1';
        end if;
      when wr =>
        br_write <= '1';
        br_ena   <= '1';
        output_rdy_o <= '1';
      when rd =>
        br_ena <= '1';
      when mix =>
      when others =>
    end case;
  
    if rst_i = '1' then
      rd_pointer <= 0;
      wr_pointer <= 1;
      mix_out(0) <= (others=>'0');
      delay(0) <= (others=>'0');
    
      state := idle;
      
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if input_rdy_i = '1' and st_fx_i /= (st_fx_i'range=>'0') then
            state := rd;
          end if;
          
        when rd =>
           if rd_pointer < st_fx then       
             rd_pointer <= rd_pointer + 1; 
           else                            
             rd_pointer <= 0;              
           end if; 

          state := mix;
          
        when mix =>
          delay(0) <= br_dout;
          mix_out <= mix_out_combi;
          
          state := wr;
          
        when wr =>
          if wr_pointer < st_fx then
            wr_pointer <= wr_pointer + 1;
          else
            wr_pointer <= 0;
          end if;
          -- mix_out_combi & half_mix_out ready
          
          state := idle;

        when others =>
          state := idle;
      end case;
    end if;
  end process;

end syn;
