-- btReceiver.vhd ----------------------------------------------------------------------------
--
-- Receives control frames via Bluetooth, parses them and generates control signals.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity btReceiver is
  generic (
    OUTPUTS : natural := 2;
    INPUTS  : natural := 2;
    FX_NO   : natural := 4
  );
  port (
    -- System
    rst_i               : in std_logic;
    clk_i               : in std_logic;
    -- Bluetooth        
    bt_rx_i             :  in std_logic;
    -- Control
    t_reset_req_o       : out std_logic;
    -- Master
    t_inc_master_comp_o : out std_logic_vector(OUTPUTS-1 downto 0);
    t_dec_master_comp_o : out std_logic_vector(OUTPUTS-1 downto 0);
    t_set_master_comp_o : out std_logic_vector(OUTPUTS-1 downto 0);
    value_master_comp_o : out std_logic_vector(3 downto 0);
    t_inc_master_vol_o  : out std_logic_vector(OUTPUTS-1 downto 0);
    t_dec_master_vol_o  : out std_logic_vector(OUTPUTS-1 downto 0);
    t_set_master_vol_o  : out std_logic_vector(OUTPUTS-1 downto 0);
    value_master_vol_o  : out std_logic_vector(3 downto 0);
    -- Tracks
    t_rec_o             : out std_logic_vector(7 downto 0);           
    t_play_o            : out std_logic_vector(7 downto 0);           
    t_mute_o            : out std_logic_vector(7 downto 0);           
    t_solo_o            : out std_logic_vector(7 downto 0);           
    t_clear_o           : out std_logic_vector(7 downto 0);           
    t_sync_o            : out std_logic_vector(7 downto 0);
    t_auto_rec_tg_o     : out std_logic_vector(7 downto 0);          
    t_inc_looplen_o     : out std_logic_vector(7 downto 0);           
    t_dec_looplen_o     : out std_logic_vector(7 downto 0);           
    t_set_looplen_o     : out std_logic_vector(7 downto 0);           
    value_looplen_o     : out std_logic_vector(4 downto 0);           
    t_inc_vol_o         : out std_logic_vector(7 downto 0);           
    t_dec_vol_o         : out std_logic_vector(7 downto 0);           
    t_set_vol_o         : out std_logic_vector(7 downto 0);           
    value_vol_o         : out std_logic_vector(3 downto 0);           
    t_inc_odub_o        : out std_logic_vector(7 downto 0);           
    t_dec_odub_o        : out std_logic_vector(7 downto 0);                          
    t_set_odub_o        : out std_logic_vector(7 downto 0);                          
    value_odub_o        : out std_logic_vector(3 downto 0);                          
    t_input_sel_o       : out std_logic_vector((8*INPUTS)-1 downto 0); 
    t_output_sel_o      : out std_logic_vector((8*OUTPUTS)-1 downto 0);
    -- Tempo                                                          
    t_set_tempo_o       : out std_logic;       
    tempo_value_o       : out std_logic_vector(15 downto 0);
    -- FX
    t_fx_toggle_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_enqueue_o      : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_inc_fx_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_dec_fx_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_set_fx_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
    value_fx_o          : out std_logic_vector((8*4)-1 downto 0);    
    t_fx_clear_o        : out std_logic_vector(7 downto 0)     
  );
end btReceiver;

architecture syn of btReceiver is
 
  component rs232Receiver
    generic (
      FREQ     : natural;  -- operation frequency in KHz  
      BAUDRATE : natural   -- transmission speed          
    );
    port (
      -- host side
      rst     : in  std_logic;   -- async reset
      clk     : in  std_logic;   -- clock
      dataRdy : out std_logic;   -- activates for one cycle when input data is ready
      data    : out std_logic_vector (7 downto 0);   -- received data
      -- RS232 side
      RxD     : in  std_logic    -- Serial RS-232 data input
    );
  end component;

  signal byte_in     : std_logic_vector(7 downto 0);
  signal byte_in_rdy : std_logic;
  signal fifo_rd, fifo_wr : std_logic;
  signal frame_to_parse : std_logic_vector(23 downto 0);
  signal frame_buffer : std_logic_vector(15 downto 0);
  signal frame_to_parse_rdy : std_logic;
  
begin

  -- Recieves UART data (8 data bits, 1 stop)
  serial_receiver : rs232Receiver
    generic map (
      FREQ => 75_000,
      BAUDRATE => 115200
    )
    port map (
      rst => rst_i,
      clk => clk_i,
      dataRdy => byte_in_rdy,
      data => byte_in,
      RxD => bt_rx_i
    );
  
  -- Collects bytes to build frames
  collector : process
      type collector_states is (--handshake, -- Waits for handshake to complete
                                receive, -- Receives bytes to build next frame
                                metadata,
                                frame_out); -- Frame ready
      variable state : collector_states;
  
    variable byte_count : unsigned(1 downto 0);
  begin
    frame_to_parse_rdy <= '0';
    
    case state is
      when receive =>
      when metadata =>
      when frame_out =>
        frame_to_parse_rdy <= '1';
      when others =>        
    end case;
  
    if rst_i = '1' then
      frame_buffer <= (others=>'0');
      byte_count := (others=>'0');
      
      state := receive;
    elsif rising_edge(clk_i) then
      case state is
        when receive =>
          -- Ready to receive
          if byte_in_rdy = '1' then
            if byte_count = 0 then
              -- If first byte received is a metadata byte, stop parsing
              -- Else, other bytes in current frame wont be metadata
              if byte_in = BTF_metadata_byte then
                byte_count := (others=>'0');
                
                state := metadata;
              else 
                frame_buffer(15 downto 8) <= byte_in;
                byte_count := byte_count + 1;
              end if;
            elsif byte_count = 1 then
              frame_buffer(7 downto 0) <= byte_in;
              byte_count := byte_count + 1;
            elsif byte_count = 2 then
              frame_to_parse <= frame_buffer & byte_in;
              frame_buffer <= (others=>'0');
              byte_count := (others=>'0');
              
              state := frame_out;
            end if;
          end if;
  
        when metadata =>
           if byte_in_rdy = '1' then
             if byte_in = BTF_metadata_byte then
               state := receive;
             end if;
           end if;
        
        when frame_out =>
          -- Sets '1' to ready bit
  
          state := receive;
          
        when others =>        
      end case;
    end if;
  end process;

  -- Parses frames
  parser : process
    variable mode   : std_logic_vector(4 downto 0); -- Mode (track, fx...)
    variable track  : std_logic_vector(2 downto 0); -- Track (if required)
    variable option : std_logic_vector(7 downto 0); -- Option to alter
    variable value  : std_logic_vector(7 downto 0); -- Option value
    
    variable one_shot_master : std_logic_vector(OUTPUTS-1 downto 0); -- Aux
    variable one_shot_track  : std_logic_vector(7 downto 0); -- Aux
    variable one_shot_fx     : std_logic_vector((8*FX_NO)-1 downto 0); -- Aux
    variable input_sel       : std_logic_vector((8*INPUTS)-1 downto 0); -- Aux
    variable output_sel      : std_logic_vector((8*OUTPUTS)-1 downto 0); -- Aux
    variable track_index     : natural range 0 to 8;
    
  begin
    t_reset_req_o       <= '0';
  
    t_inc_master_comp_o <= (others=>'0');
    t_dec_master_comp_o <= (others=>'0');
    t_set_master_comp_o <= (others=>'0');
    value_master_comp_o <= (others=>'0');
    t_inc_master_vol_o  <= (others=>'0');
    t_dec_master_vol_o  <= (others=>'0');
    t_set_master_vol_o  <= (others=>'0');
    value_master_vol_o  <= (others=>'0');
    
    t_set_tempo_o       <= '0';
    tempo_value_o       <= (others=>'0');
    
    t_rec_o             <= (others=>'0');
    t_play_o            <= (others=>'0');
    t_mute_o            <= (others=>'0');
    t_solo_o            <= (others=>'0');
    t_clear_o           <= (others=>'0');
    t_sync_o            <= (others=>'0');
    t_auto_rec_tg_o     <= (others=>'0');
    t_inc_looplen_o     <= (others=>'0');
    t_dec_looplen_o     <= (others=>'0');
    t_set_looplen_o     <= (others=>'0');
    value_looplen_o     <= (others=>'0');
    t_inc_vol_o         <= (others=>'0');
    t_dec_vol_o         <= (others=>'0');
    t_set_vol_o         <= (others=>'0');
    value_vol_o         <= (others=>'0');
    t_inc_odub_o        <= (others=>'0');
    t_dec_odub_o        <= (others=>'0');
    t_set_odub_o        <= (others=>'0');
    value_odub_o        <= (others=>'0');           
    t_input_sel_o       <= (others=>'0');
    t_output_sel_o      <= (others=>'0');

    t_fx_toggle_o       <= (others=>'0');                               
    t_fx_enqueue_o      <= (others=>'0');
    t_fx_inc_fx_o       <= (others=>'0');
    t_fx_dec_fx_o       <= (others=>'0');
    t_fx_set_fx_o       <= (others=>'0');
    value_fx_o          <= (others=>'0');
    t_fx_clear_o        <= (others=>'0');
    
    mode   := frame_to_parse(23 downto 19);
    track  := frame_to_parse(18 downto 16);
    option := frame_to_parse(15 downto 8);
    value  := frame_to_parse(7 downto 0);
        
    one_shot_master := (others=>'0');
    one_shot_track := (others=>'0');
    one_shot_fx := (others=>'0');
    input_sel := (others=>'0');
    output_sel := (others=>'0');
    track_index := 0;

    track_index := to_integer(unsigned(track));
    one_shot_track(track_index) := '1';
    one_shot_master := one_shot_track(OUTPUTS-1 downto 0);
    one_shot_fx((track_index*FX_NO)+to_integer(unsigned(value))) := '1';
    input_sel((INPUTS*(track_index+1))-1 downto (INPUTS*track_index)) := value(INPUTS-1 downto 0);
    output_sel((OUTPUTS*(track_index+1))-1 downto (OUTPUTS*track_index)) := value(OUTPUTS-1 downto 0);
    
    if frame_to_parse_rdy = '1' then
      case mode is
        when BTF_mode_reset =>
          if track = (track'range=>'0') and
             option = (option'range=>'0') and
             value = (value'range=>'0') then
            t_reset_req_o <= '1';
          end if;
        when BTF_mode_tempo =>
          t_set_tempo_o <= '1';
          tempo_value_o <= option & value;
        when BTF_mode_master =>
          case option is
            when BTF_opt_inc_vol =>
              t_inc_master_vol_o <= one_shot_master;
            when BTF_opt_dec_vol =>
              t_dec_master_vol_o <= one_shot_master;
            when BTF_opt_set_vol =>
              t_set_master_vol_o <= one_shot_master;
              value_master_vol_o <= value(3 downto 0);
            when BTF_opt_inc_comp =>
              t_inc_master_comp_o <= one_shot_master;
            when BTF_opt_dec_comp =>
              t_dec_master_comp_o <= one_shot_master;
            when BTF_opt_set_comp =>
              t_set_master_comp_o <= one_shot_master;
              value_master_comp_o <= value(3 downto 0);
            when others => 
          end case;
        when BTF_mode_track =>
          case option is
            when BTF_opt_rec =>
              t_rec_o <= one_shot_track;
            when BTF_opt_play =>
              t_play_o <= one_shot_track;
            when BTF_opt_mute =>
              t_mute_o <= one_shot_track;
            when BTF_opt_solo =>
              t_solo_o <= one_shot_track;
            when BTF_opt_clear =>
              t_clear_o <= one_shot_track;
            when BTF_opt_sync =>
              t_sync_o <= one_shot_track;
            when BTF_opt_auto_rec =>
              t_auto_rec_tg_o <= one_shot_track;
            when BTF_opt_inc_looplen =>
              t_inc_looplen_o <= one_shot_track;
            when BTF_opt_dec_looplen =>
              t_dec_looplen_o <= one_shot_track;
            when BTF_opt_set_looplen =>
              t_set_looplen_o <= one_shot_track;
              value_looplen_o <= value(4 downto 0);
            when BTF_opt_inc_vol =>
              t_inc_vol_o <= one_shot_track;
            when BTF_opt_dec_vol =>
              t_dec_vol_o <= one_shot_track;
            when BTF_opt_set_vol =>
              t_set_vol_o <= one_shot_track;
              value_vol_o <= value(3 downto 0);
            when BTF_opt_inc_odub =>
              t_inc_odub_o <= one_shot_track;
            when BTF_opt_dec_odub =>
              t_dec_odub_o <= one_shot_track;
            when BTF_opt_set_odub =>
              t_set_odub_o <= one_shot_track;
              value_odub_o <= value(3 downto 0);
            when BTF_opt_input_sel =>
              t_input_sel_o <= input_sel;
            when BTF_opt_output_sel =>
              t_output_sel_o <= output_sel;
            when others =>
          end case;
        when BTF_mode_fx =>
          if option(7) = '1' then
            -- Set value
            t_fx_set_fx_o <= one_shot_fx;
            value_fx_o(((track_index+1)*4)-1 downto track_index*4) <= option(3 downto 0);
          else
            case option is
              when BTF_fxopt_clear =>
                t_fx_clear_o <= one_shot_track;
              when BTF_fxopt_enqueue =>
                t_fx_enqueue_o <= one_shot_fx;
              when BTF_fxopt_enable =>
                t_fx_toggle_o <= one_shot_fx;
              when others =>
            end case;
          end if;
        when others =>
      end case;
    end if;
  end process;

end syn;
