-- overdubber.vhd --------------------------------------------------------------------
--
-- Mixes read sample with sample to write, so data is not overwritten. In other words,
-- this entity does overdub.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity overdubber is
  port (
    new_sample_i : in  std_logic_vector(31 downto 0);
    old_sample_i : in  std_logic_vector(31 downto 0);
    overdub      : in  std_logic_vector(3 downto 0);
    mix_o        : out std_logic_vector(31 downto 0)
  );
end overdubber;

architecture syn of overdubber is

signal dimmed_old_sample : int_sample(0 downto 0);
signal old_sample_aux    : int_sample(0 downto 0);
signal new_sample        : int_sample(0 downto 0);
signal added_sample      : int_sample(0 downto 0);
signal mix               : int_sample(0 downto 0);

begin
  
  old_sample_aux(0) <= old_sample_i;
  new_sample(0) <= new_sample_i;
  mix_o <= mix(0);
  
  old_sample_dimmer : audioDimmer
  port map (volume_i => std_logic_vector(overdub),
            input_i  => old_sample_aux,
            output_o => dimmed_old_sample);
           
  mixer : audioMixerCombi
  port map (
    -- Control
    clipping_o => open,
    -- Audio
    input0_i   => dimmed_old_sample,
    input1_i   => new_sample,
    mix_o      => mix
  );  

end syn;
