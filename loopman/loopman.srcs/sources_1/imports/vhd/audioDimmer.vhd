-- audioDimmer.vhd --------------------------------------------------------------------------
--
-- Reduces sample amplitude with fixed point multiplications. Combinational.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity audioDimmer is
  port (
    -- Control
    volume_i    : in  std_logic_vector(3 downto 0);
    -- Audio
    input_i     : in  int_sample(0 downto 0);
    output_o    : out int_sample(0 downto 0)
  );
end audioDimmer;

architecture syn of audioDimmer is

  signal input_aux    : signed(31 downto 0);
  signal multiply_aux : signed(37 downto 0);
  signal factor       : signed(5 downto 0);
  signal volume_aux   : std_logic_vector(5 downto 0);

begin

  input_aux <= signed(input_i(0));
  
  volume_aux <= "00" & volume_i;
  
  factor <= (others=>'0') when volume_i = "0000" else
            signed(volume_aux) + 1;
               
  multiply_aux <= input_aux * factor;
  
  output_o(0) <= std_logic_vector(multiply_aux(35 downto 4));

end syn;
