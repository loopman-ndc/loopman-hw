-- track.vhd ---------------------------------------------------------------------------------
--
-- Tracks manage playback, recording and synchronization. They have control and status
-- signals, as user directly works with them. Memory (DDR2) access is also performed in here.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity track is
  generic (
    OUTPUTS : natural := 2;
    INPUTS  : natural := 2
  );
  port (
    -- System
    clk_i           : in std_logic;
    rst_i           : in std_logic;
    -- Track control
    t_rec_i         : in std_logic;
    t_play_i        : in std_logic;
    t_mute_i        : in std_logic;
    t_solo_i        : in std_logic;
    not_alone_i     : in std_logic; -- Other tracks are in solo mode
    t_clear_i       : in std_logic;
    t_sync_i        : in std_logic;
    t_auto_rec_tg_i : in std_logic;
    t_inc_looplen_i : in std_logic;
    t_dec_looplen_i : in std_logic;
    t_set_looplen_i : in std_logic;
    value_looplen_i : in std_logic_vector(4 downto 0);
    t_inc_vol_i     : in std_logic;
    t_dec_vol_i     : in std_logic;
    t_set_vol_i     : in std_logic;
    value_vol_i     : in std_logic_vector(3 downto 0);
    t_inc_odub_i    : in std_logic;
    t_dec_odub_i    : in std_logic;
    t_set_odub_i    : in std_logic;
    value_odub_i    : in std_logic_vector(3 downto 0);
    t_input_sel_i   : in std_logic_vector(INPUTS-1 downto 0); 
    t_output_sel_i  : in std_logic_vector(OUTPUTS-1 downto 0);
    -- Tempo / sync / coordination
    t_tempo_i        : in std_logic;
    others_working_i : in std_logic; -- Other tracks are playing or recording
    t_tempo_sync_o   : out std_logic;
    -- Track status
    st_rec_o        : out std_logic;
    st_play_o       : out std_logic;
    st_mute_o       : out std_logic;
    st_solo_o       : out std_logic;
    st_sync_o       : out std_logic;
    st_auto_rec_o   : out std_logic;
    t_ch_status_o   : out std_logic;
    st_beat_o       : out std_logic_vector(4 downto 0);
    t_ch_beat_o     : out std_logic;
    st_looplen_o    : out std_logic_vector(4 downto 0);
    t_ch_looplen_o  : out std_logic;
    st_vol_o        : out std_logic_vector(3 downto 0);
    t_ch_vol_o      : out std_logic;
    st_odub_o       : out std_logic_vector(3 downto 0);
    t_ch_odub_o     : out std_logic;
    st_outlevel_o   : out std_logic_vector(3 downto 0); -- waveform to display, to show output level
    t_ch_outlevel_o : out std_logic;
    t_ch_in_sel_o   : out std_logic;
    t_ch_out_sel_o  : out std_logic;
    -- Memory interface
    addr_o          : out std_logic_vector(21 downto 0);        -- current memory address
    loop_audio_i    : in  std_logic_vector(31 downto 0);        -- recorded audio
    rec_audio_o     : out std_logic_vector(31 downto 0);        -- audio to record
    odub_o          : out std_logic_vector(3 downto 0);         -- overdub (amount of old sample in new sample)
    won_o           : out std_logic;                            -- write enable
    op_o            : out std_logic;                            -- operation enable (like cs)
    -- Audio IO
    audio_i         : in  std_logic_vector(31 downto 0);        -- audio in from selected external inputs or tracks
    audio_o         : out std_logic_vector(31 downto 0);        -- audio out to selected outputs
    in_sel_o        : out std_logic_vector(INPUTS-1 downto 0);  -- selected inputs
    out_sel_o       : out std_logic_vector(OUTPUTS-1 downto 0); -- selected outputs
    input_rdy_i     : in  std_logic;
    output_rdy_o    : out std_logic
  );
end track;

architecture syn of track is

  component audioMixer is
    generic ( 
      INPUTS : natural := 2
    );
    port (
      -- System
      rst_i       : in std_logic;
      clk_i       : in std_logic;
      -- Control
      input_rdy_i : in  std_logic;
      sel_i       : in  std_logic_vector(INPUTS-1 downto 0);
      mix_rdy_o   : out std_logic;
      clipping_o  : out std_logic;
      -- Audio
      input_i     : in  int_sample(INPUTS-1 downto 0);
      mix_o       : out int_sample(0 downto 0)
    );
  end component;

  -- Status and control
  signal rec, rec_req, rec_sync    : std_logic;
  signal auto_rec, t_auto_rec      : std_logic;
  signal play, play_req, play_sync : std_logic;
  signal tempo_window              : std_logic;
  signal mute                      : std_logic;
  signal solo                      : std_logic;
  signal sync                      : std_logic;
  signal beat                      : unsigned(4 downto 0);
  signal looplen                   : unsigned(4 downto 0);
  signal vol                       : unsigned(3 downto 0);
  signal odub                      : unsigned(3 downto 0);
  signal user_set_odub             : unsigned(3 downto 0);
  signal outlevel, outlevel_combi  : std_logic_vector(3 downto 0);
  signal inlevel                   : std_logic_vector(3 downto 0);
  signal in_sel                    : std_logic_vector(INPUTS-1 downto 0);
  signal out_sel                   : std_logic_vector(OUTPUTS-1 downto 0);
  
  -- Status changes
  signal t_ch_rec  : std_logic;
  signal t_ch_play : std_logic;
  signal t_ch_mute : std_logic;
  signal t_ch_solo : std_logic;
  signal t_ch_sync : std_logic;
  signal t_ch_auto_rec : std_logic;
  
  -- Mem addresses
  signal address           : unsigned(21 downto 0); -- Current R/W address
  signal last_rec_address  : unsigned(21 downto 0); -- Last recorded address, no data beyond
  signal first_rec_address : unsigned(21 downto 0); -- First recorded address, no data before (only used for delayed starts)
  signal pre_address       : unsigned(21 downto 0); -- Addresses generated to sync delayed start
  signal valid_mem_out     : std_logic;             -- Whether memory output is valid or not
  
  -- Mem access
  signal mem_op            : std_logic;
  signal mem_won           : std_logic;
  signal mem_loop_audio    : int_sample(0 downto 0);
  signal mem_rec_audio     : int_sample(0 downto 0);
  
  -- Mixing
  signal mixed_sample         : int_sample(0 downto 0); -- Input + looped audio
  signal mixer_input          : int_sample(1 downto 0); -- Input and looped audio array
  signal vol_set_sample_combi : int_sample(0 downto 0); -- Volume correction applied to mixed sample
  signal vol_set_sample       : int_sample(0 downto 0); -- Volume correction applied to mixed sample (reg)
  signal solo_mute_sample     : int_sample(0 downto 0); -- Volume correction applied to mixed sample
  signal mixer_rdy            : std_logic;
  signal mixer_in_rdy         : std_logic;
  
  signal output_rdy           : std_logic;
  
  -- Aux (VHDL type conversion)
  signal audio_aux         : int_sample(0 downto 0);

begin
  
  -- Memory interface connections ----
  addr_o      <= std_logic_vector(address);    
  --loop_audio_i -- Assigned in memory_access process
  rec_audio_o <= mem_rec_audio(0); -- Audio to be recorded
  odub_o      <= std_logic_vector(odub);         
  won_o       <= mem_won;
  op_o        <= mem_op;
  ------------------------------------
  
  -- State connections ---------------
  st_rec_o      <= rec;
  st_play_o     <= play;
  st_mute_o     <= mute;
  st_solo_o     <= solo;
  st_sync_o     <= sync;
  st_auto_rec_o <= auto_rec;
  st_beat_o     <= std_logic_vector(beat);
  st_looplen_o  <= std_logic_vector(looplen);
  st_vol_o      <= std_logic_vector(vol);
  st_odub_o     <= std_logic_vector(user_set_odub);
  st_outlevel_o <= outlevel;
  -- Changes notifier
  t_ch_status_o <= t_ch_rec or t_ch_play or t_ch_mute or t_ch_solo or t_ch_sync or t_ch_auto_rec;
--  t_ch_beat_o     <= ; -- Asigned in their processes
--  t_ch_looplen_o  <= ;
--  t_ch_vol_o      <= ;
--  t_ch_odub_o     <= ;
--  t_ch_outlevel_o <= ;
--  t_ch_in_sel_o   <= ;
--  t_ch_out_sel_o  <= ;
  ------------------------------------
  
  -- Audio IO connections ------------
  --audio_i -- Connected to mixer and memory input
  audio_o         <= solo_mute_sample(0);
  in_sel_o        <= in_sel;
  out_sel_o       <= out_sel;
  --input_rdy_i -- Connected in track_flow
  output_rdy_o    <= output_rdy; -- in track_flow, when state is "done" its set to '1'
  ------------------------------------

  -- Input/output selection-----------
  IO_sel_control : process (rst_i, clk_i)
  begin
    if rst_i = '1' then
      in_sel <= (others=>'0');
      out_sel <= (others=>'0');
      t_ch_in_sel_o <= '0';
      t_ch_out_sel_o <= '0';
    elsif rising_edge(clk_i) then
      t_ch_in_sel_o <= '0';
      t_ch_out_sel_o <= '0';
      
      for i in 0 to INPUTS-1 loop
        if t_input_sel_i(i) = '1' then
          in_sel(i) <= not in_sel(i);
          t_ch_in_sel_o <= '1';
        end if;
      end loop;

      for i in 0 to OUTPUTS-1 loop
        if t_output_sel_i(i) = '1' then
          out_sel(i) <= not out_sel(i);
          t_ch_out_sel_o <= '1';
        end if;
      end loop;
    end if;
  end process;
  ------------------------------------
 
  -- Volume --------------------------
  vol_control : process (rst_i, clk_i)
  begin
    if rst_i = '1' then
      vol <= "1111";
    elsif rising_edge(clk_i) then
      t_ch_vol_o <= '0';
      
      if t_set_vol_i = '1' then
        vol <= unsigned(value_vol_i);
        t_ch_vol_o <= '1';
      elsif t_inc_vol_i = '1' and vol < "1111" then
        vol <= vol + 1;
        t_ch_vol_o <= '1';
      elsif t_dec_vol_i = '1' and vol > "0000" then
        vol <= vol - 1;
        t_ch_vol_o <= '1';
      end if;
    end if;
  end process;
  
  
  outlevel_combi_set : outlevelIndicator
  generic map (
    threshold_step => 8_000_000 --500_000
  )
  port map (
    sample_i   => vol_set_sample,
    outlevel_o => outlevel_combi
  );
  
  outlevel_segmentated : process (rst_i, clk_i)
    type outlevel_states is (idle,
                             change,
                             reset); 
    variable state : outlevel_states;
  
    constant max_count : natural := 4095;
    variable count : natural range 0 to 4095;
    variable max_outlevel : unsigned(3 downto 0);
  begin
    if rst_i = '1' then
      outlevel <= (others=>'0');
      t_ch_outlevel_o <= '0';
      count := 0;
      max_outlevel := (others=>'0');
      
      state := idle;
    elsif rising_edge(clk_i) then
      t_ch_outlevel_o <= '0';
    
      case state is
        when idle =>
          if play = '1' or in_sel /= (in_sel'range=>'0') then
            state := change;
          end if;
          
        when change =>
          if output_rdy = '1' then
            -- To reduce number of toggles of outlevel
            if count < max_count then
              count := count + 1;
              if max_outlevel < unsigned(outlevel_combi) then
                max_outlevel := unsigned(outlevel_combi);
              end if;
            else
              outlevel <= std_logic_vector(max_outlevel);
              count := 0;
              max_outlevel := (others=>'0');
              t_ch_outlevel_o <= '1';
            end if;
          end if;
          
          -- This disables outlevel retransmission when outlevel is always 0
          -- Changes with 1 cycle delay, to ensure that last "outlevel = 0" is sent
          if play = '0' and in_sel = (in_sel'range=>'0') then
            state := reset;
          end if;
        
        when reset =>
          outlevel <= (others=>'0');
          count := 0;                
          t_ch_outlevel_o <= '1';    
          
          state := idle;
          
        when others =>
      end case;
    
    end if;
  end process;                  
  
  vol_set : audioDimmer
  port map (volume_i => std_logic_vector(vol),
            input_i  => mixed_sample,
            output_o => vol_set_sample_combi);
            
  solo_mute_control : process (rst_i, clk_i, vol_set_sample, mute, solo, not_alone_i)
  begin
    
    solo_mute_sample <= vol_set_sample;
    
    if mute = '1' then
      solo_mute_sample(0) <= (others=>'0');
    elsif solo = '0' and not_alone_i = '1' then
      solo_mute_sample(0) <= (others=>'0');
    end if;
  
    if rst_i = '1' then
      solo <= '0';
      mute <= '0';
      t_ch_mute <= '0';
      t_ch_solo <= '0';
    elsif rising_edge(clk_i) then
      t_ch_mute <= '0';
      t_ch_solo <= '0';
    
      if t_mute_i = '1' then
        mute <= not mute;
        t_ch_mute <= '1';
      end if;
      if t_solo_i = '1' then
        solo <= not solo;
        t_ch_solo <= '1';
      end if;
    end if;
  end process ;
  ------------------------------------

  -- Tempo and user control ----------
  lenControl : process (rst_i, clk_i)
    variable first_rec_len_set : std_logic;
  begin
    if rst_i = '1' then
      looplen <= (others=>'1');
      first_rec_len_set := '1';
      t_ch_looplen_o <= '0';
    elsif rising_edge(clk_i) then
      t_ch_looplen_o <= '0';
      
      if t_clear_i = '1' then
        looplen <= (others=>'1');
        first_rec_len_set := '1';
        t_ch_looplen_o <= '1';
      end if;
    
      if t_set_looplen_i = '1' then
        looplen <= unsigned(value_looplen_i);
        t_ch_looplen_o <= '1';
      elsif t_inc_looplen_i = '1' and looplen < "11111" then
        looplen <= looplen + 1;
        t_ch_looplen_o <= '1'; 
      elsif t_dec_looplen_i = '1' and looplen > "00000" then
        looplen <= looplen - 1;
        t_ch_looplen_o <= '1';
      end if;
      
      -- First recording sets loop length
      if beat = looplen and t_tempo_i = '1' and rec = '1' and first_rec_len_set = '1' then
        -- Loop full, loop length (looplen) set to max
        first_rec_len_set := '0';
      elsif t_rec_i = '1' and rec = '1' and first_rec_len_set = '1' then
        if tempo_window = '1' then
          looplen <= beat - 1;
        else
          looplen <= beat;
        end if;
        first_rec_len_set := '0';
        t_ch_looplen_o <= '1';
      end if;
      
    end if;
  end process;

  beatCounter : process (rst_i, clk_i)
    type beat_states is (idle,
                         count,
                         reset); 
    variable state : beat_states;
    
    variable past_play : std_logic; -- Those for t_ch_beat_o
    variable past_rec  : std_logic;
    variable dirty  : std_logic;
  begin
    if rst_i = '1' then
      beat <= (others=>'0');
      t_ch_beat_o <= '0';
      dirty := '0';

      state := idle;
     
    elsif rising_edge(clk_i) then
      t_ch_beat_o <= '0';
      
      case state is
        when idle =>
          if (play = '1' and last_rec_address /= (last_rec_address'range=>'0')) or rec = '1' then
            -- Beat counts only if there is something that has to play or it is recording
            if t_tempo_i = '1' then
              state := count;
            end if;
          elsif dirty = '1' and play = '0' and rec = '0' then
            state := reset;
          end if;
          
        when count =>
          if beat < looplen then
            beat <= beat + 1;
            t_ch_beat_o <= '1';
          else
            beat <= (others=>'0');
            t_ch_beat_o <= '1';
          end if;
          
          dirty := '1';
          
          state := idle;
        
        when reset =>
          beat <= (others=>'0');
          t_ch_beat_o <= '1';
          dirty := '0';
          
          state := idle;
        
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
  recPlaySync : process (rst_i, clk_i, sync, others_working_i, rec, play, t_rec_i, t_play_i)
    variable tempo_window_count : natural;
    variable tempo_window_max   : natural := 12500000; -- 0.1666 sec
  begin
    t_tempo_sync_o <= '0';
    -- Request tempo sync so you can start recording whenever you want
    -- Sync on, and no other tracks are playing or recording, not even this one. When rec or play tick comes, sync tempo
    if sync = '1' and others_working_i = '0' and rec = '0' and play = '0' and (t_rec_i = '1' or t_play_i = '1') then
      t_tempo_sync_o <= '1';
    end if;
  
    if rst_i = '1' then
      sync <= '1';
      play_sync <= '0';
      play_req <= '0';
      rec_sync <= '0';
      rec_req <= '0';
      tempo_window <= '0';
      tempo_window_count := 0;
      t_ch_rec  <= '0';
      t_ch_play <= '0';
      t_ch_sync <= '0';
      
    elsif rising_edge(clk_i) then
      t_ch_rec  <= '0';
      t_ch_play <= '0';
      t_ch_sync <= '0';

      if t_clear_i = '1' then
        play_sync <= '0';
        play_req <= '0';
        rec_sync <= '0';
        rec_req <= '0';
        t_ch_rec  <= '1';
        t_ch_play <= '1';
      end if;
      
      if t_sync_i = '1' then
        sync <= not sync;
        t_ch_sync <= '1';
      end if;
      
      if t_play_i = '1' then
        play_req <= not play_req;
        t_ch_play <= '1';
      end if;
      
      if t_rec_i = '1' then
        rec_req <= not rec_req;
        t_ch_rec  <= '1';
      elsif rec = '0' and t_auto_rec = '1' then
        rec_req <= '1';
        t_ch_rec  <= '1';
      end if;
      
      if play_req = '1' and play_sync = '0' and play = '0' then
        if tempo_window = '1' then
          play_sync <= '1';
          t_ch_play  <= '1';
        end if;
      elsif play_req = '0' then
        play_sync <= '0';
      end if;
      
      if rec_req = '1' and rec_sync = '0' and rec = '0'  then
        if tempo_window = '1' then
          rec_sync <= '1';
          t_ch_rec  <= '1';
        end if;
      elsif rec_req = '0' then
        rec_sync <= '0';
      end if;
      
      if t_tempo_i = '1' then
        tempo_window <= '1';
        tempo_window_count := 0;
      elsif tempo_window = '1' and tempo_window_count < tempo_window_max then
        tempo_window_count := tempo_window_count + 1;
      elsif tempo_window = '1' then
        tempo_window <= '0';
      end if;
    end if;
  end process;

  -- Sync when sync is enabled  
  play <= play_req when sync = '0' else play_sync;
  rec  <= rec_req  when sync = '0' else rec_sync;
  
  audio_aux(0) <= audio_i;
  
  inlevel_combi_set : outlevelIndicator
    port map (
    sample_i   => audio_aux,
    outlevel_o => inlevel
  );
  
  input_detection : process (rst_i, clk_i)
      type autorec_states is (count,    -- Waits for audio input
                              done);    -- Audio flow throught track complete
      variable state : autorec_states;
    variable sample_count : unsigned(4 downto 0);
  begin
    t_auto_rec <= '0';

    case state is
      when count =>
      when done =>
        t_auto_rec <= '1';
      when others =>
    end case;
  
    if rst_i = '1' then
      auto_rec <= '0';
      sample_count := (others=>'0');
      t_ch_auto_rec <= '0';
      
      state := count;
      
    elsif rising_edge(clk_i) then
      t_ch_auto_rec <= '0';
    
      if t_auto_rec_tg_i = '1' then
        auto_rec <= not auto_rec;
        t_ch_auto_rec <= '1';
      end if;
      
      case state is
        when count =>
          if input_rdy_i = '1' and auto_rec = '1' then
            if unsigned(inlevel) >= "0001" then
              sample_count := sample_count + 1;
            else
              sample_count := (others=>'0');
            end if;
          elsif auto_rec = '0' then
            sample_count := (others=>'0');
          end if;
          
          if sample_count > 10 then
            sample_count  := (others=>'0');
            auto_rec <= '0';
            t_ch_auto_rec <= '1';
            
            state := done;
          end if;

        when done =>
          state := count;
        
        when others =>
      end case;
    end if;
  end process;
  
  ------------------------------------
  
  -- Memory interface --
  address_gen : process (rst_i, clk_i, play)
    type address_states is (idle, -- Waits
                            pre, -- Starts generating addresses (that wont be accessed) for delayed start
                            playback, -- System recording or playing or both
                            update, -- Update addresses
                            clear); -- System clear
    variable tempo_detected : std_logic;
    variable state : address_states;
  begin

    case state is
      when idle =>
      when pre =>
      when playback =>
      when update =>
      when clear =>
      when others =>
    end case;
  
    if rst_i = '1' then
      address <= (others=>'0');
      last_rec_address <= (others=>'0');
      first_rec_address <= (others=>'1');
      pre_address <= (others=>'0');
      valid_mem_out <= '0';
      tempo_detected := '0';
      state := idle;
      
    elsif rising_edge(clk_i) then
      if (play = '1' or rec = '1') and t_tempo_i = '1' and beat = looplen then
        tempo_detected := '1';
      end if;
    
      case state is
        when idle =>
          if t_clear_i = '1' then
            state := clear;
          elsif (play = '1' and last_rec_address /= (last_rec_address'range=>'0')) or rec = '1' then
            -- Address counts only if there is something that has to play or it is recording
            address <= (others=>'0');
            state := playback;
          elsif sync = '1' and tempo_window = '1' and others_working_i = '0' then
            pre_address <= (others=>'0');
            state := pre;
          end if;
          
        when pre =>
          if t_clear_i = '1' then
            state := clear;
          elsif (play = '1' and last_rec_address /= (last_rec_address'range=>'0')) or rec = '1' then
            -- Address counts only if there is something that has to play or it is recording
            address <= pre_address;
            state := playback;
          elsif tempo_window = '0' then
            -- Tempo window expired
            pre_address <= (others=>'0');
            state := idle;
          elsif input_rdy_i = '1' then
            pre_address <= pre_address + 1;
          end if;
          
        when playback =>
          if t_clear_i = '1' then
            state := clear;
          elsif play = '0' and rec = '0' then
            address <= (others=>'0');
            state := idle;
          elsif input_rdy_i = '1' then
            -- Calculate if current read address is valid (has been written before)
            valid_mem_out <= '0';
            if play = '1' then
              if last_rec_address - 4 >= address and
                 last_rec_address /= "000000000000000000000" and
                 first_rec_address + 4 <= address and
                 first_rec_address /= "111111111111111111111" then
                valid_mem_out <= '1';
              end if;
            end if;

            state := update;
          end if;
          
        when update =>
          if t_clear_i = '1' then
            state := clear;
          else 
            -- If loop is full, reset
            -- tempo_detected used instead of t_tempo_i as pulse may come in other state
            if (beat = 0 and tempo_detected = '1') or (beat = looplen and t_tempo_i = '1') then
              address <= (others=>'0');
              tempo_detected := '0';
            else
              address <= address + 1;
            end if;
            
            -- Update valid address range
            if rec = '1' then
              -- Update valid address range
              if last_rec_address < address then
                -- Recorded after last
                last_rec_address <= address;
              end if;
              if first_rec_address > address then
                -- Recorded before first
                first_rec_address <= address;
              end if;
            end if;
    
            state := playback;
          end if;
        
        when clear =>
          address <= (others=>'0');
          last_rec_address <= (others=>'0');
          first_rec_address <= (others=>'1');
          pre_address <= (others=>'0');
          tempo_detected := '0';
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
  -- Main FSM
  track_flow : process (rst_i, clk_i, input_rdy_i, play, rec, mixer_rdy)
    type track_states is (idle,     -- Waits for audio input
                          data_out, -- Memory access done
                          mix,      -- Mix looped audio with track input
                          dim,      -- Volume set
                          done);    -- Audio flow throught track complete
    variable state : track_states;
    
  begin
    mem_op <= '0';
    mem_won <= '0';
    mixer_in_rdy <= '0';
    output_rdy <= '0';
    
    case state is
      when idle =>
        if input_rdy_i = '1' then
          if play = '1' then
            mem_op <= '1';
          end if;
          
          if rec = '1' then
            mem_op <= '1';
            mem_won <= '1';
          end if;
        end if;
        
      when data_out =>

      when mix =>
        if mixer_rdy = '0' then
          mixer_in_rdy <= '1';
        end if;
      
      when dim =>
      
      when done =>
        output_rdy <= '1';
      
      when others =>
    end case;
  
    if rst_i = '1' then
      mem_loop_audio(0) <= (others=>'0');
      mem_rec_audio(0) <= (others=>'0');
      vol_set_sample(0) <= (others=>'0');
      state := idle;
      
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if input_rdy_i = '1' then
            if play = '1' then
              state := data_out;
            else
              -- No audio to loop
              mem_loop_audio(0) <= (others=>'0');
            end if;
            
            if rec = '1' then
              mem_rec_audio(0) <= audio_i;
              state := data_out;
            else
              -- No audio to record
              mem_rec_audio(0) <= (others=>'0');
            end if;
            
            -- If no rec nor play, keep idle
            if play = '0' and rec = '0' then
              -- If there is no need to access memory, mix
              state := mix;
            end if;
          end if;

        when data_out =>
          if valid_mem_out = '1' and play = '1' then
            -- Audio from RAM stored in reg
            mem_loop_audio(0) <= loop_audio_i;
          else
            mem_loop_audio(0) <= (others=>'0');
          end if;
  
          state := mix;
          
        when mix =>
          if mixer_rdy = '1' then
            state := dim;
          end if;
          
        when dim =>
          vol_set_sample <= vol_set_sample_combi;
          state := done;
                          
        when done =>
          state := idle;
      
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
  -- Mixer must mix looped audio from RAM and audio input
  mixer_input <= mem_loop_audio & audio_i;
  
  odubControl : process (rst_i, clk_i, first_rec_address, last_rec_address, address, user_set_odub)
  begin
    -- Overdub should only be actived when something was recorded first
    if last_rec_address - 4 >= address and last_rec_address /= "000000000000000000000" and
       first_rec_address + 4 <= address and first_rec_address /= "111111111111111111111" then
      odub <= user_set_odub;
    else
      odub <= (others=>'0');
    end if;
  
    if rst_i = '1' then
      user_set_odub <= "1111";
      t_ch_odub_o <= '0';
    elsif rising_edge(clk_i) then
      t_ch_odub_o <= '0';
    
      if t_set_odub_i = '1' then
        user_set_odub <= unsigned(value_odub_i);
        t_ch_odub_o <= '1';
      elsif t_inc_odub_i = '1' and user_set_odub < "1111" then
        user_set_odub <= user_set_odub + 1;
        t_ch_odub_o <= '1';
      elsif t_dec_odub_i = '1' and user_set_odub > "0000" then
        user_set_odub <= user_set_odub - 1;
        t_ch_odub_o <= '1';
      end if;
    end if;
  end process;
  ------------------------------------

  -- Audio mixing --------------------
  mixer : audioMixer
  generic map (INPUTS => 2)
  port map (
    -- System
    rst_i       => rst_i,
    clk_i       => clk_i,
    -- Control  
    input_rdy_i => mixer_in_rdy,
    sel_i       => "11",
    mix_rdy_o   => mixer_rdy,
    clipping_o  => open, -- Wont happen at this point
    -- Audio
    input_i     => mixer_input,
    mix_o       => mixed_sample
  );

end syn;
