-- inSampleShifter.vhd -------------------------------------------------------------------------------
--
-- Reads I2S serial input to build samples.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity inSampleShifter is
  generic (
    WIDTH   : natural := 24 -- sample width
  );
  port ( 
    -- Control
    rst_i       : in  std_logic;  -- async reset
    clk_75_i    : in  std_logic;  -- clock      
    -- IO
    input0_o    : out std_logic_vector(WIDTH-1 downto 0);  -- sample from AudioCodec   
    input1_o    : out std_logic_vector(WIDTH-1 downto 0);  -- sample from AudioCodec 
    -- IIS
    channel_i   : in  std_logic;              -- channel of serial input       
    bit_num_i   : in  unsigned(4 downto 0);   -- bit being received            
    clk_cycle_i : in  unsigned(4 downto 0);   -- cycle number for current bit  
    sdin_i      : in  std_logic               -- serial data from ADC          
  );
end inSampleShifter;

---------------------------------------------------------------------

architecture syn of inSampleShifter is

begin

  shifter : process (rst_i, clk_75_i)
    variable sample_r : std_logic_vector(23 downto 0);
    variable sample_l : std_logic_vector(23 downto 0);
  begin
    input0_o <= sample_r(23 downto 24-WIDTH);
    input1_o <= sample_l(23 downto 24-WIDTH);
    
    if rst_i = '1' then
      sample_r := (others=>'0');
      sample_l := (others=>'0');
    elsif rising_edge(clk_75_i) then
      if bit_num_i > 0 and bit_num_i < 25 and clk_cycle_i = 12 then --I2S
--      if bit_num_i > 6 and bit_num_i < 31 and clk_cycle_i = 12 then -- Right-justified
        -- Reads sample
        if channel_i = '0' then
          sample_r := sample_r(22 downto 0) & sdin_i;
        else
          sample_l := sample_l(22 downto 0) & sdin_i;
        end if;
      end if;
    end if;
  end process;

end syn;
