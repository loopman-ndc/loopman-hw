-- vgaMaster.vhd -----------------------------------------------------------------------------
--
-- Generates graphics for an output channel.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vgaMaster is
  generic(
    PLACE : natural 
  );
  port (
    -- VGA
    line  : in  unsigned(9 downto 0); -- line being sweep   
    pixel : in  unsigned(9 downto 0); -- pixel being sweep  
    color : out std_logic_vector(11 downto 0);
    -- Estado
    isMute   : in std_logic;
    isVol    : in std_logic_vector(3 downto 0);
    mixLevel : in std_logic_vector(3 downto 0)
  );
end vgaMaster;

architecture syn of vgaMaster is

  signal border   : std_logic_vector(11 downto 0);
  signal volumen  : std_logic_vector(11 downto 0);
  signal salida   : std_logic_vector(11 downto 0);

  signal muteC  : std_logic_vector(11 downto 0);

  signal mute  : std_logic_vector(11 downto 0);

  signal auxVolHeight : unsigned(8 downto 0);
  signal auxLevel : unsigned(8 downto 0);

  signal pixel2 : unsigned(8 downto 0);
  signal line2  : unsigned(8 downto 0);

begin

  pixel2 <= pixel(9 downto 1);
  line2  <= line(9 downto 1);

  auxVolHeight <= '0' & unsigned(not isVol) & "0000"; -- (208+(7-unsigned(isVol))*32)
  auxLevel <= '0' & unsigned(not mixLevel) & "1111";

  color <= border or volumen or mute or muteC or salida;

  border <= "1110" & "1110" & "1110" when  ((pixel = (PLACE + 8) or pixel = (PLACE + 56)) and (line > 136 and line < 472)) or ((line = 136 or line = 472) and pixel >= PLACE+8 and pixel <= PLACE+56) else
              "0000" & "0000" & "0000";

  volumen  <= "0000" & "1111" & "0111" when (pixel > (PLACE + 12) and pixel < (PLACE + 52)) and line < 464 and line > (208+auxVolHeight) and line(3) = '1' else
              "0000" & "0000" & "0000";

  muteC    <= "1110" & "1000" & "1000" when ((pixel2 = (PLACE + 12)/2 or pixel2 = (PLACE + 50)/2) and (line2 > (128+16)/2 and line2 < (16+48+128)/2)) or ((line2 = (128+16)/2 or line2 = (16+48+128)/2) and pixel2 > (PLACE + 12)/2 and pixel2 < (PLACE + 50)/2) else
              "0000" & "0000" & "0000";

  mute     <= "1110" & "0000" & "0000" when (pixel > PLACE + 13 and pixel < PLACE + 51 and line > (128+16) and line < (16+48+128)) and isMute = '1' else
              "0000" & "0000" & "0000";

  salida   <= "1110" & "1000" & "0000" when (pixel > (PLACE + 20) and pixel < (PLACE + 44)) and line < 464 and line > (208+auxLevel) and line(1) = '1' else
              "0000" & "0000" & "0000";

end syn;

