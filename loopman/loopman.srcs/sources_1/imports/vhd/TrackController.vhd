-- trackController.vhd --------------------------------------------------------------------
--
-- Contains tracka and memory controller. Routes needed signals.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
-------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity trackController is
  generic (
    OUTPUTS : natural := 2;
    INPUTS  : natural := 2
  );
  port (
    -- System
    clk_200MHz_i     : in std_logic;
    rst_i            : in std_logic;
    clk_o            : out std_logic;
    rst_o            : out std_logic;
    -- Track control
    t_rec_i          : in std_logic_vector(7 downto 0);
    t_play_i         : in std_logic_vector(7 downto 0);
    t_mute_i         : in std_logic_vector(7 downto 0);
    t_solo_i         : in std_logic_vector(7 downto 0);
    t_clear_i        : in std_logic_vector(7 downto 0);
    t_sync_i         : in std_logic_vector(7 downto 0);
    t_auto_rec_tg_i  : in std_logic_vector(7 downto 0);
    t_inc_looplen_i  : in std_logic_vector(7 downto 0);
    t_dec_looplen_i  : in std_logic_vector(7 downto 0);
    t_set_looplen_i  : in std_logic_vector(7 downto 0);
    value_looplen_i  : in std_logic_vector(4 downto 0);
    t_inc_vol_i      : in std_logic_vector(7 downto 0);
    t_dec_vol_i      : in std_logic_vector(7 downto 0);
    t_set_vol_i      : in std_logic_vector(7 downto 0);
    value_vol_i      : in std_logic_vector(3 downto 0);
    t_inc_odub_i     : in std_logic_vector(7 downto 0);
    t_dec_odub_i     : in std_logic_vector(7 downto 0);
    t_set_odub_i     : in std_logic_vector(7 downto 0);
    value_odub_i     : in std_logic_vector(3 downto 0);
    t_input_sel_i   : in std_logic_vector((8*INPUTS)-1 downto 0); 
    t_output_sel_i  : in std_logic_vector((8*OUTPUTS)-1 downto 0);
    -- Tempo / sync / coordination
    t_tempo_i       : in std_logic;
    t_tempo_sync_o  : out std_logic;
    -- Track status
    st_rec_o        : out std_logic_vector(7 downto 0);
    st_play_o       : out std_logic_vector(7 downto 0);
    st_mute_o       : out std_logic_vector(7 downto 0);
    st_solo_o       : out std_logic_vector(7 downto 0);
    st_sync_o       : out std_logic_vector(7 downto 0);
    st_auto_rec_o   : out std_logic_vector(7 downto 0);
    t_ch_status_o   : out std_logic_vector(7 downto 0);
    st_beat_o       : out std_logic_vector((8*5)-1 downto 0);
    t_ch_beat_o     : out std_logic_vector(7 downto 0);
    st_looplen_o    : out std_logic_vector((8*5)-1 downto 0);
    t_ch_looplen_o  : out std_logic_vector(7 downto 0);
    st_vol_o        : out std_logic_vector((8*4)-1 downto 0);
    t_ch_vol_o      : out std_logic_vector(7 downto 0);
    st_odub_o       : out std_logic_vector((8*4)-1 downto 0);
    t_ch_odub_o     : out std_logic_vector(7 downto 0);
    st_outlevel_o   : out std_logic_vector((8*4)-1 downto 0); -- waveform to display, to show output level
    t_ch_outlevel_o : out std_logic_vector(7 downto 0);
    t_ch_in_sel_o   : out std_logic_vector(7 downto 0);
    t_ch_out_sel_o  : out std_logic_vector(7 downto 0);
    -- Audio IO
    audio_i         : in  int_sample(7 downto 0);       -- audio in from selected external inputs or tracks
    audio_o         : out int_sample(7 downto 0);       -- audio out to selected outputs
    in_sel_o        : out std_logic_vector((8*INPUTS)-1 downto 0); -- selected inputs
    out_sel_o       : out std_logic_vector((8*OUTPUTS)-1 downto 0); -- selected outputs
    input_rdy_i     : in  std_logic_vector(7 downto 0);
    output_rdy_o    : out std_logic_vector(7 downto 0);
    -- DDR2 interface
    ddr2_addr       : out   std_logic_vector(12 downto 0);
    ddr2_ba         : out   std_logic_vector(2 downto 0);
    ddr2_ras_n      : out   std_logic;
    ddr2_cas_n      : out   std_logic;
    ddr2_we_n       : out   std_logic;
    ddr2_ck_p       : out   std_logic_vector(0 downto 0);
    ddr2_ck_n       : out   std_logic_vector(0 downto 0);
    ddr2_cke        : out   std_logic_vector(0 downto 0);
    ddr2_cs_n       : out   std_logic_vector(0 downto 0);
    ddr2_odt        : out   std_logic_vector(0 downto 0);
    ddr2_dq         : inout std_logic_vector(15 downto 0);
    ddr2_dm         : out   std_logic_vector(1 downto 0);
    ddr2_dqs_p      : inout std_logic_vector(1 downto 0);
    ddr2_dqs_n      : inout std_logic_vector(1 downto 0)
  );
end trackController;

architecture syn of trackController is

  component track is
    generic (
      OUTPUTS : natural := 2;
      INPUTS  : natural := 2
    );
    port (
      -- System
      clk_i           : in std_logic;
      rst_i           : in std_logic;
      -- Track control
      t_rec_i         : in std_logic;
      t_play_i        : in std_logic;
      t_mute_i        : in std_logic;
      t_solo_i        : in std_logic;
      not_alone_i     : in std_logic; -- Other tracks are in solo mode
      t_clear_i       : in std_logic;
      t_sync_i        : in std_logic;
      t_auto_rec_tg_i : in std_logic;
      t_inc_looplen_i : in std_logic;
      t_dec_looplen_i : in std_logic;
      t_set_looplen_i : in std_logic;
      value_looplen_i : in std_logic_vector(4 downto 0);
      t_inc_vol_i     : in std_logic;
      t_dec_vol_i     : in std_logic;
      t_set_vol_i     : in std_logic;
      value_vol_i     : in std_logic_vector(3 downto 0);
      t_inc_odub_i    : in std_logic;
      t_dec_odub_i    : in std_logic;
      t_set_odub_i    : in std_logic;
      value_odub_i    : in std_logic_vector(3 downto 0);
      t_input_sel_i   : in std_logic_vector(INPUTS-1 downto 0); 
      t_output_sel_i  : in std_logic_vector(OUTPUTS-1 downto 0);
      -- Tempo / sync / coordination
      t_tempo_i        : in std_logic;
      others_working_i : in std_logic; -- Other tracks are playing or recording
      t_tempo_sync_o   : out std_logic;
      -- Track status
      st_rec_o        : out std_logic;
      st_play_o       : out std_logic;
      st_mute_o       : out std_logic;
      st_solo_o       : out std_logic;
      st_sync_o       : out std_logic;
      st_auto_rec_o   : out std_logic;
      t_ch_status_o   : out std_logic;
      st_beat_o       : out std_logic_vector(4 downto 0);
      t_ch_beat_o     : out std_logic;
      st_looplen_o    : out std_logic_vector(4 downto 0);
      t_ch_looplen_o  : out std_logic;
      st_vol_o        : out std_logic_vector(3 downto 0);
      t_ch_vol_o      : out std_logic;
      st_odub_o       : out std_logic_vector(3 downto 0);
      t_ch_odub_o     : out std_logic;
      st_outlevel_o   : out std_logic_vector(3 downto 0); -- waveform to display, to show output level
      t_ch_outlevel_o : out std_logic;
      t_ch_in_sel_o   : out std_logic;
      t_ch_out_sel_o  : out std_logic;
      -- Memory interface
      addr_o          : out std_logic_vector(21 downto 0);        -- current memory address
      loop_audio_i    : in  std_logic_vector(31 downto 0);        -- recorded audio
      rec_audio_o     : out std_logic_vector(31 downto 0);        -- audio to record
      odub_o          : out std_logic_vector(3 downto 0);         -- overdub (amount of old sample in new sample)
      won_o           : out std_logic;                            -- write enable
      op_o            : out std_logic;                            -- operation enable (like cs)
      -- Audio IO
      audio_i         : in  std_logic_vector(31 downto 0);        -- audio in from selected external inputs or tracks
      audio_o         : out std_logic_vector(31 downto 0);        -- audio out to selected outputs
      in_sel_o        : out std_logic_vector(INPUTS-1 downto 0);  -- selected inputs
      out_sel_o       : out std_logic_vector(OUTPUTS-1 downto 0); -- selected outputs
      input_rdy_i     : in  std_logic;
      output_rdy_o    : out std_logic
    );
  end component;

  component multitrackRecorder is
    port (
      -- Control interface
      clk_200MHz_i      : in  std_logic;
      rst_i             : in  std_logic;
      ui_clk_o          : out std_logic;
      ui_clk_sync_rst_o : out std_logic;  
      ---- Track interfaces
      -- Track 0
      addr0_i  : in  std_logic_vector(21 downto 0); -- address input 22 bits to select 32 bit sample
      data0_i  : in  std_logic_vector(31 downto 0); -- data input
      data0_o  : out std_logic_vector(31 downto 0); -- data output
      odub0_i  : in  std_logic_vector(3 downto 0);  -- overdub (amount of old sample in new sample)
      won0_i   : in  std_logic;                     -- enable writing
      op0_i    : in  std_logic;                     -- enable operation
      -- Track 1
      addr1_i  : in  std_logic_vector(21 downto 0);
      data1_i  : in  std_logic_vector(31 downto 0);
      data1_o  : out std_logic_vector(31 downto 0);
      odub1_i  : in  std_logic_vector(3 downto 0); 
      won1_i   : in  std_logic;
      op1_i    : in  std_logic;
      -- Track 2
      addr2_i  : in  std_logic_vector(21 downto 0);
      data2_i  : in  std_logic_vector(31 downto 0);
      data2_o  : out std_logic_vector(31 downto 0);
      odub2_i  : in  std_logic_vector(3 downto 0); 
      won2_i   : in  std_logic;
      op2_i    : in  std_logic;
      -- Track 3
      addr3_i  : in  std_logic_vector(21 downto 0);
      data3_i  : in  std_logic_vector(31 downto 0);
      data3_o  : out std_logic_vector(31 downto 0);
      odub3_i  : in  std_logic_vector(3 downto 0); 
      won3_i   : in  std_logic;
      op3_i    : in  std_logic;
      -- Track 4
      addr4_i  : in  std_logic_vector(21 downto 0);
      data4_i  : in  std_logic_vector(31 downto 0);
      data4_o  : out std_logic_vector(31 downto 0);
      odub4_i  : in  std_logic_vector(3 downto 0); 
      won4_i   : in  std_logic;
      op4_i    : in  std_logic;
      -- Track 5
      addr5_i  : in  std_logic_vector(21 downto 0);
      data5_i  : in  std_logic_vector(31 downto 0);
      data5_o  : out std_logic_vector(31 downto 0);
      odub5_i  : in  std_logic_vector(3 downto 0); 
      won5_i   : in  std_logic;
      op5_i    : in  std_logic;
      -- Track 6
      addr6_i  : in  std_logic_vector(21 downto 0);
      data6_i  : in  std_logic_vector(31 downto 0);
      data6_o  : out std_logic_vector(31 downto 0);
      odub6_i  : in  std_logic_vector(3 downto 0); 
      won6_i   : in  std_logic;
      op6_i    : in  std_logic;
      -- Track 7
      addr7_i  : in  std_logic_vector(21 downto 0);
      data7_i  : in  std_logic_vector(31 downto 0);
      data7_o  : out std_logic_vector(31 downto 0);
      odub7_i  : in  std_logic_vector(3 downto 0); 
      won7_i   : in  std_logic;
      op7_i    : in  std_logic;
      -- DDR2 interface
      ddr2_addr  : out   std_logic_vector(12 downto 0);
      ddr2_ba    : out   std_logic_vector(2 downto 0);
      ddr2_ras_n : out   std_logic;
      ddr2_cas_n : out   std_logic;
      ddr2_we_n  : out   std_logic;
      ddr2_ck_p  : out   std_logic_vector(0 downto 0);
      ddr2_ck_n  : out   std_logic_vector(0 downto 0);
      ddr2_cke   : out   std_logic_vector(0 downto 0);
      ddr2_cs_n  : out   std_logic_vector(0 downto 0);
      ddr2_odt   : out   std_logic_vector(0 downto 0);
      ddr2_dq    : inout std_logic_vector(15 downto 0);
      ddr2_dm    : out   std_logic_vector(1 downto 0);
      ddr2_dqs_p : inout std_logic_vector(1 downto 0);
      ddr2_dqs_n : inout std_logic_vector(1 downto 0)
    );
  end component;

  -- System
  signal clk : std_logic;
  signal rst : std_logic;
  
  -- Track status
  signal st_rec  : std_logic_vector(7 downto 0);
  signal st_play : std_logic_vector(7 downto 0);
  
  -- Track control and sync
  signal not_alone : std_logic_vector(7 downto 0);
  signal st_solo   : std_logic_vector(7 downto 0);
  signal others_working : std_logic_vector(7 downto 0);
  signal t_tempo_sync : std_logic_vector(7 downto 0);
  
  -- Memory interface
  signal addr        : std_logic_vector((22*8)-1 downto 0);
  signal loop_audio  : int_sample(7 downto 0);
  signal rec_audio   : int_sample(7 downto 0);
  signal odub        : std_logic_vector((4*8)-1 downto 0);
  signal won         : std_logic_vector(7 downto 0);
  signal op          : std_logic_vector(7 downto 0);
  
  signal probe0      : std_logic_vector(7 downto 0);
  signal probe1      : std_logic_vector(7 downto 0);
  signal probe2      : std_logic_vector(7 downto 0);
  signal probe3      : std_logic_vector(7 downto 0);

begin

  -- System signals ------------------
  clk_o <= clk;
  rst_o <= rst;
  ------------------------------------
  
  -- Audio IO ------------------------
  -- Direct to tracks
  ------------------------------------
  
  -- Memory interface  ---------------
  -- Direct to multitrackRecorder
  ------------------------------------
  
  -- Track control -------------------
  st_rec_o <= st_rec;
  st_play_o <= st_play;
  
  st_solo_o <= st_solo;

  t_tempo_sync_o <= '1' when t_tempo_sync /= "00000000" else '0';
  
  -- To avoid combinational loops, dont route signals to the track they come from
  others_working_not_alone_assign : process (st_rec, st_play, st_solo)
    type st_array is array (integer range <>) of std_logic_vector (7 downto 0);
    variable st_rec_aux, st_play_aux, st_solo_aux : st_array(7 downto 0);
  begin
    for i in 0 to 7 loop
      st_rec_aux(i) := st_rec;
      st_rec_aux(i)(i) := '0';
      st_play_aux(i) := st_play;
      st_play_aux(i)(i) := '0';
      st_solo_aux(i) := st_solo;
      st_solo_aux(i)(i) := '0';
      
      if (st_rec_aux(i) /= "00000000") or (st_play_aux(i) /= "00000000") then
        others_working(i) <= '1';
      else
        others_working(i) <= '0';
      end if;
      
      not_alone(i) <= '0';
      if (st_solo_aux(i) /= "00000000") then
        not_alone(i) <= '1';
      else
        not_alone(i) <= '0';
      end if;
    end loop;
  end process;
  ------------------------------------
 
  tracks : for i in 0 to 7 generate
    track_i : track
    generic map (
      OUTPUTS => OUTPUTS,
      INPUTS  => INPUTS 
    )
    port map (
      -- System
      clk_i           => clk,
      rst_i           => rst,
      -- Track control
      t_rec_i         => t_rec_i(i),
      t_play_i        => t_play_i(i),
      t_mute_i        => t_mute_i(i),
      t_solo_i        => t_solo_i(i),
      not_alone_i     => not_alone(i),
      t_clear_i       => t_clear_i(i),
      t_sync_i        => t_sync_i(i),
      t_auto_rec_tg_i => t_auto_rec_tg_i(i),
      t_inc_looplen_i => t_inc_looplen_i(i),
      t_dec_looplen_i => t_dec_looplen_i(i),
      t_set_looplen_i => t_set_looplen_i(i),
      value_looplen_i => value_looplen_i,
      t_inc_vol_i     => t_inc_vol_i(i),
      t_dec_vol_i     => t_dec_vol_i(i),
      t_set_vol_i     => t_set_vol_i(i),
      value_vol_i     => value_vol_i,
      t_inc_odub_i    => t_inc_odub_i(i),
      t_dec_odub_i    => t_dec_odub_i(i),
      t_set_odub_i    => t_set_odub_i(i),
      value_odub_i    => value_odub_i,
      t_input_sel_i   => t_input_sel_i(((i+1)*INPUTS)-1 downto i*(INPUTS)),
      t_output_sel_i  => t_output_sel_i(((i+1)*OUTPUTS)-1 downto i*(OUTPUTS)),
      -- Tempo
      t_tempo_i       => t_tempo_i,
      others_working_i => others_working(i),
      t_tempo_sync_o  => t_tempo_sync(i),
      st_rec_o        => st_rec(i),
      st_play_o       => st_play(i),
      st_mute_o       => st_mute_o(i),
      st_solo_o       => st_solo(i),
      st_sync_o       => st_sync_o(i),
      st_auto_rec_o   => st_auto_rec_o(i),
      t_ch_status_o   => t_ch_status_o(i),
      st_beat_o       => st_beat_o(((i+1)*5)-1 downto i*5),
      t_ch_beat_o     => t_ch_beat_o(i),
      st_looplen_o    => st_looplen_o(((i+1)*5)-1 downto i*5),
      t_ch_looplen_o  => t_ch_looplen_o(i),
      st_vol_o        => st_vol_o(((i+1)*4)-1 downto i*4),
      t_ch_vol_o      => t_ch_vol_o(i),
      st_odub_o       => st_odub_o(((i+1)*4)-1 downto i*4),
      t_ch_odub_o     => t_ch_odub_o(i),
      st_outlevel_o   => st_outlevel_o(((i+1)*4)-1 downto i*4), -- waveform to display, to show output level
      t_ch_outlevel_o => t_ch_outlevel_o(i),
      t_ch_in_sel_o   => t_ch_in_sel_o(i),
      t_ch_out_sel_o  => t_ch_out_sel_o(i),
      -- Memory interface
      addr_o          => addr(((i+1)*22)-1 downto i*22), -- current memory address
      loop_audio_i    => loop_audio(i),                  -- recorded audio
      rec_audio_o     => rec_audio(i),                   -- audio to record
      odub_o          => odub(((i+1)*4)-1 downto i*4),   -- overdub (amount of old sample in new sample)
      won_o           => won(i),                         -- write enable
      op_o            => op(i),                          -- operation enable (like cs)
      -- Audio IO
      audio_i         => audio_i(i),                                      -- audio in from selected external inputs or tracks
      audio_o         => audio_o(i),                                      -- audio out to selected outputs
      in_sel_o        => in_sel_o(((i+1)*INPUTS)-1 downto i*(INPUTS)),    -- selected inputs
      out_sel_o       => out_sel_o(((i+1)*OUTPUTS)-1 downto i*(OUTPUTS)), -- selected outputs
      input_rdy_i     => input_rdy_i(i),
      output_rdy_o    => output_rdy_o(i)
    );
  end generate;
  
  memory : multitrackRecorder
  port map (
    -- Control interface
    clk_200MHz_i      => clk_200MHz_i,
    rst_i             => rst_i,
    ui_clk_o          => clk,
    ui_clk_sync_rst_o => rst,
    ---- Track interfaces
    -- Track 0
    addr0_i => addr((1*22)-1 downto 0*22),
    data0_i => rec_audio(0),    
    data0_o => loop_audio(0),     
    odub0_i => odub((1*4)-1 downto 0*4),
    won0_i  => won(0),
    op0_i   => op(0), 
    -- Track 1
    addr1_i => addr((2*22)-1 downto 1*22),
    data1_i => rec_audio(1),              
    data1_o => loop_audio(1),             
    odub1_i => odub((2*4)-1 downto 1*4),  
    won1_i  => won(1),                    
    op1_i   => op(1),                     
    -- Track 2
    addr2_i => addr((3*22)-1 downto 2*22), 
    data2_i => rec_audio(2),               
    data2_o => loop_audio(2),              
    odub2_i => odub((3*4)-1 downto 2*4),   
    won2_i  => won(2),                     
    op2_i   => op(2),                      
    -- Track 3
    addr3_i => addr((4*22)-1 downto 3*22),  
    data3_i => rec_audio(3),                
    data3_o => loop_audio(3),               
    odub3_i => odub((4*4)-1 downto 3*4),    
    won3_i  => won(3),                      
    op3_i   => op(3),                       
    -- Track 4
    addr4_i => addr((5*22)-1 downto 4*22),  
    data4_i => rec_audio(4),                
    data4_o => loop_audio(4),               
    odub4_i => odub((5*4)-1 downto 4*4),    
    won4_i  => won(4),                      
    op4_i   => op(4),                       
    -- Track 5
    addr5_i => addr((6*22)-1 downto 5*22),
    data5_i => rec_audio(5),                
    data5_o => loop_audio(5),               
    odub5_i => odub((6*4)-1 downto 5*4),    
    won5_i  => won(5),                      
    op5_i   => op(5),                       
    -- Track 6
    addr6_i => addr((7*22)-1 downto 6*22),  
    data6_i => rec_audio(6),                
    data6_o => loop_audio(6),               
    odub6_i => odub((7*4)-1 downto 6*4),    
    won6_i  => won(6),                      
    op6_i   => op(6),                       
    -- Track 7
    addr7_i => addr((8*22)-1 downto 7*22), 
    data7_i => rec_audio(7),               
    data7_o => loop_audio(7),              
    odub7_i => odub((8*4)-1 downto 7*4),
    won7_i  => won(7),                     
    op7_i   => op(7),                      
    -- DDR2 interface
    ddr2_addr   => ddr2_addr,
    ddr2_ba     => ddr2_ba,
    ddr2_ras_n  => ddr2_ras_n,
    ddr2_cas_n  => ddr2_cas_n,
    ddr2_we_n   => ddr2_we_n,
    ddr2_ck_p   => ddr2_ck_p,
    ddr2_ck_n   => ddr2_ck_n,
    ddr2_cke    => ddr2_cke,
    ddr2_cs_n   => ddr2_cs_n,
    ddr2_odt    => ddr2_odt,
    ddr2_dq     => ddr2_dq,
    ddr2_dm     => ddr2_dm,
    ddr2_dqs_p  => ddr2_dqs_p,
    ddr2_dqs_n  => ddr2_dqs_n
  );

end syn;
