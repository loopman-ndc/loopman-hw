-- synchronizer.vhd -------------------------------------------------------------------------
--
-- Synchronizes binary input
--
-- (c) J.M. Mendias
-- Dise�o Autom�tico de Sistemas
-- Facultad de Inform�tica. Universidad Complutense de Madrid
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity synchronizer is
  generic (
    STAGES  : in natural;    -- number of flip-flops in synchronizer
    INIT    : in std_logic   -- flip-flops initial value
  );
  port (
    rst   : in  std_logic;   -- async reset
    clk   : in  std_logic;   -- clock
    x     : in  std_logic;   -- input to synchronize
    xSync : out std_logic    -- synchronized output
  );
end synchronizer;

-------------------------------------------------------------------

architecture syn of synchronizer is 
begin

  process (rst, clk)
    variable aux : std_logic_vector(STAGES-1 downto 0); 
  begin
    xSync <= aux(STAGES-1);		
    if rst='1' then
      aux := (others => INIT);
    elsif rising_edge(clk) then
      for i in STAGES-1 downto 1 loop
        aux(i) := aux(i-1);
      end loop;
      aux(0) := x;
    end if;
  end process;

end syn;
