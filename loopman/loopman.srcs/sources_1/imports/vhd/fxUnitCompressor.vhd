-- fxUnitCompressor.vhd -------------------------------------------------------------------------
--
-- Compressor effect.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitCompressor is
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitCompressor;

architecture syn of fxUnitCompressor is

  signal st_fx_n   : std_logic_vector(3 downto 0);
  signal st_fx     : natural;
  signal st_fx_aux : unsigned(21 downto 0);
  
  constant threshold_max  : natural := 32_000_000;--1_000_000_000;
  constant threshold_step : natural := 1_000_000;--65_000_000
  
  constant max_window  : unsigned(9 downto 0) := '0' & to_unsigned(127, 9);
  
  signal multipler_out : signed(39 downto 0);
  
  signal cval          : signed(7 downto 0); -- 8 bits wide instead of 6 because max value is 1.0 in C2 -> "01000000"
  
  signal fx        : int_sample(0 downto 0);
  
  signal over_threshold : std_logic;
  
begin

  -- True bypass
  output_o <= fx when st_fx_i /= (st_fx_i'range=>'0') else input_i;
  
  -- Value control
  st_fx <= to_integer(unsigned(st_fx_i));

  -- Threshold preset constant easy assignment to detect signals over threshold. Combinational
  level_detect : process
    type natural_vector is array (integer range <>) of natural;
    variable threshold_preset : natural_vector(15 downto 0);
    variable output_val : natural;
    
  begin
    over_threshold <= '0';
    if fx(0)(31) = '0' then
      output_val := to_integer(signed(fx(0)));
    else
      output_val := to_integer(-signed(fx(0)));
    end if;
  
    for i in 0 to 15 loop
      threshold_preset(i) := threshold_max-(i*threshold_step);
      
      if st_fx = i then
        if output_val > threshold_preset(i) then --fx(0)(31) = '0' and 
          over_threshold <= '1';
        end if;
      end if;
    end loop;
  end process;
  
  multipler_out <= signed(input_i(0)) * cval;

  mainFSM :
  process (rst_i, clk_i, st_fx_i)
    type states is (idle,
                    compress,
                    ready,
                    feedback);
    variable state : states;
    
    variable sample_count, over_count : unsigned(9 downto 0);
  begin
    output_rdy_o <= '0';
    
    case state is
      when idle =>
        if input_rdy_i = '1' and st_fx_i = (st_fx_i'range=>'0') then
          output_rdy_o <= '1';
        end if;
      when compress => 
      when ready =>
        output_rdy_o <= '1';
      when feedback =>
      when others =>
    end case;
  
    if rst_i = '1' then
      state := idle;
      cval <= "01000000";
      fx(0) <= (others=>'0');
      sample_count := (others=>'0');
      over_count := (others=>'0');
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if input_rdy_i = '1' and st_fx_i /= (st_fx_i'range=>'0') then
            state := compress;
          end if;

        when compress => 
          fx(0) <= std_logic_vector(multipler_out(37 downto 6));
          state := ready;
        
        when ready =>
          state := feedback;
        
        when feedback =>
          if sample_count >= max_window then
            if over_count > to_unsigned(5, 9) and cval > "00001100"  then
              cval <= cval - 1;
            elsif over_count = 0 and cval < "01000000" then
              cval <= cval + 1;
            end if;
            
            sample_count := (others=>'0');
            over_count := (others=>'0');
          else
            if over_threshold = '1' then
              over_count := over_count + 1;
            end if;
            sample_count := sample_count + 1;
          end if;
          
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;

end syn;
