-- fxController.vhd ----------------------------------------------------------------------------
--
-- Wrapper for the fx processors.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxController is
  generic (
    FX_NO : natural := 4
  );
  port (
    -- System
    rst_i         : in  std_logic;
    clk_i         : in  std_logic;
    -- Audio output
    output_o      : out int_sample(7 downto 0);
    output_rdy_o  : out std_logic_vector(7 downto 0);
    -- Audio input
    input_i       : in  int_sample(7 downto 0);
    input_rdy_i   : in  std_logic_vector(7 downto 0);
    -- Control
    t_toggle_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
    t_enqueue_i   : in  std_logic_vector((8*FX_NO)-1 downto 0);
    t_inc_fx_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
    t_dec_fx_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
    t_set_fx_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
    value_fx_i    : in  std_logic_vector((8*4)-1 downto 0);
    t_clear_i     : in  std_logic_vector(7 downto 0);
    -- Status
    st_on_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
    st_queued_o   : out std_logic_vector((8*FX_NO)-1 downto 0);
    st_fx_o       : out std_logic_vector(8*(FX_NO*4)-1 downto 0);
    t_ch_fx_o     : out std_logic_vector((8*FX_NO)-1 downto 0)
  );
end fxController;

architecture syn of fxController is

  component fxProcessor is
    generic (
      FX_NO    : natural := 4;
      TRACK_ID : natural := 0
    );
    port (
      -- System
      rst_i         : in  std_logic;
      clk_i         : in  std_logic;
      -- Audio output
      output_o      : out int_sample;
      output_rdy_o  : out std_logic;
      -- Audio input
      input_i       : in  int_sample;
      input_rdy_i   : in  std_logic;
      -- Control
      t_toggle_i    : in  std_logic_vector(FX_NO-1 downto 0);
      t_enqueue_i   : in  std_logic_vector(FX_NO-1 downto 0);
      t_inc_fx_i    : in  std_logic_vector(FX_NO-1 downto 0);
      t_dec_fx_i    : in  std_logic_vector(FX_NO-1 downto 0);
      t_set_fx_i    : in  std_logic_vector(FX_NO-1 downto 0);
      value_fx_i    : in  std_logic_vector(3 downto 0);
      t_clear_i     : in  std_logic;
      -- Status
      st_on_o       : out std_logic_vector(FX_NO-1 downto 0);
      st_queued_o   : out std_logic_vector(FX_NO-1 downto 0);
      st_fx_o       : out std_logic_vector((FX_NO*4)-1 downto 0);
      t_ch_fx_o     : out std_logic_vector(FX_NO-1 downto 0)
    );
  end component;
  
  type int_sample_array is array (integer range <>) of int_sample(0 downto 0);
  
  signal output : int_sample_array(7 downto 0);
  signal input  : int_sample_array(7 downto 0);

begin

  input_output_connect : process
  begin
    for i in 0 to 7 loop
      output_o(i) <= output(i)(0);
      input(i)(0) <= input_i(i);
    end loop;
  end process;

  fxProcessor_component : for i in 0 to 7 generate
    fxProcessor_generated : fxProcessor
    generic map (
      FX_NO    => FX_NO,
      TRACK_ID => i
    )
    port map (
      -- System
      rst_i         => rst_i,
      clk_i         => clk_i,
      -- Audio output
      output_o      => output(i),
      output_rdy_o  => output_rdy_o(i),
      -- Audio input
      input_i       => input(i),    
      input_rdy_i   => input_rdy_i(i),
      -- Control
      t_toggle_i    => t_toggle_i(((i+1)*FX_NO)-1 downto i*FX_NO),
      t_enqueue_i   => t_enqueue_i(((i+1)*FX_NO)-1 downto i*FX_NO),
      t_inc_fx_i    => t_inc_fx_i(((i+1)*FX_NO)-1 downto i*FX_NO),
      t_dec_fx_i    => t_dec_fx_i(((i+1)*FX_NO)-1 downto i*FX_NO),
      t_set_fx_i    => t_set_fx_i(((i+1)*FX_NO)-1 downto i*FX_NO),
      value_fx_i    => value_fx_i(((i+1)*4)-1 downto i*4),
      t_clear_i     => t_clear_i(i),
      -- Status
      st_on_o       => st_on_o(((i+1)*FX_NO)-1 downto i*FX_NO),
      st_queued_o   => st_queued_o(((i+1)*FX_NO)-1 downto i*FX_NO),
      st_fx_o       => st_fx_o(((i+1)*FX_NO*4)-1 downto i*FX_NO*4),
      t_ch_fx_o     => t_ch_fx_o(((i+1)*FX_NO)-1 downto i*FX_NO)
    );
  end generate;
  
end syn;

