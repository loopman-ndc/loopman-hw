-- outputMixer.vhd -----------------------------------------------------------------------
--
-- Mixes tracks outputs into final 24 bit samples. Receives output selection from each
-- track, so mixed samples only contain choosen tracks.
-- Also provides a compressor and volume booster.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity outputMixer is
  generic ( 
    OUTPUTS : natural := 2
  );
  port (
    -- System
    rst_i           : in  std_logic;
    clk_i           : in  std_logic;
    -- Audio outputs (mixed audio)
    output_o        : out ext_sample(OUTPUTS-1 downto 0);
    output_rdy_o    : out std_logic_vector(OUTPUTS-1 downto 0);
    -- Audio inputs
    input_i         : in  int_sample(7 downto 0);
    input_rdy_i     : in  std_logic_vector(7 downto 0);
    -- Compression
    t_inc_comp_i    : in  std_logic_vector(OUTPUTS-1 downto 0);
    t_dec_comp_i    : in  std_logic_vector(OUTPUTS-1 downto 0);
    t_set_comp_i    : in  std_logic_vector(OUTPUTS-1 downto 0);
    value_comp_i    : in  std_logic_vector(3 downto 0);
    st_comp_o       : out std_logic_vector((OUTPUTS*4)-1 downto 0);
    t_ch_comp_o     : out std_logic_vector(OUTPUTS-1 downto 0);
    -- Volume
    t_inc_vol_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
    t_dec_vol_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
    t_set_vol_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
    value_vol_i     : in  std_logic_vector(3 downto 0);
    st_vol_o        : out std_logic_vector((OUTPUTS*4)-1 downto 0);
    t_ch_vol_o      : out std_logic_vector(OUTPUTS-1 downto 0);
    st_outlevel_o   : out std_logic_vector((OUTPUTS*4)-1 downto 0);
    st_clipping_o   : out std_logic_vector(OUTPUTS-1 downto 0);
    t_ch_outlevel_o : out std_logic_vector(OUTPUTS-1 downto 0);
    -- Tracks output selection
    out_sel0_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    out_sel1_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    out_sel2_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    out_sel3_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    out_sel4_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    out_sel5_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    out_sel6_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    out_sel7_i      : in  std_logic_vector(OUTPUTS-1 downto 0)
  );
end outputMixer;

architecture syn of outputMixer is

  type out_mix_sel is array (integer range <>) of std_logic_vector (7 downto 0);
  
  constant max_clip : std_logic_vector(23 downto 0) :=  "011111111111111111111111";
  constant min_clip : std_logic_vector(23 downto 0) :=  "100000000000000000000000";

  component compressorMixer is
    generic ( 
      INPUTS : natural := 2
    );
    port (
      -- System
      rst_i        : in std_logic;
      clk_i        : in std_logic;
      -- Control
      compressor_i : in  std_logic_vector(3 downto 0);
      volume_i     : in  std_logic_vector(3 downto 0);
      sel_i        : in  std_logic_vector(INPUTS-1 downto 0);
      compressed_o : out std_logic;
      -- Audio
      input_i      : in  int_sample(INPUTS-1 downto 0);
      input_rdy_i  : in  std_logic; 
      mix_o        : out int_sample(0 downto 0);
      mix_rdy_o    : out std_logic
    );
  end component;

  signal inputs_array   : int_sample(7 downto 0);
  signal ext_output     : int_sample(OUTPUTS-1 downto 0); -- Output signal after 32 to 24 bit conversion
  signal mix_sel        : out_mix_sel(OUTPUTS-1 downto 0);
  
  type int_sample_aux is array (integer range <>) of int_sample(0 downto 0);
  signal ext_output_aux : int_sample_aux(OUTPUTS-1 downto 0);
  
  signal output         : ext_sample(OUTPUTS-1 downto 0);       -- Intermediaate signal for output_o
  signal output_rdy     : std_logic_vector(OUTPUTS-1 downto 0); -- Intermediaate signal for output_rdy_o
 
  signal vol            : unsigned((OUTPUTS*4)-1 downto 0);
  signal comp           : unsigned((OUTPUTS*4)-1 downto 0);
  
  signal outlevel_combi : std_logic_vector((OUTPUTS*4)-1 downto 0);
  signal outlevel       : std_logic_vector((OUTPUTS*4)-1 downto 0);
  
begin

  output_o     <= output;
  output_rdy_o <= output_rdy;
  
  st_vol_o  <= std_logic_vector(vol);
  st_comp_o <= std_logic_vector(comp);
  
  st_outlevel_o <= outlevel;

  -- Volume and compression control
  vol_control : process (rst_i, clk_i)
    variable voli : unsigned(3 downto 0); -- Just to write less
  begin
    if rst_i = '1' then
      vol <= (others=>'0');
      t_ch_vol_o <= (others=>'0');
      
    elsif rising_edge(clk_i) then
      for i in 0 to OUTPUTS-1 loop
        voli := vol(((i+1)*4)-1 downto i*4);
      
        t_ch_vol_o(i) <= '0';
        
        if t_set_vol_i(i) = '1' then
          vol(((i+1)*4)-1 downto i*4) <= unsigned(value_vol_i);
          t_ch_vol_o(i) <= '1';
        elsif t_inc_vol_i(i) = '1' and voli < "1111" then
          vol(((i+1)*4)-1 downto i*4) <= voli + 1;
          t_ch_vol_o(i) <= '1';
        elsif t_dec_vol_i(i) = '1' and voli > "0000" then
          vol(((i+1)*4)-1 downto i*4) <= voli - 1;
          t_ch_vol_o(i) <= '1';
        end if;
      end loop;
    end if;
  end process;
  
  comp_control : process (rst_i, clk_i)
    variable compi : unsigned(3 downto 0); -- Just to write less
  begin
      if rst_i = '1' then
        comp <= (others=>'0');
        t_ch_comp_o <= (others=>'0');
        
      elsif rising_edge(clk_i) then
        for i in 0 to OUTPUTS-1 loop   
          compi := comp(((i+1)*4)-1 downto i*4);
          
          t_ch_comp_o(i) <= '0';
          
          if t_set_comp_i(i) = '1' then
            comp(((i+1)*4)-1 downto i*4) <= unsigned(value_comp_i);
            t_ch_comp_o(i) <= '1';
          elsif t_inc_comp_i(i) = '1' and compi < "1111" then
            comp(((i+1)*4)-1 downto i*4) <= compi + 1;
            t_ch_comp_o(i) <= '1';
          elsif t_dec_comp_i(i) = '1' and compi > "0000" then
            comp(((i+1)*4)-1 downto i*4) <= compi - 1;
            t_ch_comp_o(i) <= '1';
          end if;
        end loop;
      end if;
  end process;
  
  ext_output_aux_connect : process
  begin
    for i in 0 to OUTPUTS-1 loop
      ext_output_aux(i)(0) <= ext_output(i);
    end loop;
  end process;
  
  -- Outlevel
  outlevel_combi_sets : for i in 0 to OUTPUTS-1 generate
    outlevel_combi_set : outlevelIndicator
    generic map (
      threshold_step => 143_165_576
    )
    port map (
      sample_i   => ext_output_aux(i),
      outlevel_o => outlevel_combi(((i+1)*4)-1 downto i*4)
    );
  end generate;
  
  outlevel_segmentated : process (rst_i, clk_i)
    type outlevel_states is (idle,
                             change,
                             reset);
    type outlevel_states_vector is array (integer range <>) of outlevel_states;
    variable state : outlevel_states_vector(OUTPUTS-1 downto 0);
  
    constant max_count : natural := 4095;
    
    type count_vector is array (integer range <>) of natural range 0 to 4095;
    variable count : count_vector(OUTPUTS-1 downto 0);
    
    type max_vector is array (integer range <>) of unsigned(3 downto 0);
    variable max_outlevel : max_vector(OUTPUTS-1 downto 0);
  begin
    if rst_i = '1' then
      outlevel <= (others=>'0');
      t_ch_outlevel_o <= (others=>'0');
      count := (others=>0);
      max_outlevel := (others=>(others=>'0'));
      
      state := (others=>idle);
    elsif rising_edge(clk_i) then
      t_ch_outlevel_o <= (others=>'0');
      
      for i in 0 to OUTPUTS-1 loop
        case state(i) is
          when idle =>
            if mix_sel(i) /= (mix_sel(i)'range=>'0') then --and outlevel_combi(((i+1)*4)-1 downto i*4) /= (4=>'0') then
              state(i) := change;
            end if;
            
          when change =>
            if output_rdy(i) = '1' then
              -- To reduce number of toggles of outlevel
              if count(i) < max_count then
                count(i) := count(i) + 1;
                if max_outlevel(i) < unsigned(outlevel_combi(((i+1)*4)-1 downto i*4)) then
                  max_outlevel(i) := unsigned(outlevel_combi(((i+1)*4)-1 downto i*4));
                end if;
              else
                outlevel(((i+1)*4)-1 downto i*4) <= std_logic_vector(max_outlevel(i));
                count(i) := 0;
                max_outlevel := (others=>(others=>'0'));
                t_ch_outlevel_o(i) <= '1';
              end if;
            end if;
            
            -- This disables outlevel retransmission when outlevel is always 0
            -- Changes with 1 cycle delay, to ensure that last "outlevel = 0" is sent
            if mix_sel(i) = (mix_sel(i)'range=>'0') then --or outlevel_combi(((i+1)*4)-1 downto i*4) = (4=>'0') then
              state(i) := reset;
            end if;
          
          when reset =>
            outlevel(((i+1)*4)-1 downto i*4) <= (others=>'0');
            count(i) := 0;                
            t_ch_outlevel_o(i) <= '1';    
            
            state(i) := idle;
            
          when others =>
        end case;
      end loop;
    end if;
  end process;

  -- Convert 32 bit samples to 24 bit samples with rounding.
  int_to_ext_output : process
    variable aux_output  : std_logic_vector(23 downto 0);
    variable add         : std_logic_vector(23 downto 0);
    variable sat_control : std_logic_vector(23 downto 0);
  begin
    for i in 0 to OUTPUTS-1 loop
      aux_output := ext_output(i)(31 downto 8);
      
      if ext_output(i)(31) = '0' and ext_output(i)(7) = '1' then
        -- Numero positivo con redondeo
        add := std_logic_vector(signed(aux_output) + 1);
         -- Control de saturacion
        if add(23) /= '0' then
          output(i) <= max_clip;
        else
          output(i) <= add;
        end if;
      elsif ext_output(i)(31) = '1' and ext_output(i)(7) = '0' then
        -- Numero negativo con redondeo
        add := std_logic_vector(signed(aux_output) - 1);
        -- Control de saturacion
        if add(23) /= '1' then
          output(i) <= min_clip;
        else
          output(i) <= add;
        end if;
      else
        -- Nada que redondear
        output(i) <= aux_output;
      end if;
    end loop;
  end process;

  generate_output_sel : process
  begin
    for i in 0 to OUTPUTS-1 loop
      mix_sel(i)(0) <= out_sel0_i(i); -- For input i, output 0 says...
      mix_sel(i)(1) <= out_sel1_i(i); -- For input i, output 1 says...
      mix_sel(i)(2) <= out_sel2_i(i);
      mix_sel(i)(3) <= out_sel3_i(i);
      mix_sel(i)(4) <= out_sel4_i(i);
      mix_sel(i)(5) <= out_sel5_i(i);
      mix_sel(i)(6) <= out_sel6_i(i);
      mix_sel(i)(7) <= out_sel7_i(i);
    end loop;
  end process;

  mixers : for i in 0 to OUTPUTS-1 generate
    mixer : compressorMixer
    generic map (INPUTS => 8)
    port map (
      -- System
      rst_i        => rst_i,
      clk_i        => clk_i,
      -- Control
      compressor_i => std_logic_vector(comp(((i+1)*4)-1 downto i*4)),
      volume_i     => std_logic_vector(vol(((i+1)*4)-1 downto i*4)),
      sel_i        => mix_sel(i),
      compressed_o => st_clipping_o(i),
      -- Audio
      input_i      => input_i,
      input_rdy_i  => input_rdy_i(i),
      mix_o        => ext_output(i downto i),
      mix_rdy_o    => output_rdy(i)
    );
  end generate;
  
end syn;
