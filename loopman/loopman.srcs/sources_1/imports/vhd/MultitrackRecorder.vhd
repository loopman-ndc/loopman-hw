-- multitrackRecorder.vhd --------------------------------------------------------------------
--
-- LoopMAN's memory interface.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multitrackRecorder is
  port (
    -- Control interface
    clk_200MHz_i         : in  std_logic;
    rst_i                : in  std_logic;
    ui_clk_o             : out std_logic;
    ui_clk_sync_rst_o    : out std_logic;  
    ---- Track interfaces
    -- Track 0
    addr0_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data0_i              : in  std_logic_vector(31 downto 0); -- data input
    data0_o              : out std_logic_vector(31 downto 0); -- data output
    odub0_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won0_i               : in  std_logic; -- enable writing
    op0_i                : in  std_logic; -- enable operation
    -- Track 1
    addr1_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data1_i              : in  std_logic_vector(31 downto 0); -- data input
    data1_o              : out std_logic_vector(31 downto 0); -- data output
    odub1_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won1_i               : in  std_logic; -- enable writing
    op1_i                : in  std_logic; -- enable operation
    -- Track 2
    addr2_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data2_i              : in  std_logic_vector(31 downto 0); -- data input
    data2_o              : out std_logic_vector(31 downto 0); -- data output
    odub2_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won2_i               : in  std_logic; -- enable writing
    op2_i                : in  std_logic; -- enable operation
    -- Track 3
    addr3_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data3_i              : in  std_logic_vector(31 downto 0); -- data input
    data3_o              : out std_logic_vector(31 downto 0); -- data output
    odub3_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won3_i               : in  std_logic; -- enable writing
    op3_i                : in  std_logic; -- enable operation
    -- Track 4
    addr4_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data4_i              : in  std_logic_vector(31 downto 0); -- data input
    data4_o              : out std_logic_vector(31 downto 0); -- data output
    odub4_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won4_i               : in  std_logic; -- enable writing
    op4_i                : in  std_logic; -- enable operation
    -- Track 5
    addr5_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data5_i              : in  std_logic_vector(31 downto 0); -- data input
    data5_o              : out std_logic_vector(31 downto 0); -- data output
    odub5_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won5_i               : in  std_logic; -- enable writing
    op5_i                : in  std_logic; -- enable operation
    -- Track 6
    addr6_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data6_i              : in  std_logic_vector(31 downto 0); -- data input
    data6_o              : out std_logic_vector(31 downto 0); -- data output
    odub6_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won6_i               : in  std_logic; -- enable writing
    op6_i                : in  std_logic; -- enable operation
    -- Track 7
    addr7_i              : in  std_logic_vector(21 downto 0); -- address input 23 bits to select 16 bit sample
    data7_i              : in  std_logic_vector(31 downto 0); -- data input
    data7_o              : out std_logic_vector(31 downto 0); -- data output
    odub7_i              : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
    won7_i               : in  std_logic; -- enable writing
    op7_i                : in  std_logic; -- enable operation
    -- DDR2 interface
    ddr2_addr            : out   std_logic_vector(12 downto 0);
    ddr2_ba              : out   std_logic_vector(2 downto 0);
    ddr2_ras_n           : out   std_logic;
    ddr2_cas_n           : out   std_logic;
    ddr2_we_n            : out   std_logic;
    ddr2_ck_p            : out   std_logic_vector(0 downto 0);
    ddr2_ck_n            : out   std_logic_vector(0 downto 0);
    ddr2_cke             : out   std_logic_vector(0 downto 0);
    ddr2_cs_n            : out   std_logic_vector(0 downto 0);
    ddr2_odt             : out   std_logic_vector(0 downto 0);
    ddr2_dq              : inout std_logic_vector(15 downto 0);
    ddr2_dm              : out   std_logic_vector(1 downto 0);
    ddr2_dqs_p           : inout std_logic_vector(1 downto 0);
    ddr2_dqs_n           : inout std_logic_vector(1 downto 0)
  );
end multitrackRecorder;

architecture syn of multitrackRecorder is

  component Ram2Ddr is
  port (
    -- Common
    clk_200MHz_i      : in    std_logic; -- 200 MHz system clock
    rst_i             : in    std_logic; -- active high system reset
    ui_clk_o          : out   std_logic;
    ui_clk_sync_rst_o : out   std_logic;
    -- RAM interface
    ram_a             : in    std_logic_vector(22 downto 0);
    ram_dq_i          : in    std_logic_vector(127 downto 0);
    ram_dq_o          : out   std_logic_vector(127 downto 0);
    ram_cen           : in    std_logic;
    ram_oen           : in    std_logic;
    ram_wen           : in    std_logic;
    ram_ack           : out   std_logic;
    -- DDR2 interface
    ddr2_addr         : out   std_logic_vector(12 downto 0);
    ddr2_ba           : out   std_logic_vector(2 downto 0);
    ddr2_ras_n        : out   std_logic;
    ddr2_cas_n        : out   std_logic;
    ddr2_we_n         : out   std_logic;
    ddr2_ck_p         : out   std_logic_vector(0 downto 0);
    ddr2_ck_n         : out   std_logic_vector(0 downto 0);
    ddr2_cke          : out   std_logic_vector(0 downto 0);
    ddr2_cs_n         : out   std_logic_vector(0 downto 0);
    ddr2_odt          : out   std_logic_vector(0 downto 0);
    ddr2_dq           : inout std_logic_vector(15 downto 0);
    ddr2_dm           : out   std_logic_vector(1 downto 0);
    ddr2_dqs_p        : inout std_logic_vector(1 downto 0);
    ddr2_dqs_n        : inout std_logic_vector(1 downto 0)
  );
  end component;
  
  component bufferManager is
    port (-- System
          clk_i           : in    std_logic;
          rst_i           : in    std_logic;
          -- Control
          rel_on_pre_buf  : out std_logic; -- turn on buffer reload
          com_on_pos_buf  : out std_logic; -- turn on buffer commit
          won_i           : in  std_logic; -- enable writing
          op_i            : in  std_logic; -- enable operation
          odub_i          : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
          
          addr_i          : in  std_logic_vector(21 downto 0); -- address input 22 bits to select 32 bit sample
          addr_buf_o      : out  std_logic_vector(19 downto 0); -- buffer current address
          data_i          : in  std_logic_vector(31 downto 0); -- data to write
          pre_buf_i       : in  std_logic_vector(127 downto 0); -- data from prebuffer
          data_o          : out std_logic_vector(31 downto 0); -- data output
          pos_buf_o       : out  std_logic_vector(127 downto 0) -- data from posbuffer
          );
  end component;
  
  component prePosBufferSetReload is
  port (-- System
        clk_i           : in    std_logic;
        rst_i           : in    std_logic;
        -- Control
        rel_on_pre_buf  : in std_logic; -- turn on buffer reload
        com_on_pos_buf  : in std_logic; -- turn on buffer commit
        rel_off_pre_buf : in std_logic; -- turn on buffer reload
        com_off_pos_buf : in std_logic; -- turn on buffer commit
        -- Reload/Commit status
        com_pos_buf     : out std_logic; -- buffer commit
        rel_pre_buf     : out std_logic -- buffer reload
        );
  end component;
  
  -- System
  signal clk : std_logic;
  signal rst : std_logic;
  
  -- Ram2DDR
  signal mem_a      : std_logic_vector(22 downto 0);  -- Address
  signal mem_dq_o   : std_logic_vector(127 downto 0); -- Data Out to RAM
  signal mem_dq_i   : std_logic_vector(127 downto 0); -- Data In from RAM
  signal mem_cen    : std_logic; -- Chip Enable
  signal mem_oen    : std_logic; -- Output Enable
  signal mem_wen    : std_logic; -- Write Enable
  signal mem_ack    : std_logic; -- ack
  
  -- Buffer addresses
  signal addr_buf0 : std_logic_vector(19 downto 0); -- (128 bit word)
  signal addr_buf1 : std_logic_vector(19 downto 0);
  signal addr_buf2 : std_logic_vector(19 downto 0);
  signal addr_buf3 : std_logic_vector(19 downto 0);
  signal addr_buf4 : std_logic_vector(19 downto 0);
  signal addr_buf5 : std_logic_vector(19 downto 0);
  signal addr_buf6 : std_logic_vector(19 downto 0);
  signal addr_buf7 : std_logic_vector(19 downto 0);
  
  -- Prebuffers control signals and tags
  signal rel_pre_buf : std_logic_vector(7 downto 0);
  signal rel_on_pre_buf : std_logic_vector(7 downto 0);
  signal rel_off_pre_buf : std_logic_vector(7 downto 0);
  
  -- Prebuffers
  signal pre_buf0 : std_logic_vector(127 downto 0);
  signal pre_buf1 : std_logic_vector(127 downto 0);
  signal pre_buf2 : std_logic_vector(127 downto 0);
  signal pre_buf3 : std_logic_vector(127 downto 0);
  signal pre_buf4 : std_logic_vector(127 downto 0);
  signal pre_buf5 : std_logic_vector(127 downto 0);
  signal pre_buf6 : std_logic_vector(127 downto 0);
  signal pre_buf7 : std_logic_vector(127 downto 0);
  
  -- Postbuffers control signals and tags
  signal com_pos_buf : std_logic_vector(7 downto 0);
  signal com_on_pos_buf : std_logic_vector(7 downto 0);
  signal com_off_pos_buf : std_logic_vector(7 downto 0);
  
  -- Postbuffers 
  signal pos_buf0 : std_logic_vector(127 downto 0);
  signal pos_buf1 : std_logic_vector(127 downto 0);
  signal pos_buf2 : std_logic_vector(127 downto 0);
  signal pos_buf3 : std_logic_vector(127 downto 0);
  signal pos_buf4 : std_logic_vector(127 downto 0);
  signal pos_buf5 : std_logic_vector(127 downto 0);
  signal pos_buf6 : std_logic_vector(127 downto 0);
  signal pos_buf7 : std_logic_vector(127 downto 0);

begin
  
  -- DDR clock controls everything
  ui_clk_o <= clk;
  ui_clk_sync_rst_o <= rst; -- hi async reset
  
  DDR : Ram2Ddr
  port map (
    clk_200MHz_i      => clk_200MHz_i,
    rst_i             => rst_i,
    ui_clk_o          => clk,
    ui_clk_sync_rst_o => rst,
    -- RAM interface (from RamCntrl)
    ram_a             => mem_a,
    ram_dq_i          => mem_dq_o,
    ram_dq_o          => mem_dq_i,
    ram_cen           => mem_cen,
    ram_oen           => mem_oen,
    ram_wen           => mem_wen,
    ram_ack           => mem_ack,
    -- DDR2
    ddr2_addr         => ddr2_addr,
    ddr2_ba           => ddr2_ba,
    ddr2_ras_n        => ddr2_ras_n,
    ddr2_cas_n        => ddr2_cas_n,
    ddr2_we_n         => ddr2_we_n,
    ddr2_ck_p         => ddr2_ck_p,
    ddr2_ck_n         => ddr2_ck_n,
    ddr2_cke          => ddr2_cke,
    ddr2_cs_n         => ddr2_cs_n,
    ddr2_odt          => ddr2_odt,
    ddr2_dq           => ddr2_dq,
    ddr2_dm           => ddr2_dm,
    ddr2_dqs_p        => ddr2_dqs_p,
    ddr2_dqs_n        => ddr2_dqs_n
  );
  
  buffer0_reload : bufferManager
  port map (-- System
            clk_i           => clk,
            rst_i           => rst,
            -- Control
            rel_on_pre_buf  => rel_on_pre_buf(0),
            com_on_pos_buf  => com_on_pos_buf(0),
            won_i           => won0_i,
            op_i            => op0_i,
            odub_i          => odub0_i,
            -- Addr
            addr_i          => addr0_i,
            addr_buf_o      => addr_buf0,
            -- Data
            data_i          => data0_i,
            pre_buf_i       => pre_buf0,
            data_o          => data0_o,
            pos_buf_o       => pos_buf0
        );
  buffer1_reload : bufferManager
  port map (-- System
            clk_i           => clk,
            rst_i           => rst,
            -- Control
            rel_on_pre_buf  => rel_on_pre_buf(1),
            com_on_pos_buf  => com_on_pos_buf(1),
            won_i           => won1_i,
            op_i            => op1_i,
            odub_i          => odub1_i,
            -- Addr
            addr_i          => addr1_i,
            addr_buf_o      => addr_buf1,
            -- Data
            data_i          => data1_i,
            pre_buf_i       => pre_buf1,
            data_o          => data1_o,
            pos_buf_o       => pos_buf1
        );
  buffer2_reload : bufferManager
  port map (-- System
            clk_i           => clk,
            rst_i           => rst,
            -- Control
            rel_on_pre_buf  => rel_on_pre_buf(2),
            com_on_pos_buf  => com_on_pos_buf(2),
            won_i           => won2_i,
            op_i            => op2_i,
            odub_i          => odub2_i,
            -- Addr
            addr_i          => addr2_i,
            addr_buf_o      => addr_buf2,
            -- Data
            data_i          => data2_i,
            pre_buf_i       => pre_buf2,
            data_o          => data2_o,
            pos_buf_o       => pos_buf2
        );
  buffer3_reload : bufferManager
  port map (-- System
            clk_i           => clk,
            rst_i           => rst,
            -- Control
            rel_on_pre_buf  => rel_on_pre_buf(3),
            com_on_pos_buf  => com_on_pos_buf(3),
            won_i           => won3_i,
            op_i            => op3_i,
            odub_i          => odub3_i,
            -- Addr
            addr_i          => addr3_i,
            addr_buf_o      => addr_buf3,
            -- Data
            data_i          => data3_i,
            pre_buf_i       => pre_buf3,
            data_o          => data3_o,
            pos_buf_o       => pos_buf3
        );
  buffer4_reload : bufferManager
  port map (-- System
            clk_i           => clk,
            rst_i           => rst,
            -- Control
            rel_on_pre_buf  => rel_on_pre_buf(4),
            com_on_pos_buf  => com_on_pos_buf(4),
            won_i           => won4_i,
            op_i            => op4_i,
            odub_i          => odub4_i,
            -- Addr
            addr_i          => addr4_i,
            addr_buf_o      => addr_buf4,
            -- Data
            data_i          => data4_i,
            pre_buf_i       => pre_buf4,
            data_o          => data4_o,
            pos_buf_o       => pos_buf4
        );
  buffer5_reload : bufferManager
  port map (-- System
            clk_i           => clk,
            rst_i           => rst,
            -- Control
            rel_on_pre_buf  => rel_on_pre_buf(5),
            com_on_pos_buf  => com_on_pos_buf(5),
            won_i           => won5_i,
            op_i            => op5_i,
            odub_i          => odub5_i,
            -- Addr
            addr_i          => addr5_i,
            addr_buf_o      => addr_buf5,
            -- Data
            data_i          => data5_i,
            pre_buf_i       => pre_buf5,
            data_o          => data5_o,
            pos_buf_o       => pos_buf5
        );
  buffer6_reload : bufferManager
  port map (-- System
            clk_i           => clk,
            rst_i           => rst,
            -- Control
            rel_on_pre_buf  => rel_on_pre_buf(6),
            com_on_pos_buf  => com_on_pos_buf(6),
            won_i           => won6_i,
            op_i            => op6_i,
            odub_i          => odub6_i,
            -- Addr
            addr_i          => addr6_i,
            addr_buf_o      => addr_buf6,
            -- Data
            data_i          => data6_i,
            pre_buf_i       => pre_buf6,
            data_o          => data6_o,
            pos_buf_o       => pos_buf6
        );
  buffer7_reload : bufferManager
  port map (-- System
            clk_i          => clk,
            rst_i          => rst,
            -- Control
            rel_on_pre_buf => rel_on_pre_buf(7),
            com_on_pos_buf => com_on_pos_buf(7),
            won_i          => won7_i,
            op_i           => op7_i,
            odub_i         => odub7_i,
            -- Addr
            addr_i         => addr7_i,
            addr_buf_o     => addr_buf7,
            -- Data
            data_i         => data7_i,
            pre_buf_i      => pre_buf7,
            data_o         => data7_o,
            pos_buf_o      => pos_buf7
        );
  
  pre_pos_buffer_set_reload_placer :
  for i in (7) downto 0 generate
    pre_pos_buffer_set_reload : prePosBufferSetReload
    port map (
      clk_i           => clk,
      rst_i           => rst,
      rel_on_pre_buf  => rel_on_pre_buf(i),
      com_on_pos_buf  => com_on_pos_buf(i),
      rel_off_pre_buf => rel_off_pre_buf(i),
      com_off_pos_buf => com_off_pos_buf(i),
      com_pos_buf     => com_pos_buf(i),
      rel_pre_buf     => rel_pre_buf(i)
    );
  end generate;
  
  
  mem_reload : process(clk, rst, mem_ack, pos_buf0, addr_buf0, pos_buf1, addr_buf1,
                       pos_buf2, addr_buf2, pos_buf3, addr_buf3, pos_buf4, addr_buf4,
                       pos_buf5, addr_buf5, pos_buf6, addr_buf6, pos_buf7, addr_buf7)
    type mem_state is (wait_req,    
                       load_buf,    
                       commit_buf,  
                       wait_wr_ack, 
                       wait_rd_ack);
    variable mem_reload_state : mem_state;
    variable index : natural range 0 to 7;
    variable pos_buf : std_logic_vector(127 downto 0);
    variable addr_buf : std_logic_vector(19 downto 0);
  begin
  
    -- Combinational defaults
    mem_oen  <= '1';
    mem_wen  <= '1';
    mem_cen  <= '1'; -- memory enable
    mem_a    <= (others=>'0');
    mem_dq_o <= (others=>'0');
    
    rel_off_pre_buf <= (others=>'0');
    com_off_pos_buf <= (others=>'0');
    
    -- Combi input signals
    case index is
      when 0 =>
        pos_buf  := pos_buf0;
        addr_buf := addr_buf0;
      when 1 =>
        pos_buf  := pos_buf1;
        addr_buf := addr_buf1;
      when 2 =>
        pos_buf  := pos_buf2;
        addr_buf := addr_buf2;
      when 3 =>
        pos_buf  := pos_buf3;
        addr_buf := addr_buf3;
      when 4 =>
        pos_buf  := pos_buf4;
        addr_buf := addr_buf4;
      when 5 =>
        pos_buf  := pos_buf5;
        addr_buf := addr_buf5;
      when 6 =>
        pos_buf  := pos_buf6;
        addr_buf := addr_buf6;
      when 7 =>
        pos_buf  := pos_buf7;
        addr_buf := addr_buf7;
      when others =>
        pos_buf  := (others=>'0');
        addr_buf := (others=>'0');
    end case;
  
    case mem_reload_state is
      when wait_req =>
      when load_buf =>
        if mem_ack = '0' then
          mem_oen <= '0';
          mem_wen <= '1';
          mem_cen <= '0'; -- memory enable
          mem_a <= std_logic_vector(to_unsigned(index, 3) & unsigned(unsigned(addr_buf) + 1));
        else
          rel_off_pre_buf(index) <= '1';
        end if;
      when commit_buf =>
        if mem_ack = '0' then
          mem_oen <= '1';
          mem_wen <= '0';
          mem_cen <= '0'; -- memory enable
          mem_a <= std_logic_vector(to_unsigned(index, 3) & (unsigned(addr_buf) - 1));
          mem_dq_o <= pos_buf;
        else
          com_off_pos_buf(index) <= '1';
        end if;
      when others =>
    end case;
  
    if rst = '1' then
      pre_buf0 <= (others=>'0');
      pre_buf1 <= (others=>'0');
      pre_buf2 <= (others=>'0');
      pre_buf3 <= (others=>'0');
      pre_buf4 <= (others=>'0');
      pre_buf5 <= (others=>'0');
      pre_buf6 <= (others=>'0');
      pre_buf7 <= (others=>'0');
      index := 0;
    
      mem_reload_state := wait_req;
    elsif rising_edge(clk) then
      case mem_reload_state is
        when wait_req =>
          if rel_pre_buf(index) = '1' then
            mem_reload_state := load_buf;
          elsif com_pos_buf(index) = '1' then
            mem_reload_state := commit_buf;
          else
            index := index + 1;
          end if;
  
        when load_buf =>
          if mem_ack = '1' then
            case index is
              when 0 => pre_buf0 <= mem_dq_i;
              when 1 => pre_buf1 <= mem_dq_i;
              when 2 => pre_buf2 <= mem_dq_i;
              when 3 => pre_buf3 <= mem_dq_i;
              when 4 => pre_buf4 <= mem_dq_i;
              when 5 => pre_buf5 <= mem_dq_i;
              when 6 => pre_buf6 <= mem_dq_i;
              when 7 => pre_buf7 <= mem_dq_i;
              when others =>
            end case;
            if com_pos_buf(index) = '1' then
              mem_reload_state := commit_buf;
            else
              mem_reload_state := wait_req;
              index := index + 1;
            end if;
          end if;
  
        when commit_buf =>
          if mem_ack = '1' then
            mem_reload_state := wait_req;
            index := index + 1;
          end if;
          
        when others =>
      end case;
    end if;
  end process;

end syn;
