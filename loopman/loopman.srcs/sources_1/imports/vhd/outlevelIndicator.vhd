-- outlevelIndicator.vhd --------------------------------------------------------------------
--
-- Calculates how high is the amplitude of a sample. Sensitivity is adjustable via
-- threshold_step parameter.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity outlevelIndicator is
  generic (
    threshold_step : natural := 559_240 -- Sensitivity
  );
  port (
    sample_i   : in int_sample(0 downto 0);
    outlevel_o : out std_logic_vector(3 downto 0)
  );
end outlevelIndicator;

architecture syn of outlevelIndicator is

  constant threshold_max  : natural := 2_147_483_647;

begin

  process
    type natural_vector is array (integer range <>) of natural;
    variable threshold_preset : natural_vector(15 downto 0);
    variable output_val : natural;

  begin
    outlevel_o <= (others=>'0');
    if sample_i(0)(31) = '0' then
      output_val := to_integer(signed(sample_i(0)));
    else
      output_val := to_integer(-signed(sample_i(0)));
    end if;
  
    for i in 0 to 15 loop
      threshold_preset(i) := i*threshold_step;
      
      if output_val > threshold_preset(i) then
        outlevel_o <= std_logic_vector(to_unsigned(i, 4));
      end if;
    end loop;
  end process;

  -- Symmetric verision to save hardware, threshold step cannot be set (is default).
--  process
--    type natural_vector is array (integer range <>) of natural;
--    variable abs_sample : std_logic_vector(31 downto 0);

--  begin
--    if sample_i(0)(31) = '0' then
--      abs_sample := std_logic_vector(signed(sample_i(0)));
--    else
--      abs_sample := std_logic_vector(-signed(sample_i(0)));
--    end if;
    
--    outlevel_o <= abs_sample(30 downto 27);
--  end process;

end syn;
