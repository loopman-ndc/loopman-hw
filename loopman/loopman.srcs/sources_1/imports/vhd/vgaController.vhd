-- vgaController.vhd -------------------------------------------------------------------------
--
-- Generates graphics for VGA monitor.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity vgaController is
  generic ( OUTPUTS : natural := 2 );
  port (
    rst_i                : in  std_logic; 
    clk_i                : in  std_logic;
    -- VGA
    hs_o                 : out std_logic;
    vs_o                 : out std_logic;
    color_o              : out std_logic_vector(11 downto 0);
    -- Track status
    st_rec_i             : in std_logic_vector(7 downto 0);      
    st_play_i            : in std_logic_vector(7 downto 0);      
    st_mute_i            : in std_logic_vector(7 downto 0);      
    st_solo_i            : in std_logic_vector(7 downto 0);      
    st_sync_i            : in std_logic_vector(7 downto 0);      
    st_beat_i            : in std_logic_vector((8*5)-1 downto 0);
    st_looplen_i         : in std_logic_vector((8*5)-1 downto 0);
    st_vol_i             : in std_logic_vector((8*4)-1 downto 0);
    st_odub_i            : in std_logic_vector((8*4)-1 downto 0);
    st_outlevel_i        : in std_logic_vector((8*4)-1 downto 0);
    t_tempo_i            : in std_logic;
    st_master_vol_i      : in std_logic_vector((OUTPUTS*4)-1 downto 0);
    st_master_outlevel_i : in std_logic_vector((OUTPUTS*4)-1 downto 0);
    st_master_clipping_i : in std_logic_vector(OUTPUTS-1 downto 0);
    track_select_i       : in  std_logic_vector(7 downto 0)
  );
end vgaController;

architecture syn of vgaController is

  type vga_element is array (integer range <>) of std_logic_vector(11 downto 0);

  constant CLKFREQ : natural := 75_000_000;
  
  component vgaInterface is
    generic(
      FREQ      : natural; 
      SYNCDELAY : natural  
    );
    port ( 
      -- host side
      rst   : in  std_logic;   
      clk   : in  std_logic;   
      line  : out std_logic_vector(9 downto 0);   
      pixel : out std_logic_vector(9 downto 0);   
      R     : in  std_logic_vector(3 downto 0);   
      G     : in  std_logic_vector(3 downto 0);   
      B     : in  std_logic_vector(3 downto 0);   
      -- VGA side
      hSync : out std_logic;  
      vSync : out std_logic;   
      RGB   : out std_logic_vector(11 downto 0) 
    );
  end component;

  component vgaTrack is
      generic(
         PLACE  : natural
      );
      port ( 
        -- VGA
        line    : in   unsigned(9 downto 0);   
        pixel   : in   unsigned(9 downto 0);  
        color   : out  std_logic_vector(11 downto 0);
        -- Estado
        isOn    : in std_logic;
        isRec   : in std_logic;
        isMute  : in std_logic;
        isSolo  : in std_logic;
        isLen   : in std_logic_vector(4 downto 0);
        isBeat  : in std_logic_vector(4 downto 0);
        isVol   : in std_logic_vector(3 downto 0);
        isLevel : in std_logic_vector(3 downto 0);
    
        isSel   : in std_logic 
      );
  end component;
  
  component vgaMaster is
  generic(
    PLACE      : natural 
  );
  port ( 
    -- VGA
    line  : in   unsigned(9 downto 0); 
    pixel : in   unsigned(9 downto 0);  
    color : out  std_logic_vector(11 downto 0);
    -- Estado
    isMute   : in  std_logic;
    isVol    : in  std_logic_vector(3 downto 0);
    mixLevel : in  std_logic_vector(3 downto 0)
  );
  end component;
  
  type romNum is array (0 to 63) of std_logic; 
  signal rom1 : romNum := (
    X"00_00_00_08_08_08_08_00"
  );

  signal rom2 : romNum := (
    X"00_00_00_3C_0C_10_3C_00"
  );
  
  signal rom3 : romNum := (
    X"00_00_00_3C_1C_04_3C_00"
  );
  
  signal rom4 : romNum := (
    X"00_00_00_20_24_3C_04_00"
  );
  
  signal rom5 : romNum := (
    X"00_00_00_3C_30_0C_3C_00"
  );
  
  signal rom6 : romNum := (
    X"00_00_00_20_3C_24_3C_00"
  );
  
  signal rom7 : romNum := (
    X"00_00_00_3C_04_08_10_00"
  );
  
  signal rom8 : romNum := (
    X"00_00_00_1C_34_2C_38_00"
  );
  
  type romLogo is array (0 to (32*16)-1) of std_logic;
  signal romLoopMan : romLogo := (
    X"00_00_00_00" &
    X"00_00_00_00" & 
    X"80_00_00_00" & 
    X"80_00_00_00" & 
    X"80_00_8B_DA" & 
    X"80_00_DA_5A" & 
    X"80_00_FA_5E" & 
    X"80_00_AA_56" & 
    X"BF_EC_8B_D6" &
    X"80_0A_8A_52" & 
    X"BF_EA_8A_52" & 
    X"A5_2A_8A_52" & 
    X"A5_2C_8A_52" & 
    X"BD_E8_8A_52" & 
    X"80_08_8A_52" & 
    X"FF_E8_8A_52"
  );
  
  signal romAddrNum : std_logic_vector(5 downto 0);
  signal romAddrLogo : std_logic_vector(8 downto 0);

  signal color : std_logic_vector(11 downto 0);
  signal line, pixel : std_logic_vector(9 downto 0);
  
  signal metronome, borderLine : std_logic_vector(11 downto 0);
         
  signal colorTrack  : vga_element(7 downto 0);
  signal colorMaster : vga_element(1 downto 0);
            
  signal showTempo  : std_logic; -- Tempo comes one cycle and then goes like lost trains in life. With this and a FSM we will hold it fors a while.
  
  signal bitMap1, bitMap2, bitMap3, bitMap4, bitMap5, bitMap6, bitMap7, bitMap8, bitMapLogo : std_logic;
  
  signal uno, dos, tre, cua, cin, sei, sie, och, logo : std_logic_vector(11 downto 0);
  
begin

  color <= colorTrack(0) or colorTrack(1) or colorTrack(2) or colorTrack(3) or
           colorTrack(4) or colorTrack(5) or colorTrack(6) or colorTrack(7) or
           colorMaster(0) or colorMaster(1) or
           metronome or borderLine or
           uno or dos or tre or cua or cin or sei or sie or och or logo;

  screenInteface : vgaInterface
    generic map ( FREQ => 75_000, SYNCDELAY => 0 )
    port map ( rst => rst_i, clk => clk_i, line => line, pixel => pixel, R => color(11 downto 8), G => color(7 downto 4), B => color(3 downto 0), hSync => hs_o, vSync => vs_o, RGB => color_o );

  tracks : for i in 0 to 7 generate
    track : vgaTrack
    generic map (PLACE => i*64)
    port map (
      line => unsigned(line),
      pixel => unsigned(pixel),
      color => colorTrack(i),
      isOn => st_play_i(i),
      isRec => st_rec_i(i),
      isMute => st_mute_i(i),
      isSolo => st_solo_i(i),
      isLen => st_looplen_i(((i+1)*5)-1 downto i*5),
      isBeat => st_beat_i(((i+1)*5)-1 downto i*5),
      isVol => st_vol_i(((i+1)*4)-1 downto i*4),
      isLevel => st_outlevel_i(((i+1)*4)-1 downto i*4),
      isSel => track_select_i(i)
    );
  end generate;
  
  outs : for i in 0 to 1 generate
   master : vgaMaster 
      generic map (PLACE => (8+i)*64)
      port map ( 
        -- VGA
        line => unsigned(line),
        pixel => unsigned(pixel),
        color => colorMaster(i),
        -- Estado
        isMute   => st_master_clipping_i(i),
        isVol    => st_master_vol_i(((i+1)*4)-1 downto i*4),
        mixLevel => st_master_outlevel_i(((i+1)*4)-1 downto i*4)
      );
    end generate;
              
  metronome <= "1111" & "1111" & "1111" when (unsigned(pixel) > (512) and unsigned(pixel) < (640-8) and unsigned(line) < (16+64+48) and unsigned(line) > (16+64+8)) and showTempo = '1' else
               "0000" & "0000" & "0000";
  
  borderLine <= "1111" & "1111" & "1111" when ((unsigned(pixel)=1 or unsigned(pixel)=638) and unsigned(line) >= 1 and unsigned(line) <= 478) or ((unsigned(line)=1 or unsigned(line)=478) and unsigned(pixel) >= 1 and unsigned(pixel) <= 638) else
                "0000" & "0000" & "0000";
  
  romAddrNum <= line(5 downto 3) & pixel(5 downto 3);
  romAddrLogo <= line(5 downto 2) & pixel(6 downto 2);

  romGraphics :
  process (clk_i)
  begin
    if rising_edge(clk_i) then
      bitMap1 <= rom1(to_integer(unsigned(romAddrNum)));
        bitMap2 <= rom2(to_integer(unsigned(romAddrNum)));
        bitMap3 <= rom3(to_integer(unsigned(romAddrNum)));
        bitMap4 <= rom4(to_integer(unsigned(romAddrNum)));
        bitMap5 <= rom5(to_integer(unsigned(romAddrNum)));
        bitMap6 <= rom6(to_integer(unsigned(romAddrNum)));
        bitMap7 <= rom7(to_integer(unsigned(romAddrNum)));
        bitMap8 <= rom8(to_integer(unsigned(romAddrNum)));
        bitMapLogo <= romLoopMan(to_integer(unsigned(romAddrLogo)));
    end if;
  end process;
  
  uno  <= "1111" & "1111" & "1111" when bitMap1 = '1'    and unsigned(pixel) > 0   and unsigned(pixel) < 64  and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  dos  <= "1111" & "1111" & "1111" when bitMap2 = '1'    and unsigned(pixel) > 64  and unsigned(pixel) < 128 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  tre  <= "1111" & "1111" & "1111" when bitMap3 = '1'    and unsigned(pixel) > 128 and unsigned(pixel) < 192 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  cua  <= "1111" & "1111" & "1111" when bitMap4 = '1'    and unsigned(pixel) > 192 and unsigned(pixel) < 256 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  cin  <= "1111" & "1111" & "1111" when bitMap5 = '1'    and unsigned(pixel) > 256 and unsigned(pixel) < 320 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  sei  <= "1111" & "1111" & "1111" when bitMap6 = '1'    and unsigned(pixel) > 320 and unsigned(pixel) < 384 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  sie  <= "1111" & "1111" & "1111" when bitMap7 = '1'    and unsigned(pixel) > 384 and unsigned(pixel) < 448 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  och  <= "1111" & "1111" & "1111" when bitMap8 = '1'    and unsigned(pixel) > 448 and unsigned(pixel) < 512 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  logo <= "1111" & "0000" & "1111" when bitMapLogo = '1' and unsigned(pixel) > 512 and unsigned(pixel) < 639 and unsigned(line) > 0 and unsigned(line) < 64 else "0000" & "0000" & "0000";
  
  -- FSMT to show tempo
  tempoVisualizer :
  process (rst_i, clk_i)
    constant maxValue : natural := CLKFREQ/10-1; -- 0.1 secs on
    type states is (idle, show);
    variable state : states;
    variable count : natural range 0 to maxValue;
  begin
    showTempo <= '0';
  
    case state is
      when idle =>
        showTempo <= '0';
      when show =>
        showTempo <= '1';
    end case;
  
    if rst_i = '1' then
      state := idle;
      count := maxValue;
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          count := maxValue;
          if t_tempo_i = '1' then
            state := show;
          end if;
        when show =>
          if t_tempo_i = '1' or count = 0 then
            -- Count ended or blinking is faster
            state := idle;
          else -- count /= 0
            count := count - 1;
          end if;
      end case;
    end if;
  end process;
  
end syn;

