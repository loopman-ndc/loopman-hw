-- tempoGenerator.vhd ------------------------------------------------------------------------
-- 
-- Generates main tempo clock (ticks, work with one cycle pulses, not edges). Tempo hit button
-- measuring and manual tempo set operations also performed within this entity.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tempoGenerator is
  port (
    -- System
    clk_i              : in std_logic;
    rst_i              : in std_logic;
    -- Tempo setup
    set_value_i        : in std_logic_vector(15 downto 0);
    t_set_tempo_i      : in std_logic;
    t_hit_tempo_i      : in std_logic;
    -- Track sync
    t_sync_tempo_i     : in std_logic;
    -- Tempo output
    tempo_value_o      : out std_logic_vector(15 downto 0);
    t_ch_tempo_value_o : out std_logic;
    t_tempo_o          : out std_logic
  );
end tempoGenerator;

architecture syn of tempoGenerator is

  constant max_value : natural := 99_999_999; -- 1.333 secs, or 45 bpm

  signal tempo_ticks, measured_ticks : unsigned(27 downto 0);
  signal measure_rdy : std_logic;

begin

  tempo_value_o <= std_logic_vector(tempo_ticks(27 downto 12));

  tempo_generator : process (clk_i, rst_i, tempo_ticks, t_sync_tempo_i)
    variable count : unsigned(27 downto 0);
  begin
    t_tempo_o <= '0';
    if count >= tempo_ticks or t_sync_tempo_i = '1' then
      t_tempo_o <= '1';
    end if;

    if rst_i = '1' then
      count := (others=>'0');
    elsif rising_edge(clk_i) then
      if count >= tempo_ticks or t_sync_tempo_i = '1' then
        count := (others=>'0');
      else
        count := count + 1;
      end if;
    end if;
  end process;

  tempo_value_setter : process (clk_i, rst_i)
  begin
    if rst_i = '1' then
      -- Default tempo: 120 bpm
      tempo_ticks <= "0010" & "0011" & "1100" & "0011" & "0100" & "0110" & "0000";
      t_ch_tempo_value_o <= '0';
    elsif rising_edge(clk_i) then
      t_ch_tempo_value_o <= '0';
      if t_set_tempo_i = '1' then
        tempo_ticks <= unsigned(set_value_i & "000000000000");
        t_ch_tempo_value_o <= '1';
      elsif measure_rdy = '1' then
        tempo_ticks <= measured_ticks;
        t_ch_tempo_value_o <= '1';
      end if;
    end if;
  end process;

  tempo_measurer : process (clk_i, rst_i)
    type states is (idle, work, process_measure, ready);
    
    constant fixed_095 : unsigned(31 downto 0) := "00000000" & "00000000" & "00000000" & "0000" & "1110";
    constant fixed_105 : unsigned(31 downto 0) := "00000000" & "00000000" & "00000000" & "0001" & "0001";
    
    constant max_ticks : unsigned(27 downto 0) := "1000" & "1111" & "0000" & "1101" & "0001" & "1000" & "0000";
    
    variable state : states;
    variable tick_avg, tick_count, tick_measure : unsigned(27 downto 0);
    variable tick_avg_fp : unsigned(31 downto 0);
    variable min_below_avg, max_above_avg : unsigned(27 downto 0);
    variable min_below_avg_fp, max_above_avg_fp : unsigned(63 downto 0);
  begin
    tick_avg_fp := tick_avg & "0000";
    
    min_below_avg_fp := tick_avg_fp * fixed_095;
    max_above_avg_fp := tick_avg_fp * fixed_105;
    
    min_below_avg := min_below_avg_fp(35 downto 8);
    max_above_avg := max_above_avg_fp(35 downto 8);
    
    measure_rdy <= '0';
    
    case state is
      when idle =>
      when work =>
      when process_measure =>
      when ready =>
        measure_rdy <= '1';
      when others =>
    end case;

    if rst_i = '1' then
      state := idle;
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if t_hit_tempo_i = '1' then
            tick_avg := (others=>'0');
            tick_count := (others=>'0');
            
            state := work;
          end if;

        when work =>
          if t_set_tempo_i = '1' then
            -- Stops measure
            state := idle;
          elsif t_hit_tempo_i = '1' then
            -- Register ticks and update average
            tick_measure := tick_count;
            tick_count := (others=>'0');

            state := process_measure;
          elsif tick_count >= max_ticks then
            -- Timeout
            state := idle;
          else
            tick_count := tick_count + 1;
          end if;

        when process_measure =>
          if tick_avg = 0 then
            -- First measure
            tick_avg := tick_measure;

            state := ready;
          elsif tick_measure >= min_below_avg and tick_measure <= max_above_avg then
            -- Other measure within range, adds value to average
            tick_avg := ('0' & tick_avg(27 downto 1)) + ('0' & tick_measure(27 downto 1));
            
            state := ready;
          else
            -- Other measure off range, starts new measurement
            tick_avg := tick_measure;

            state := ready;
          end if;
          
          measured_ticks <= tick_avg;

        when ready =>
          state := work;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;

end syn;
