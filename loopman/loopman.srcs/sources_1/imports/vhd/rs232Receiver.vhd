-- rs232Receiver.vhd --------------------------------------------------------------------------
--
-- Serial to parallel rs232 conversor.
--    - Parity: NONE
--    - Num data bits: 8
--    - Num stop bits: 1
--
-- (c) J.M. Mendias
-- Dise�o Autom�tico de Sistemas
-- Facultad de Inform�tica. Universidad Complutense de Madrid
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity rs232Receiver is
  generic (
    FREQ     : natural;  -- operation frequency in KHz  
    BAUDRATE : natural   -- transmission speed          
  );
  port (
    -- host side
    rst     : in  std_logic;   -- async reset
    clk     : in  std_logic;   -- clock
    dataRdy : out std_logic;   -- activates for one cycle when input data is ready
    data    : out std_logic_vector (7 downto 0);   -- received data
    -- RS232 side
    RxD     : in  std_logic    -- Serial RS-232 data input
  );
end rs232Receiver;

-------------------------------------------------------------------

use work.common.all;

architecture syn of rs232Receiver is

  -- Registros
  signal bitPos : natural range 0 to 10;   
  signal RxDShf : std_logic_vector (7 downto 0);
  -- Se�ales
  signal RxDSync : std_logic;
  signal readRxD, baudCntCE : std_logic;

begin

  RxDSynchronizer : synchronizer
    generic map ( STAGES => 2, INIT => '1' )
    port map ( rst => rst, clk => clk, x => RxD, xSync => RxDSync );

  baudCnt:
  process (rst, clk)
    constant numCycles : natural := (FREQ*1000)/BAUDRATE;
    constant maxValue  : natural := numCycles-1;
    variable count     : natural range 0 to maxValue;
  begin
    readRxD <= '0';
    if count = maxValue/2 then
      readRxD <= '1';
    end if;
    if rst='1' then
      count := 0;
    elsif rising_edge(clk) then
      if baudCntCE='0' then
        count := 0;
      else
        if count=maxValue then
          count := 0;
        else
          count := count + 1;
        end if;
      end if;
    end if;
  end process;
  
  fsmd :
  process (rst, clk, bitPos, readRxD, RxDShf)
  begin
    data      <= RxDShf;
    baudCntCE <= '1';
    dataRdy   <= '0';
    if bitPos=0 then
      baudCntCE <= '0';
    end if;
    if bitPos=10 and readRxD='1' then
      dataRdy <= '1';
    end if;
    if rst='1' then
      RxDShf <= (others =>'0'); 
      bitPos <= 0;
    elsif rising_edge(clk) then
      case bitPos is
        when 0 =>                -- Waits for start bit
          if RxDSync='0' then
            bitPos <= 1;
          end if;
        when 1 =>                -- Ignores start bit
          if readRxD='1' then
            bitPos <= bitPos + 1;
          end if;
        when 10 =>               -- Ignores stop bit
          if readRxD='1' then    
            bitPos <= 0;
          end if;
        when others =>           -- Shifts
          if readRxD='1' then 
            RxDShf <= RxDSync & RxDShf(7 downto 1);
            bitPos <= bitPos + 1;
          end if;
      end case;
    end if;
  end process;
  
end syn;
