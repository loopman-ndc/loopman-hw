-- rs232Transmitter.vhd ----------------------------------------------------------------------
--
-- Parallel to serial (RS-232) converter, strobe protocol.
-- - Parity: NONE    
-- - Num data bits: 8
-- - Num stop bits: 1
--
-- (c) J.M. Mendias
-- Dise�o Autom�tico de Sistemas
-- Facultad de Inform�tica. Universidad Complutense de Madrid
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity rs232Transmitter is
  generic (
    FREQ     : natural;  -- operation frequency in KHz
    BAUDRATE : natural   -- transmission speed
  );
  port (
    -- host side
    rst_i      : in  std_logic;   -- reset
    clk_i      : in  std_logic;   -- clock
    data_rdy_i : in  std_logic;   -- when data is ready to send, set to one for one cycle
    data_i     : in  std_logic_vector (7 downto 0);   -- data to send
    busy_o     : out std_logic;   -- activates while transmitting
    -- RS232 side
    txd_o      : out std_logic    -- rs-232 interface serial output
  );
end rs232Transmitter;

-------------------------------------------------------------------

use work.common.all;

architecture syn of rs232Transmitter is

  -- Registros
  signal bitPos : natural range 0 to 10;   
  signal TxDShf : std_logic_vector(9 downto 0);
  -- Se�ales
  signal baudCntCE, writeTxD : std_logic;

begin

  baudCnt:
  process (rst_i, clk_i)
    constant numCycles : natural := (FREQ*1000)/BAUDRATE;
    constant maxValue  : natural := numCycles-1;
    variable count     : natural range 0 to maxValue;
  begin
    writeTxD <= '0';
    if count=maxValue then
      writeTxD <= '1';
    end if;
    if rst_i='1' then
      count := 0;
    elsif rising_edge(clk_i) then
      if baudCntCE='0' then
        count := 0;
      else
        if count=maxValue then
          count := 0;
        else
          count := count + 1;
        end if;
      end if;
    end if;
  end process;
  
  fsmd :
  process (rst_i, clk_i, bitPos, TxDShf)
  begin
    txd_o     <= TxDShf(0);
    baudCntCE <= '1';
    busy_o    <= '1';
    if bitPos=0 then
      baudCntCE <= '0';
      busy_o    <= '0';
    end if;
    if rst_i='1' then
      TxDShf <= (others =>'1'); 
      bitPos <= 0;
    elsif rising_edge(clk_i) then
      case bitPos is
        when 0 =>                              -- Waits for send request
          if data_rdy_i='1' then
            TxDShf <= "1" & data_i & "0";
            bitPos <= 1;
          end if;
        when 10 =>                             
          if writeTxD='1' then                 -- Shifts
            TxDShf <= "1" & TxDShf(9 downto 1);
            bitPos <= 0;
          end if;
        when others =>                         -- Shifts
          if (writeTxD = '1') then 
            TxDShf <= "1" & TxDShf(9 downto 1);
            bitPos <= bitPos + 1;
          end if;
      end case;
    end if;
  end process;
  
end syn;

