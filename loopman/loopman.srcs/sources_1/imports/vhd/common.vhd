-- common.vhd ----------------------------------------------------------------------------
--
-- Constants, system parameters, vhdl types, reusable components...
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package common is

  -- Boolean constants
  constant YES  : std_logic := '1';
  constant NO   : std_logic := '0';
  constant HI   : std_logic := '1';
  constant LO   : std_logic := '0';
  constant ONE  : std_logic := '1';
  constant ZERO : std_logic := '0';
  
  function log2(v : in natural) return natural;
  function int_select(s : in boolean; a : in integer; b : in integer) return integer;
  function toFix( d: real; qn : natural; qm : natural ) return signed; 
  
  -- System parameters
  constant SYS_INPUTS      : natural := 4;
  constant SYS_OUTPUTS     : natural := 2;
  constant SYS_AUDIO_WIDTH : natural := 24;
  constant SYS_FX_NO       : natural := 9;

  -- Types
  type int_sample is array (integer range <>) of std_logic_vector(31 downto 0);
  type ext_sample is array (integer range <>) of std_logic_vector(23 downto 0);
  
  -- Bluetooth frames
  constant BTF_metadata_byte    : std_logic_vector(7 downto 0) := "00100101";
  -- MODE
  constant BTF_mode_reset       : std_logic_vector(4 downto 0) := "00000";
  constant BTF_mode_track       : std_logic_vector(4 downto 0) := "00001";
  constant BTF_mode_fx          : std_logic_vector(4 downto 0) := "00010";
  constant BTF_mode_master      : std_logic_vector(4 downto 0) := "00011";
  constant BTF_mode_forbidden   : std_logic_vector(4 downto 0) := "00100"; -- Forbidden to avoid metadata collision
  constant BTF_mode_tempo       : std_logic_vector(4 downto 0) := "00101";
  -- OPTION
  constant BTF_opt_rec          : std_logic_vector(7 downto 0) := "00000001";
  constant BTF_opt_play         : std_logic_vector(7 downto 0) := "00000010";
  constant BTF_opt_mute         : std_logic_vector(7 downto 0) := "00000011";
  constant BTF_opt_solo         : std_logic_vector(7 downto 0) := "00000100";
  constant BTF_opt_clear        : std_logic_vector(7 downto 0) := "00000101";
  constant BTF_opt_sync         : std_logic_vector(7 downto 0) := "00000110";
  constant BTF_opt_auto_rec     : std_logic_vector(7 downto 0) := "00000111";
  constant BTF_opt_track_status : std_logic_vector(7 downto 0) := "00001000";
  constant BTF_opt_beat         : std_logic_vector(7 downto 0) := "00001001";
  constant BTF_opt_inc_looplen  : std_logic_vector(7 downto 0) := "00001010";
  constant BTF_opt_dec_looplen  : std_logic_vector(7 downto 0) := "00001011";
  constant BTF_opt_set_looplen  : std_logic_vector(7 downto 0) := "00001100";
  constant BTF_opt_inc_vol      : std_logic_vector(7 downto 0) := "00001101";
  constant BTF_opt_dec_vol      : std_logic_vector(7 downto 0) := "00001110";
  constant BTF_opt_set_vol      : std_logic_vector(7 downto 0) := "00001111";
  constant BTF_opt_inc_odub     : std_logic_vector(7 downto 0) := "00010000";
  constant BTF_opt_dec_odub     : std_logic_vector(7 downto 0) := "00010001";
  constant BTF_opt_set_odub     : std_logic_vector(7 downto 0) := "00010010";
  constant BTF_opt_input_sel    : std_logic_vector(7 downto 0) := "00010011";
  constant BTF_opt_output_sel   : std_logic_vector(7 downto 0) := "00010100";
  constant BTF_opt_outlevel     : std_logic_vector(7 downto 0) := "00010101";
  constant BTF_opt_inc_comp     : std_logic_vector(7 downto 0) := "00010110";
  constant BTF_opt_dec_comp     : std_logic_vector(7 downto 0) := "00010111";
  constant BTF_opt_set_comp     : std_logic_vector(7 downto 0) := "00011000";
  -- FX OPTION
  constant BTF_fxopt_clear      : std_logic_vector(7 downto 0) := "00000001";
  constant BTF_fxopt_enqueue    : std_logic_vector(7 downto 0) := "00000010";
  constant BTF_fxopt_enable     : std_logic_vector(7 downto 0) := "00000011";
  constant BTF_fxopt_set_value  : std_logic_vector(7 downto 0) := "10000000";
  -- TRACK STATUS INDEX
  constant BTF_TS_rec           : natural := 0;
  constant BTF_TS_play          : natural := 1;
  constant BTF_TS_mute          : natural := 2;
  constant BTF_TS_solo          : natural := 3;
  constant BTF_TS_sync          : natural := 4;
  constant BTF_TS_auto_rec      : natural := 5;
                  
  component audioDimmer is
    port (
      -- Control
      volume_i    : in  std_logic_vector(3 downto 0);
      -- Audio
      input_i     : in  int_sample(0 downto 0);
      output_o    : out int_sample(0 downto 0)
    );
  end component;
  
  component audioBooster is
    port (
      -- Control
      volume_i    : in  std_logic_vector(3 downto 0);
      -- Audio
      input_i     : in  int_sample(0 downto 0);
      output_o    : out int_sample(0 downto 0)
    );
  end component;
  
  component audioMixerCombi is
    port (
      -- Control
      clipping_o  : out std_logic;
      -- Audio
      input0_i    : in  int_sample(0 downto 0);
      input1_i    : in  int_sample(0 downto 0);
      mix_o       : out int_sample(0 downto 0)
    );
  end component;
  
  component audioMixerManualBalance is
    port (
      -- System
      rst_i       : in std_logic;
      clk_i       : in std_logic;
      -- Control
      input_rdy_i : in  std_logic;
      mix_rdy_o   : out std_logic;
      clipping_o  : out std_logic;
      balance_i   : in  std_logic_vector(3 downto 0);
      -- Audio
      input0_i    : in  int_sample(0 downto 0);
      input1_i    : in  int_sample(0 downto 0);
      mix_o       : out int_sample(0 downto 0)
    );
  end component;
  
  component outlevelIndicator is
    generic (
      threshold_step : natural := 559_240
    );
    port (
      sample_i   : in int_sample(0 downto 0);
      outlevel_o : out std_logic_vector(3 downto 0)
    );
  end component;
  
  component synchronizer is
    generic (                                                      
      STAGES  : in natural;    -- no of flip-flops in synchronizer 
      INIT    : in std_logic   -- flip-flops initial value         
    );                                                             
    port (                                                         
      rst   : in  std_logic;   -- async reset                      
      clk   : in  std_logic;   -- clock                            
      x     : in  std_logic;   -- input to synchronize             
      xSync : out std_logic    -- synchronized output              
    );                                                             
  end component;
  
  component edgeDetector is
    port (
      rst   : in  std_logic;   -- reset as�ncrono del sistema (a baja)
      clk   : in  std_logic;   -- reloj del sistema
      x_n   : in  std_logic;   -- entrada binaria con flancos a detectar (a baja en reposo)
      xFall : out std_logic;   -- se activa durante 1 ciclo cada vez que detecta un flanco de subida en x
      xRise : out std_logic    -- se activa durante 1 ciclo cada vez que detecta un flanco de bajada en x
    );
  end component;
  
  component debouncer is
    generic(
      FREQ   : natural;  -- operation frecuency
      BOUNCE : natural   -- bounce time (miliseconds)
    );
    port (
      rst    : in  std_logic;   -- async reset
      clk    : in  std_logic;   -- clock
      x_n    : in  std_logic;   -- input to debounce (idle is low)
      xdeb_n : out std_logic    -- debounced output
    );
  end component;
  
  component fifo is
    generic (
      WIDTH : natural;
      DEPTH : natural 
    );
    port (
      rst     : in  std_logic; 
      clk     : in  std_logic; 
      wrE     : in  std_logic;
      dataIn  : in  std_logic_vector(WIDTH-1 downto 0);
      rdE     : in  std_logic;  
      dataOut : out std_logic_vector(WIDTH-1 downto 0);
      full    : out std_logic;
      empty   : out std_logic 
    );
    end component;

end package common;
-------------------------------------------------------------------

package body common is

  function log2(v : in natural) return natural is
    variable n    : natural;
    variable logn : natural;
  begin
    n := 1;
    for i in 0 to 128 loop
      logn := i;
      exit when (n >= v);
      n := n * 2;
    end loop;
    return logn;
  end function log2;
  
  function int_select(s : in boolean; a : in integer; b : in integer) return integer is
  begin
    if s then
      return a;
    else
      return b;
    end if;
    return a;
  end function int_select;
  
  function toFix( d: real; qn : natural; qm : natural ) return signed is 
  begin 
    return to_signed( integer(d*(2.0**qm)), qn+qm );
  end function; 
  
end package body common;
