-- audioBooster.vhd --------------------------------------------------------------------------
--
-- Increases sample amplitude by performing fixed poing multiplications by a factor
-- (volume_i).
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity audioBooster is
  port (
    -- Control
    volume_i    : in  std_logic_vector(3 downto 0);
    -- Audio
    input_i     : in  int_sample(0 downto 0);
    output_o    : out int_sample(0 downto 0)
  );
end audioBooster;

architecture syn of audioBooster is

  constant max_clip   : std_logic_vector(31 downto 0) :=  "01111111111111111111111111111111";
  constant min_clip   : std_logic_vector(31 downto 0) :=  "10000000000000000000000000000000";
  
  signal input_aux    : signed(31 downto 0);
  signal multiply_aux : signed(37 downto 0);
  signal factor       : signed(5 downto 0);
  signal volume_aux   : std_logic_vector(5 downto 0);

begin

  input_aux <= signed(input_i(0));
  
  volume_aux <= "00" & volume_i;
  
  factor    <= signed(volume_aux) + 1;
               
  multiply_aux <= input_aux * factor;

  output_o(0) <= max_clip when multiply_aux(37) = '0' and multiply_aux(37 downto 31) /= "0000000" else
                 min_clip when multiply_aux(37) = '1' and multiply_aux(37 downto 31) /= "1111111" else
                 std_logic_vector(multiply_aux(31 downto 0));

end syn;
