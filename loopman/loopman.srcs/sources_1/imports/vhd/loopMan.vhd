-- loopMan.vhd -------------------------------------------------------------------------------
--
-- LoopMAN's top module. Contains all main components.
--
-- Designed by nueve de copas in San Rafael, between 2019 and 2020.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity loopMan is
  port (
    clk_i                : in  std_logic;
    rstn_i               : in  std_logic;
    -- 7-segment display
    disp_seg_o           : out std_logic_vector(7 downto 0);
    disp_an_o            : out std_logic_vector(7 downto 0);
    -- Leds
    led_o                : out std_logic_vector(15 downto 0);
    -- DDR2 interface signals
    ddr2_addr            : out   std_logic_vector(12 downto 0);
    ddr2_ba              : out   std_logic_vector(2 downto 0);
    ddr2_ras_n           : out   std_logic;
    ddr2_cas_n           : out   std_logic;
    ddr2_we_n            : out   std_logic;
    ddr2_ck_p            : out   std_logic_vector(0 downto 0);
    ddr2_ck_n            : out   std_logic_vector(0 downto 0);
    ddr2_cke             : out   std_logic_vector(0 downto 0);
    ddr2_cs_n            : out   std_logic_vector(0 downto 0);
    ddr2_odt             : out   std_logic_vector(0 downto 0);
    ddr2_dq              : inout std_logic_vector(15 downto 0);
    ddr2_dm              : out   std_logic_vector(1 downto 0);
    ddr2_dqs_p           : inout std_logic_vector(1 downto 0);
    ddr2_dqs_n           : inout std_logic_vector(1 downto 0);
    -- IIS
    mclk_dac_o           : out std_logic_vector((SYS_OUTPUTS/2)-1 downto 0);  -- master clock, 256fs    
    lrck_dac_o           : out std_logic_vector((SYS_OUTPUTS/2)-1 downto 0);  -- serial bit clocl, 64fs 
    sclk_dac_o           : out std_logic_vector((SYS_OUTPUTS/2)-1 downto 0);  -- left-right clock, fs   
    sdout_dac_o          : out std_logic_vector((SYS_OUTPUTS/2)-1 downto 0);  -- serial data to DACs 
    mclk_adc_o           : out std_logic_vector((SYS_INPUTS/2)-1 downto 0);   -- master clock, 256fs    
    lrck_adc_o           : out std_logic_vector((SYS_INPUTS/2)-1 downto 0);   -- serial bit clocl, 64fs 
    sclk_adc_o           : out std_logic_vector((SYS_INPUTS/2)-1 downto 0);   -- left-right clock, fs   
    sdin_adc_i           : in  std_logic_vector((SYS_INPUTS/2)-1 downto 0);   -- serial data from ADCs
    -- VGA
    vga_hs_o             : out std_logic;
    vga_vs_o             : out std_logic;
    vga_color_o          : out std_logic_vector(11 downto 0);
    -- PS2
    ps2_clk_i            : in  std_logic;
    ps2_data_i           : in  std_logic;
    -- Bluetooth         
    bt_rx_i              : in  std_logic;
    bt_tx_o              : out std_logic;
    bt_rst_n_o           : out std_logic;
    -- Pedalboard
    pedal_rec_encoder_i  : in std_logic_vector(2 downto 0);
    pedal_rec_gs_i       : in std_logic;
    pedal_play_encoder_i : in std_logic_vector(2 downto 0);
    pedal_play_gs_i      : in std_logic
  );
end loopMan;

---------------------------------------------------------------------

architecture syn of loopMan is
  
  -- 200 MHz Clock Generator
  component ClkGen
    port (
      clk_100MHz_i : in     std_logic;
      clk_200MHz_o : out    std_logic;
      reset        : in     std_logic;
      locked       : out    std_logic
    );
  end component;
  
  -- Audio input/output
  component audioIO is
    generic (
      INPUTS  : natural := 2;
      OUTPUTS : natural := 2;
      WIDTH   : natural := 24 -- sample width
    );
    port ( 
      -- System
      rst_i        : in  std_logic;   -- async reset
      clk_75_i     : in  std_logic;   -- clock
      -- Control
      input_rdy_o  : out std_logic;   -- sets to high for one cycle when new data is received
      output_req_o : out std_logic;   -- sets to high for one cycle when data to send is requested
      -- IO
      output_i     : in  ext_sample(OUTPUTS-1 downto 0);   -- sample to AudioCodec
      input_o      : out ext_sample(INPUTS-1 downto 0);    -- sample from AudioCodec
      -- IIS
      mclk_o       : out std_logic;   -- master clock, 256fs
      sclk_o       : out std_logic;   -- serial bit clocl, 64fs
      lrck_o       : out std_logic;   -- left-right clock, fs
      sdout_o      : out std_logic_vector((OUTPUTS/2)-1 downto 0); -- serial data to DAC
      sdin_i       : in  std_logic_vector((INPUTS/2)-1 downto 0)   -- serial data from ADC
    );
  end component;
  
  -- Audio input selector
  component trackInputMixer is
    generic ( 
      INPUTS : natural := 2
    );
    port (
      -- System
      rst_i       : in  std_logic;
      clk_i       : in  std_logic;
      -- Audio outputs 
      mix_o       : out int_sample(7 downto 0);
      mix_rdy_o   : out std_logic_vector(7 downto 0);
      -- Audio inputs (audio to mix)
      input_i     : in  ext_sample(INPUTS-1 downto 0);
      input_rdy_i : in std_logic; -- new samples from audio codec
      -- Tracks (input selection)
      in_sel_i    : in  std_logic_vector((INPUTS*8)-1 downto 0)
    );
  end component;
  
  component outputMixer is
    generic ( 
      OUTPUTS : natural := 2
    );
    port (
      -- System
      rst_i           : in  std_logic;
      clk_i           : in  std_logic;
      -- Audio outputs (mixed audio)
      output_o        : out ext_sample(OUTPUTS-1 downto 0);
      output_rdy_o    : out std_logic_vector(OUTPUTS-1 downto 0);
      -- Audio inputs
      input_i         : in  int_sample(7 downto 0);
      input_rdy_i     : in  std_logic_vector(7 downto 0);
      -- Compression
      t_inc_comp_i    : in  std_logic_vector(OUTPUTS-1 downto 0);
      t_dec_comp_i    : in  std_logic_vector(OUTPUTS-1 downto 0);
      t_set_comp_i    : in  std_logic_vector(OUTPUTS-1 downto 0);
      value_comp_i    : in  std_logic_vector(3 downto 0);
      st_comp_o       : out std_logic_vector((OUTPUTS*4)-1 downto 0);
      t_ch_comp_o     : out std_logic_vector(OUTPUTS-1 downto 0);
      -- Volume
      t_inc_vol_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
      t_dec_vol_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
      t_set_vol_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
      value_vol_i     : in  std_logic_vector(3 downto 0);
      st_vol_o        : out std_logic_vector((OUTPUTS*4)-1 downto 0);
      t_ch_vol_o      : out std_logic_vector(OUTPUTS-1 downto 0);
      st_outlevel_o   : out std_logic_vector((OUTPUTS*4)-1 downto 0);
      st_clipping_o   : out std_logic_vector(OUTPUTS-1 downto 0);
      t_ch_outlevel_o : out std_logic_vector(OUTPUTS-1 downto 0);
      -- Tracks output selection
      out_sel0_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      out_sel1_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      out_sel2_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      out_sel3_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      out_sel4_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      out_sel5_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      out_sel6_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      out_sel7_i      : in  std_logic_vector(OUTPUTS-1 downto 0)
    );
  end component;
  
  -- Tracks
  component trackController is
    generic (
      OUTPUTS : natural := 2;
      INPUTS  : natural := 2
    );
    port (
      -- System
      clk_200MHz_i    : in std_logic;
      rst_i           : in std_logic;
      clk_o           : out std_logic;
      rst_o           : out std_logic;
      -- Track control
      t_rec_i         : in std_logic_vector(7 downto 0);
      t_play_i        : in std_logic_vector(7 downto 0);
      t_mute_i        : in std_logic_vector(7 downto 0);
      t_solo_i        : in std_logic_vector(7 downto 0);
      t_clear_i       : in std_logic_vector(7 downto 0);
      t_sync_i        : in std_logic_vector(7 downto 0);
      t_auto_rec_tg_i : in std_logic_vector(7 downto 0);
      t_inc_looplen_i : in std_logic_vector(7 downto 0);
      t_dec_looplen_i : in std_logic_vector(7 downto 0);
      t_set_looplen_i : in std_logic_vector(7 downto 0);
      value_looplen_i : in std_logic_vector(4 downto 0);
      t_inc_vol_i     : in std_logic_vector(7 downto 0);
      t_dec_vol_i     : in std_logic_vector(7 downto 0);
      t_set_vol_i     : in std_logic_vector(7 downto 0);
      value_vol_i     : in std_logic_vector(3 downto 0);
      t_inc_odub_i    : in std_logic_vector(7 downto 0);
      t_dec_odub_i    : in std_logic_vector(7 downto 0);
      t_set_odub_i    : in std_logic_vector(7 downto 0);
      value_odub_i    : in std_logic_vector(3 downto 0);
      t_input_sel_i   : in std_logic_vector((8*INPUTS)-1 downto 0); 
      t_output_sel_i  : in std_logic_vector((8*OUTPUTS)-1 downto 0);
      -- Tempo / sync / coordination
      t_tempo_i       : in std_logic;
      t_tempo_sync_o  : out std_logic;
      -- Track status
      st_rec_o        : out std_logic_vector(7 downto 0);
      st_play_o       : out std_logic_vector(7 downto 0);
      st_mute_o       : out std_logic_vector(7 downto 0);
      st_solo_o       : out std_logic_vector(7 downto 0);
      st_sync_o       : out std_logic_vector(7 downto 0);
      st_auto_rec_o   : out std_logic_vector(7 downto 0);
      t_ch_status_o   : out std_logic_vector(7 downto 0);
      st_beat_o       : out std_logic_vector((8*5)-1 downto 0);
      t_ch_beat_o     : out std_logic_vector(7 downto 0);
      st_looplen_o    : out std_logic_vector((8*5)-1 downto 0);
      t_ch_looplen_o  : out std_logic_vector(7 downto 0);
      st_vol_o        : out std_logic_vector((8*4)-1 downto 0);
      t_ch_vol_o      : out std_logic_vector(7 downto 0);
      st_odub_o       : out std_logic_vector((8*4)-1 downto 0);
      t_ch_odub_o     : out std_logic_vector(7 downto 0);
      st_outlevel_o   : out std_logic_vector((8*4)-1 downto 0); -- waveform to display, to show output level
      t_ch_outlevel_o : out std_logic_vector(7 downto 0);
      t_ch_in_sel_o   : out std_logic_vector(7 downto 0);
      t_ch_out_sel_o  : out std_logic_vector(7 downto 0);
      -- Audio IO
      audio_i         : in  int_sample(7 downto 0);       -- audio in from selected external inputs or tracks
      audio_o         : out int_sample(7 downto 0);       -- audio out to selected outputs
      in_sel_o        : out std_logic_vector((8*INPUTS)-1 downto 0); -- selected inputs
      out_sel_o       : out std_logic_vector((8*OUTPUTS)-1 downto 0); -- selected outputs
      input_rdy_i     : in  std_logic_vector(7 downto 0);
      output_rdy_o    : out std_logic_vector(7 downto 0);
      -- DDR2 interface
      ddr2_addr       : out   std_logic_vector(12 downto 0);
      ddr2_ba         : out   std_logic_vector(2 downto 0);
      ddr2_ras_n      : out   std_logic;
      ddr2_cas_n      : out   std_logic;
      ddr2_we_n       : out   std_logic;
      ddr2_ck_p       : out   std_logic_vector(0 downto 0);
      ddr2_ck_n       : out   std_logic_vector(0 downto 0);
      ddr2_cke        : out   std_logic_vector(0 downto 0);
      ddr2_cs_n       : out   std_logic_vector(0 downto 0);
      ddr2_odt        : out   std_logic_vector(0 downto 0);
      ddr2_dq         : inout std_logic_vector(15 downto 0);
      ddr2_dm         : out   std_logic_vector(1 downto 0);
      ddr2_dqs_p      : inout std_logic_vector(1 downto 0);
      ddr2_dqs_n      : inout std_logic_vector(1 downto 0)
    );
  end component;
  
  component tempoGenerator is
    port (
      -- System
      clk_i              : in std_logic;
      rst_i              : in std_logic;
      -- Tempo setup
      set_value_i        : in std_logic_vector(15 downto 0);
      t_set_tempo_i      : in std_logic;
      t_hit_tempo_i      : in std_logic;
      -- Track sync
      t_sync_tempo_i     : in std_logic;
      -- Tempo output
      tempo_value_o      : out std_logic_vector(15 downto 0);
      t_ch_tempo_value_o : out std_logic;
      t_tempo_o          : out std_logic
    );
  end component;
  
  component fxController is
    generic (
      FX_NO : natural := 4
    );
    port (
      -- System
      rst_i         : in  std_logic;
      clk_i         : in  std_logic;
      -- Audio output
      output_o      : out int_sample(7 downto 0);
      output_rdy_o  : out std_logic_vector(7 downto 0);
      -- Audio input
      input_i       : in  int_sample(7 downto 0);
      input_rdy_i   : in  std_logic_vector(7 downto 0);
      -- Control
      t_toggle_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
      t_enqueue_i   : in  std_logic_vector((8*FX_NO)-1 downto 0);
      t_inc_fx_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
      t_dec_fx_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
      t_set_fx_i    : in  std_logic_vector((8*FX_NO)-1 downto 0);
      value_fx_i    : in  std_logic_vector((8*4)-1 downto 0);
      t_clear_i     : in  std_logic_vector(7 downto 0);
      -- Status
      st_on_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
      st_queued_o   : out std_logic_vector((8*FX_NO)-1 downto 0);
      st_fx_o       : out std_logic_vector(8*(FX_NO*4)-1 downto 0);
      t_ch_fx_o     : out std_logic_vector((8*FX_NO)-1 downto 0)
    );
  end component;
  
  component userDisplay is
    generic (
      OUTPUTS : natural := 2;
      INPUTS  : natural := 2;
      FX_NO   : natural := 4
    );
    port (
      -- System
      clk_i                  : in  std_logic;
      rst_i                  : in  std_logic;
      -- VGA
      hs_o                   : out std_logic;
      vs_o                   : out std_logic;
      color_o                : out std_logic_vector(11 downto 0);
      -- Bluetooth
      bt_tx_o                : out std_logic;
      -- System status
      -- Tempo
      tempo_value_i          : in  std_logic_vector(15 downto 0);
      t_ch_tempo_value_i     : in  std_logic;
      t_tempo_i              : in  std_logic;
      -- Master
      st_master_vol_i        : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
      t_ch_master_vol_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      st_master_comp_i       : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
      t_ch_master_comp_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
      st_master_outlevel_i   : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
      st_master_clipping_i   : in  std_logic_vector(OUTPUTS-1 downto 0);
      t_ch_master_outlevel_i : in  std_logic_vector(OUTPUTS-1 downto 0);
      -- Track status
      st_rec_i               : in  std_logic_vector(7 downto 0);
      st_play_i              : in  std_logic_vector(7 downto 0);
      st_mute_i              : in  std_logic_vector(7 downto 0);
      st_solo_i              : in  std_logic_vector(7 downto 0);
      st_sync_i              : in  std_logic_vector(7 downto 0);
      st_auto_rec_i          : in  std_logic_vector(7 downto 0);
      t_ch_status_i          : in  std_logic_vector(7 downto 0);
      st_beat_i              : in  std_logic_vector((8*5)-1 downto 0);
      t_ch_beat_i            : in  std_logic_vector(7 downto 0);
      st_looplen_i           : in  std_logic_vector((8*5)-1 downto 0);
      t_ch_looplen_i         : in  std_logic_vector(7 downto 0);
      st_vol_i               : in  std_logic_vector((8*4)-1 downto 0);
      t_ch_vol_i             : in  std_logic_vector(7 downto 0);
      st_odub_i              : in  std_logic_vector((8*4)-1 downto 0);
      t_ch_odub_i            : in  std_logic_vector(7 downto 0);
      st_outlevel_i          : in  std_logic_vector((8*4)-1 downto 0);
      t_ch_outlevel_i        : in  std_logic_vector(7 downto 0);
      in_sel_i               : in  std_logic_vector((8*INPUTS)-1 downto 0);
      t_ch_in_sel_i          : in  std_logic_vector(7 downto 0);
      out_sel_i              : in  std_logic_vector((8*OUTPUTS)-1 downto 0);
      t_ch_out_sel_i         : in  std_logic_vector(7 downto 0);
      track_select_i         : in  std_logic_vector(7 downto 0);
      -- FX status
      st_on_i                : in  std_logic_vector((8*FX_NO)-1 downto 0);  
      st_queued_i            : in  std_logic_vector((8*FX_NO)-1 downto 0);  
      st_fx_i                : in  std_logic_vector(8*(FX_NO*4)-1 downto 0);
      t_ch_fx_i              : in  std_logic_vector((8*FX_NO)-1 downto 0)   
    );
  end component;
  
  component userControl is
    generic (
      OUTPUTS : natural := 2;
      INPUTS  : natural := 2;
      FX_NO   : natural := 4
    );
    port (
      -- System
      rst_i                : in  std_logic;
      clk_i                : in  std_logic;
      -- PS2
      ps2_clk_i            : in  std_logic;
      ps2_data_i           : in  std_logic;
      -- Bluetooth          
      bt_rx_i              : in  std_logic;
      -- HW switches
      pedal_rec_encoder_i  : in  std_logic_vector(2 downto 0);
      pedal_rec_gs_i       : in  std_logic;
      pedal_play_encoder_i : in  std_logic_vector(2 downto 0);
      pedal_play_gs_i      : in  std_logic;
      -- Control
      t_reset_req_o        : out std_logic;
      -- Master
      t_inc_master_comp_o  : out std_logic_vector(OUTPUTS-1 downto 0);
      t_dec_master_comp_o  : out std_logic_vector(OUTPUTS-1 downto 0);
      t_set_master_comp_o  : out std_logic_vector(OUTPUTS-1 downto 0);
      value_master_comp_o  : out std_logic_vector(3 downto 0);
      t_inc_master_vol_o   : out std_logic_vector(OUTPUTS-1 downto 0);
      t_dec_master_vol_o   : out std_logic_vector(OUTPUTS-1 downto 0);
      t_set_master_vol_o   : out std_logic_vector(OUTPUTS-1 downto 0);
      value_master_vol_o   : out std_logic_vector(3 downto 0);
      -- Tracks
      t_rec_o              : out std_logic_vector(7 downto 0);           
      t_play_o             : out std_logic_vector(7 downto 0);           
      t_mute_o             : out std_logic_vector(7 downto 0);           
      t_solo_o             : out std_logic_vector(7 downto 0);           
      t_clear_o            : out std_logic_vector(7 downto 0);           
      t_sync_o             : out std_logic_vector(7 downto 0);
      t_auto_rec_tg_o      : out std_logic_vector(7 downto 0);         
      t_inc_looplen_o      : out std_logic_vector(7 downto 0);           
      t_dec_looplen_o      : out std_logic_vector(7 downto 0);           
      t_set_looplen_o      : out std_logic_vector(7 downto 0);
      value_looplen_o      : out std_logic_vector(4 downto 0);           
      t_inc_vol_o          : out std_logic_vector(7 downto 0);           
      t_dec_vol_o          : out std_logic_vector(7 downto 0);           
      t_set_vol_o          : out std_logic_vector(7 downto 0);
      value_vol_o          : out std_logic_vector(3 downto 0);           
      t_inc_odub_o         : out std_logic_vector(7 downto 0);           
      t_dec_odub_o         : out std_logic_vector(7 downto 0);                          
      t_set_odub_o         : out std_logic_vector(7 downto 0);
      value_odub_o         : out std_logic_vector(3 downto 0);                          
      t_input_sel_o        : out std_logic_vector((8*INPUTS)-1 downto 0); 
      t_output_sel_o       : out std_logic_vector((8*OUTPUTS)-1 downto 0);
      track_select_o       : out std_logic_vector(7 downto 0); -- for VGA only
      -- Tempo                                                           
      t_hit_tempo_o        : out std_logic;       
      t_set_tempo_o        : out std_logic;       
      tempo_value_o        : out std_logic_vector(15 downto 0);
      -- FX
      t_fx_toggle_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_enqueue_o       : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_inc_fx_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_dec_fx_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
      t_fx_set_fx_o        : out std_logic_vector((8*FX_NO)-1 downto 0);
      value_fx_o           : out std_logic_vector((8*4)-1 downto 0);    
      t_fx_clear_o         : out std_logic_vector(7 downto 0)          
    );
  end component;
  
  -- 7 segment display
  component sSegDisplay is
    port (
      ck     : in  std_logic;                     -- 100Mhz system clock
      number : in  std_logic_vector(63 downto 0); -- eght digit hex data to be displayed, active-low
      seg    : out std_logic_vector(7 downto 0);  -- display cathodes
      an     : out std_logic_vector(7 downto 0)   -- display anodes (active-low, due to transistor complementing)
    );
  end component;
  
  -- System clock and reset
  signal rst               : std_logic; -- Inverted input reset signal (not synchronized)
  signal reset             : std_logic; -- Reset signal conditioned by the PLL lock
  signal reset_sync        : std_logic;
  signal reset_sync_n      : std_logic;
  signal locked            : std_logic;
  signal clk_200MHz        : std_logic; -- Generated clk
  signal ui_clk            : std_logic;
  signal ui_clk_sync_rst   : std_logic;
  signal reset_req         : std_logic;
  
  -- IIS
  signal mclk              : std_logic;
  signal sclk              : std_logic;
  signal lrck              : std_logic;
  signal sdout_dac         : std_logic_vector((SYS_OUTPUTS/2)-1 downto 0);
  signal sdin_adc          : std_logic_vector((SYS_INPUTS/2)-1 downto 0);
  
  -- Audio input/output
  signal audio_input_rdy   : std_logic;
  signal audio_input       : ext_sample(SYS_INPUTS-1 downto 0);
  
  -- Audio mixers
  signal in_sel            : std_logic_vector((8*SYS_INPUTS)-1 downto 0); 
  signal out_sel           : std_logic_vector((8*SYS_OUTPUTS)-1 downto 0);
  signal input_mix         : int_sample(7 downto 0);
  signal input_mix_rdy     : std_logic_vector(7 downto 0);
  signal output_mix        : ext_sample(SYS_OUTPUTS-1 downto 0);
  signal output_mix_rdy    : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  
  -- Track audio
  signal track_output      : int_sample(7 downto 0);
  signal track_output_rdy  : std_logic_vector(7 downto 0);
  
  -- Fx audio
  signal fx_output         : int_sample(7 downto 0);
  signal fx_output_rdy     : std_logic_vector(7 downto 0);

  -- CONTROL TICKS
  -- MASTER
  signal t_inc_master_comp : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal t_dec_master_comp : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal t_set_master_comp : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal value_master_comp : std_logic_vector(3 downto 0);
  signal t_inc_master_vol  : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal t_dec_master_vol  : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal t_set_master_vol  : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal value_master_vol  : std_logic_vector(3 downto 0);
  -- TRACKS
  signal t_rec             : std_logic_vector(7 downto 0);            
  signal t_play            : std_logic_vector(7 downto 0);            
  signal t_mute            : std_logic_vector(7 downto 0);            
  signal t_solo            : std_logic_vector(7 downto 0);            
  signal t_clear           : std_logic_vector(7 downto 0);            
  signal t_sync            : std_logic_vector(7 downto 0);
  signal t_auto_rec_tg     : std_logic_vector(7 downto 0);           
  signal t_inc_looplen     : std_logic_vector(7 downto 0);            
  signal t_dec_looplen     : std_logic_vector(7 downto 0);            
  signal t_set_looplen     : std_logic_vector(7 downto 0);            
  signal value_looplen     : std_logic_vector(4 downto 0);            
  signal t_inc_vol         : std_logic_vector(7 downto 0);            
  signal t_dec_vol         : std_logic_vector(7 downto 0);            
  signal t_set_vol         : std_logic_vector(7 downto 0);            
  signal value_vol         : std_logic_vector(3 downto 0);            
  signal t_inc_odub        : std_logic_vector(7 downto 0);            
  signal t_dec_odub        : std_logic_vector(7 downto 0);            
  signal t_set_odub        : std_logic_vector(7 downto 0);            
  signal value_odub        : std_logic_vector(3 downto 0);            
  signal t_input_sel       : std_logic_vector((8*SYS_INPUTS)-1 downto 0); 
  signal t_output_sel      : std_logic_vector((8*SYS_OUTPUTS)-1 downto 0);
  -- FX
  signal t_fx_toggle       : std_logic_vector((8*SYS_FX_NO)-1 downto 0);
  signal t_fx_enqueue      : std_logic_vector((8*SYS_FX_NO)-1 downto 0);
  signal t_fx_inc_fx       : std_logic_vector((8*SYS_FX_NO)-1 downto 0);
  signal t_fx_dec_fx       : std_logic_vector((8*SYS_FX_NO)-1 downto 0);
  signal t_fx_set_fx       : std_logic_vector((8*SYS_FX_NO)-1 downto 0);
  signal value_fx          : std_logic_vector((8*4)-1 downto 0);
  signal t_fx_clear        : std_logic_vector(7 downto 0);
  
  -- Tempo
  signal t_inc_tempo       : std_logic;
  signal t_dec_tempo       : std_logic;
  signal set_tempo_value   : std_logic_vector(15 downto 0);
  signal tempo_value       : std_logic_vector(15 downto 0);
  signal t_ch_tempo_value  : std_logic;
  signal t_set_tempo       : std_logic;
  signal t_tempo           : std_logic;
  signal t_tempo_sync      : std_logic;
  signal t_hit_tempo       : std_logic;

  -- STATUS FLAGS
  -- MASTER
  signal st_master_comp       : std_logic_vector((SYS_OUTPUTS*4)-1 downto 0);
  signal t_ch_master_comp     : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal st_master_vol        : std_logic_vector((SYS_OUTPUTS*4)-1 downto 0);
  signal t_ch_master_vol      : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal st_master_outlevel   : std_logic_vector((SYS_OUTPUTS*4)-1 downto 0);
  signal st_master_clipping   : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  signal t_ch_master_outlevel : std_logic_vector(SYS_OUTPUTS-1 downto 0);
  -- TRACKS
  signal st_rec              : std_logic_vector(7 downto 0);
  signal st_play             : std_logic_vector(7 downto 0);
  signal st_mute             : std_logic_vector(7 downto 0);
  signal st_solo             : std_logic_vector(7 downto 0);
  signal st_sync             : std_logic_vector(7 downto 0);
  signal st_auto_rec         : std_logic_vector(7 downto 0);
  signal t_ch_status         : std_logic_vector(7 downto 0);
  signal st_beat             : std_logic_vector((8*5)-1 downto 0);
  signal t_ch_beat           : std_logic_vector(7 downto 0);
  signal st_looplen          : std_logic_vector((8*5)-1 downto 0);
  signal t_ch_looplen        : std_logic_vector(7 downto 0);
  signal st_vol              : std_logic_vector((8*4)-1 downto 0);
  signal t_ch_vol            : std_logic_vector(7 downto 0);
  signal st_odub             : std_logic_vector((8*4)-1 downto 0);
  signal t_ch_odub           : std_logic_vector(7 downto 0);
  signal st_outlevel         : std_logic_vector((8*4)-1 downto 0);
  signal t_ch_outlevel       : std_logic_vector(7 downto 0);
  signal t_ch_in_sel         : std_logic_vector(7 downto 0);
  signal t_ch_out_sel        : std_logic_vector(7 downto 0);
  signal track_select        : std_logic_vector(7 downto 0); -- for VGA only
  -- FX
  signal st_fx_on            : std_logic_vector((8*SYS_FX_NO)-1 downto 0);  
  signal st_fx_queued        : std_logic_vector((8*SYS_FX_NO)-1 downto 0);  
  signal st_val_fx           : std_logic_vector(8*(SYS_FX_NO*4)-1 downto 0);
  signal t_ch_fx             : std_logic_vector((8*SYS_FX_NO)-1 downto 0);
  
  -- 7 seg display
  signal loopman_7seg      : std_logic_vector(63 downto 0);

begin

  --- EXTERNAL CONNECTIONS -----------------------------------------
  
  led_o(7 downto 0) <= st_play(7 downto 0);
  led_o(15 downto 8) <= st_rec(7 downto 0);
  
  -- IIS ---------------------
  mclk_dac_o  <= (others=>mclk);
  mclk_adc_o  <= (others=>mclk);
  lrck_dac_o  <= (others=>lrck);
  lrck_adc_o  <= (others=>lrck);
  sclk_dac_o  <= (others=>sclk);
  sclk_adc_o  <= (others=>sclk);
  sdout_dac_o <= sdout_dac;
  sdin_adc    <= sdin_adc_i;
  ----------------------------
  
  -- BT ----------------------
  bt_rst_n_o <= '1';
  ----------------------------
  
  ------------------------------------------------------------------
    
  rst   <= (not rstn_i) or reset_req;
  reset <= rst or (not locked);
  reset_sync_n <= not reset_sync;
  
  reset_synchronizer : synchronizer
  generic map (
    STAGES  => 2, 
    INIT    => '1' 
  )
  port map (
    rst   => reset,
    clk   => clk_200MHz,
    x     => '0',
    xSync => reset_sync
  );
  
  Inst_ClkGen: ClkGen
  port map (
    clk_100MHz_i => clk_i,
    clk_200MHz_o => clk_200MHz,
    reset        => rst,
    locked       => locked
  );
            
  audioIO_component : audioIO
  generic map (
    WIDTH   => SYS_AUDIO_WIDTH,
    INPUTS  => SYS_INPUTS,
    OUTPUTS => SYS_OUTPUTS
  )
  port map ( 
    rst_i        => ui_clk_sync_rst,
    clk_75_i     => ui_clk,
    -- Control
    input_rdy_o  => audio_input_rdy,
    output_req_o => open,
    -- IO 
    output_i     => output_mix,
    input_o      => audio_input,
    -- IIS
    mclk_o       => mclk,
    sclk_o       => sclk,
    lrck_o       => lrck,
    sdout_o      => sdout_dac,
    sdin_i       => sdin_adc
  );
  
  trackInputMixer_component : trackInputMixer
  generic map ( 
    INPUTS => SYS_INPUTS
  )
  port map (
    -- System
    rst_i       => ui_clk_sync_rst,
    clk_i       => ui_clk,
    -- Audio outputs 
    mix_o       => input_mix,
    mix_rdy_o   => input_mix_rdy,
    -- Audio inputs
    input_i     => audio_input,
    input_rdy_i => audio_input_rdy,
    -- Tracks input selection
    in_sel_i    => in_sel
  );
   
  outputMixer_component : outputMixer
  generic map ( 
    OUTPUTS => SYS_OUTPUTS
  )
  port map (
    -- System
    rst_i           => ui_clk_sync_rst,
    clk_i           => ui_clk,
    -- Audio outputs
    output_o        => output_mix,
    output_rdy_o    => output_mix_rdy,
    -- Audio inputs
    input_i         => fx_output,
    input_rdy_i     => fx_output_rdy,
    -- Compression  
    t_inc_comp_i    => t_inc_master_comp,  
    t_dec_comp_i    => t_dec_master_comp,  
    t_set_comp_i    => t_set_master_comp,  
    value_comp_i    => value_master_comp,  
    st_comp_o       => st_master_comp,  
    t_ch_comp_o     => t_ch_master_comp,  
    -- Volume        
    t_inc_vol_i     => t_inc_master_vol,  
    t_dec_vol_i     => t_dec_master_vol,  
    t_set_vol_i     => t_set_master_vol,  
    value_vol_i     => value_master_vol,  
    st_vol_o        => st_master_vol,  
    t_ch_vol_o      => t_ch_master_vol,  
    st_outlevel_o   => st_master_outlevel,  
    st_clipping_o   => st_master_clipping,  
    t_ch_outlevel_o => t_ch_master_outlevel, 
    -- Tracks output selection
    out_sel0_i      => out_sel((1*SYS_OUTPUTS)-1 downto 0*SYS_OUTPUTS),
    out_sel1_i      => out_sel((2*SYS_OUTPUTS)-1 downto 1*SYS_OUTPUTS),
    out_sel2_i      => out_sel((3*SYS_OUTPUTS)-1 downto 2*SYS_OUTPUTS),
    out_sel3_i      => out_sel((4*SYS_OUTPUTS)-1 downto 3*SYS_OUTPUTS),
    out_sel4_i      => out_sel((5*SYS_OUTPUTS)-1 downto 4*SYS_OUTPUTS),
    out_sel5_i      => out_sel((6*SYS_OUTPUTS)-1 downto 5*SYS_OUTPUTS),
    out_sel6_i      => out_sel((7*SYS_OUTPUTS)-1 downto 6*SYS_OUTPUTS),
    out_sel7_i      => out_sel((8*SYS_OUTPUTS)-1 downto 7*SYS_OUTPUTS)
  );
   
  trackController_component : trackController
  generic map (
    OUTPUTS => SYS_OUTPUTS,
    INPUTS  => SYS_INPUTS
  )
  port map (
    -- System
    clk_200MHz_i    => clk_200MHz,
    rst_i           => reset_sync,
    clk_o           => ui_clk,
    rst_o           => ui_clk_sync_rst,
    -- Track control
    t_rec_i         => t_rec,                
    t_play_i        => t_play,               
    t_mute_i        => t_mute,               
    t_solo_i        => t_solo,               
    t_clear_i       => t_clear,              
    t_sync_i        => t_sync,    
    t_auto_rec_tg_i => t_auto_rec_tg,
    t_inc_looplen_i => t_inc_looplen,
    t_dec_looplen_i => t_dec_looplen,
    t_set_looplen_i => t_set_looplen,
    value_looplen_i => value_looplen,
    t_inc_vol_i     => t_inc_vol,
    t_dec_vol_i     => t_dec_vol,
    t_set_vol_i     => t_set_vol,
    value_vol_i     => value_vol,
    t_inc_odub_i    => t_inc_odub,
    t_dec_odub_i    => t_dec_odub,
    t_set_odub_i    => t_set_odub,
    value_odub_i    => value_odub,        
    t_input_sel_i   => t_input_sel,          
    t_output_sel_i  => t_output_sel,         
    -- Tempo
    t_tempo_i       => t_tempo,
    t_tempo_sync_o  => t_tempo_sync,
    -- Track status
    st_rec_o        => st_rec,
    st_play_o       => st_play,
    st_mute_o       => st_mute,
    st_solo_o       => st_solo,
    st_sync_o       => st_sync,
    st_auto_rec_o   => st_auto_rec,
    t_ch_status_o   => t_ch_status,
    st_beat_o       => st_beat,
    t_ch_beat_o     => t_ch_beat,
    st_looplen_o    => st_looplen,
    t_ch_looplen_o  => t_ch_looplen,
    st_vol_o        => st_vol,
    t_ch_vol_o      => t_ch_vol,
    st_odub_o       => st_odub,
    t_ch_odub_o     => t_ch_odub,
    st_outlevel_o   => st_outlevel,
    t_ch_outlevel_o => t_ch_outlevel,
    t_ch_in_sel_o   => t_ch_in_sel,
    t_ch_out_sel_o  => t_ch_out_sel,
    -- Audio IO
    audio_i         => input_mix,
    audio_o         => track_output,
    in_sel_o        => in_sel,
    out_sel_o       => out_sel,
    input_rdy_i     => input_mix_rdy,
    output_rdy_o    => track_output_rdy,
    -- DDR2 interface
    ddr2_addr       => ddr2_addr, 
    ddr2_ba         => ddr2_ba,   
    ddr2_ras_n      => ddr2_ras_n,
    ddr2_cas_n      => ddr2_cas_n,
    ddr2_we_n       => ddr2_we_n, 
    ddr2_ck_p       => ddr2_ck_p,
    ddr2_ck_n       => ddr2_ck_n, 
    ddr2_cke        => ddr2_cke,  
    ddr2_cs_n       => ddr2_cs_n, 
    ddr2_odt        => ddr2_odt,  
    ddr2_dq         => ddr2_dq,  
    ddr2_dm         => ddr2_dm,  
    ddr2_dqs_p      => ddr2_dqs_p,
    ddr2_dqs_n      => ddr2_dqs_n
  );

  tempoGenerator_component : tempoGenerator
  port map (
    -- System
    rst_i              => ui_clk_sync_rst,
    clk_i              => ui_clk,
    -- Tempo setup      
    set_value_i        => set_tempo_value, 
    t_set_tempo_i      => t_set_tempo, 
    t_hit_tempo_i      => t_hit_tempo, 
    -- Track sync       
    t_sync_tempo_i     => t_tempo_sync, 
    -- Tempo output     
    tempo_value_o      => tempo_value, 
    t_ch_tempo_value_o => t_ch_tempo_value,
    t_tempo_o          => t_tempo
  );
  
  fxController_component : fxController
  generic map (
    FX_NO => SYS_FX_NO
  )
  port map (
    -- System
    rst_i         => ui_clk_sync_rst,
    clk_i         => ui_clk,
    -- Audio output
    output_o      => fx_output,
    output_rdy_o  => fx_output_rdy,
    -- Audio input
    input_i       => track_output,
    input_rdy_i   => track_output_rdy,
    -- Control
    t_toggle_i    => t_fx_toggle,
    t_enqueue_i   => t_fx_enqueue,
    t_inc_fx_i    => t_fx_inc_fx,
    t_dec_fx_i    => t_fx_dec_fx,
    t_set_fx_i    => t_fx_set_fx,
    value_fx_i    => value_fx,
    t_clear_i     => t_fx_clear,
    -- Status 
    st_on_o       => st_fx_on,
    st_queued_o   => st_fx_queued,
    st_fx_o       => st_val_fx,
    t_ch_fx_o     => t_ch_fx  
  );
  
  userDisplay_component : userDisplay
  generic map (
    OUTPUTS => SYS_OUTPUTS,
    INPUTS  => SYS_INPUTS,
    FX_NO   => SYS_FX_NO
  )
  port map (
    -- System
    clk_i                  => ui_clk,
    rst_i                  => ui_clk_sync_rst,
    -- VGA  
    hs_o                   => vga_hs_o,   
    vs_o                   => vga_vs_o,   
    color_o                => vga_color_o,
    -- Bluetooth 
    bt_tx_o                => bt_tx_o,
    -- System status  
    -- Tempo
    tempo_value_i          => tempo_value,
    t_ch_tempo_value_i     => t_ch_tempo_value,
    t_tempo_i              => t_tempo,
    -- Master
    st_master_vol_i        => st_master_vol,
    t_ch_master_vol_i      => t_ch_master_vol,
    st_master_comp_i       => st_master_comp,
    t_ch_master_comp_i     => t_ch_master_comp,
    st_master_outlevel_i   => st_master_outlevel,
    st_master_clipping_i   => st_master_clipping,
    t_ch_master_outlevel_i => t_ch_master_outlevel,
    -- Track status  
    st_rec_i               => st_rec,
    st_play_i              => st_play,
    st_mute_i              => st_mute,
    st_solo_i              => st_solo,
    st_sync_i              => st_sync,
    st_auto_rec_i          => st_auto_rec,
    t_ch_status_i          => t_ch_status,
    st_beat_i              => st_beat,
    t_ch_beat_i            => t_ch_beat,
    st_looplen_i           => st_looplen,
    t_ch_looplen_i         => t_ch_looplen,
    st_vol_i               => st_vol,
    t_ch_vol_i             => t_ch_vol,
    st_odub_i              => st_odub,
    t_ch_odub_i            => t_ch_odub,
    st_outlevel_i          => st_outlevel,
    t_ch_outlevel_i        => t_ch_outlevel,
    in_sel_i               => in_sel,
    t_ch_in_sel_i          => t_ch_in_sel,
    out_sel_i              => out_sel,
    t_ch_out_sel_i         => t_ch_out_sel,
    track_select_i         => track_select,
    -- FX status
    st_on_i                => st_fx_on,  
    st_queued_i            => st_fx_queued,
    st_fx_i                => st_val_fx,    
    t_ch_fx_i              => t_ch_fx  
  );

  userControl_component : userControl
  generic map (
    OUTPUTS => SYS_OUTPUTS,
    INPUTS  => SYS_INPUTS,
    FX_NO   => SYS_FX_NO
  )
  port map (
   -- System
   rst_i                => ui_clk_sync_rst,
   clk_i                => ui_clk,
   -- PS2
   ps2_clk_i            => ps2_clk_i,
   ps2_data_i           => ps2_data_i,
   -- Bluetooth
   bt_rx_i              => bt_rx_i,
   -- HW switches
   pedal_rec_encoder_i  => pedal_rec_encoder_i,
   pedal_rec_gs_i       => pedal_rec_gs_i,
   pedal_play_encoder_i => pedal_play_encoder_i,
   pedal_play_gs_i      => pedal_play_gs_i,
   -- Control
   t_reset_req_o        => reset_req,
   -- Master
   t_inc_master_comp_o  => t_inc_master_comp,
   t_dec_master_comp_o  => t_dec_master_comp,
   t_set_master_comp_o  => t_set_master_comp,
   value_master_comp_o  => value_master_comp,
   t_inc_master_vol_o   => t_inc_master_vol,
   t_dec_master_vol_o   => t_dec_master_vol,
   t_set_master_vol_o   => t_set_master_vol,
   value_master_vol_o   => value_master_vol,
   -- Tracks
   t_rec_o              => t_rec,
   t_play_o             => t_play,
   t_mute_o             => t_mute,
   t_solo_o             => t_solo,
   t_clear_o            => t_clear,
   t_sync_o             => t_sync,
   t_auto_rec_tg_o      => t_auto_rec_tg,
   t_inc_looplen_o      => t_inc_looplen,
   t_dec_looplen_o      => t_dec_looplen,
   t_set_looplen_o      => t_set_looplen,
   value_looplen_o      => value_looplen,
   t_inc_vol_o          => t_inc_vol, 
   t_dec_vol_o          => t_dec_vol,                
   t_set_vol_o          => t_set_vol,
   value_vol_o          => value_vol,
   t_inc_odub_o         => t_inc_odub,
   t_dec_odub_o         => t_dec_odub,
   t_set_odub_o         => t_set_odub,
   value_odub_o         => value_odub,
   t_input_sel_o        => t_input_sel, 
   t_output_sel_o       => t_output_sel,
   track_select_o       => track_select,
   -- Tempo             
   t_hit_tempo_o        => t_hit_tempo,
   t_set_tempo_o        => t_set_tempo,
   tempo_value_o        => set_tempo_value,
   -- FX
   t_fx_toggle_o        => t_fx_toggle,
   t_fx_enqueue_o       => t_fx_enqueue,
   t_fx_inc_fx_o        => t_fx_inc_fx,
   t_fx_dec_fx_o        => t_fx_dec_fx,
   t_fx_set_fx_o        => t_fx_set_fx,
   value_fx_o           => value_fx,
   t_fx_clear_o         => t_fx_clear
  );
  
  -- LoopMAN logo on 7-seg display
  loopman_7seg <= not("00111000" & "01011100" & "01011100" & "01110011" & "00110011" & "00100111" & "01110111" & "01010100");
  
  sSegDisplay_component : sSegDisplay
  port map (
    ck     => ui_clk,
    number => loopman_7seg,
    seg    => disp_seg_o,
    an     => disp_an_o
  );

end syn;
