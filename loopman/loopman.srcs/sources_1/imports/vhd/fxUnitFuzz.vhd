-- fxUnitFuzz.vhd -----------------------------------------------------------------------
--
-- Fuzz effect.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitFuzz is
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitFuzz;

architecture syn of fxUnitFuzz is

  constant limit : std_logic_vector(31 downto 0) := "00000000" & "11111111" & "11111111" & "11111111";
  
  signal boosted_input         : int_sample(0 downto 0);
  signal boosted_input_combi   : int_sample(0 downto 0);
  signal asymmetric_clip       : int_sample(0 downto 0);
  signal asymmetric_clip_combi : int_sample(0 downto 0);
  signal dimmed_combi          : int_sample(0 downto 0);
    
  signal dim_control : std_logic_vector(3 downto 0);
  signal dim_control_aux : std_logic_vector(3 downto 0);
  
  signal boost_control : std_logic_vector(3 downto 0);
    
begin

  boost_control <= "00" & st_fx_i(3 downto 2);

  booster : audioBooster 
  port map (
    -- Control
    volume_i    => boost_control,
    -- Audio
    input_i     => input_i,
    output_o    => boosted_input_combi
  );
  
  dim_control_aux <= std_logic_vector(unsigned(not st_fx_i) + 1);
  dim_control <= "11" & dim_control_aux(3 downto 2);
  
  dimmer : audioDimmer
  port map (
    -- Control
    volume_i    => dim_control,
    -- Audio
    input_i     => asymmetric_clip,
    output_o    => dimmed_combi
  );

  asymmetric_clip_combi(0) <= boosted_input(0) when boosted_input(0)(31) = '1' else
                              boosted_input(0) when unsigned(boosted_input(0)) < unsigned(limit) else
                              limit;

  main_flow : process (clk_i, rst_i)
    type fx_states is (idle,  -- Waits for input
                       clip,  -- Asymmetrical clipping
                       dim,   -- Reduce volume
                       done); -- Sends out
    variable state : fx_states;
  begin
    output_rdy_o <= '0';
    
    case state is
      when idle =>
      when clip =>
      when dim =>
      when done =>
        output_rdy_o <= '1';
      when others =>
    end case;
  
    if rst_i = '1' then
      boosted_input(0)   <= (others=>'0');
      asymmetric_clip(0) <= (others=>'0');
      output_o(0)        <= (others=>'0');

      state := idle;
      
    elsif rising_edge(clk_i) then
    
      case state is
        when idle =>
          if input_rdy_i = '1' then
            boosted_input <= boosted_input_combi;
            
            if st_fx_i /= (st_fx_i'range=>'0') then
              state := clip;
            else
              output_o <= input_i;
              
              state := done;
            end if;
          end if;
          
        when clip =>
          asymmetric_clip <= asymmetric_clip_combi;
          
          state := dim;
          
        when dim =>
          output_o <= dimmed_combi;
          
          state := done;
          
        when done =>
          
          state := idle;
          
        when others =>
      end case;

    end if;
  end process;
    
end syn;

