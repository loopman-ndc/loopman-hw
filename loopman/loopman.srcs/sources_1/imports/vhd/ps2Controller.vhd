-- ps2Controller.vhd --------------------------------------------------------------------
--
-- Receives data from PS/2 keyboard to generate control signals.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
-----------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity ps2Controller is
  generic (
    OUTPUTS : natural := 2;
    INPUTS  : natural := 2;
    FX_NO   : natural := 2
  );
  port (
    clk_i      : in  std_logic;
    rst_i      : in  std_logic;
    -- PS2
    ps2_clk_i  : in  std_logic;
    ps2_data_i : in  std_logic;
    -- Master
    t_inc_master_vol_o : out std_logic_vector(OUTPUTS-1 downto 0);
    t_dec_master_vol_o : out std_logic_vector(OUTPUTS-1 downto 0);
    -- Tracks
    t_rec_o         : out std_logic_vector(7 downto 0);
    t_play_o        : out std_logic_vector(7 downto 0);
    t_mute_o        : out std_logic_vector(7 downto 0);
    t_solo_o        : out std_logic_vector(7 downto 0);
    t_clear_o       : out std_logic_vector(7 downto 0);
    t_sync_o        : out std_logic_vector(7 downto 0);
    t_inc_looplen_o : out std_logic_vector(7 downto 0);
    t_dec_looplen_o : out std_logic_vector(7 downto 0);
    t_inc_vol_o     : out std_logic_vector(7 downto 0);
    t_dec_vol_o     : out std_logic_vector(7 downto 0);
    t_inc_odub_o    : out std_logic_vector(7 downto 0);
    t_dec_odub_o    : out std_logic_vector(7 downto 0);               
    t_input_sel_o   : out std_logic_vector((8*INPUTS)-1 downto 0); 
    t_output_sel_o  : out std_logic_vector((8*OUTPUTS)-1 downto 0);
    track_select_o  : out std_logic_vector(7 downto 0);
    -- Tempo                                                      
    t_hit_tempo_o   : out std_logic;
    -- FX
    t_fx_toggle_o   : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_enqueue_o  : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_inc_fx_o   : out std_logic_vector((8*FX_NO)-1 downto 0);
    t_fx_dec_fx_o   : out std_logic_vector((8*FX_NO)-1 downto 0);  
    t_fx_clear_o    : out std_logic_vector(7 downto 0)
  );
end ps2Controller;

architecture syn of ps2Controller is

  component ps2Receiver is
    generic (
      REGOUTPUTS : boolean   -- registered outputs
    );
    port (
      -- host side
      rst        : in  std_logic;   -- reset
      clk        : in  std_logic;   -- clock
      dataRdy    : out std_logic;   -- sets to one for a cycle when data is received
      data       : out std_logic_vector (7 downto 0);  -- received data
      -- PS2 side
      ps2Clk     : in  std_logic;   -- PS/2 interface input clock 
      ps2Data    : in  std_logic    -- PS/2 interface data input
    );
  end component;
  
  signal charRdy : std_logic;

  --para PS2
  signal data: std_logic_vector(7 downto 0);
  signal dataRdy: std_logic;

  signal sel    : natural;
  signal incSel : std_logic;
  signal decSel : std_logic;
  
  signal sel_fx     : natural;
  signal inc_sel_fx : std_logic;
  signal dec_sel_fx : std_logic;
  
  signal input_sel  : std_logic_vector(INPUTS-1 downto 0);
  signal output_sel : std_logic_vector(OUTPUTS-1 downto 0);
    
  signal inc_looplen  : std_logic;           
  signal dec_looplen  : std_logic;
             
  signal inc_vol      : std_logic;           
  signal dec_vol      : std_logic;
             
  signal inc_odub     : std_logic;           
  signal dec_odub     : std_logic;
  
  signal t_solo  : std_logic;
  
  signal fx_inc_fx  : std_logic;
  signal fx_dec_fx  : std_logic;
  signal fx_toggle  : std_logic;
  signal fx_enqueue : std_logic;
  signal fx_clear   : std_logic;

begin

  ps2KeyboardInterface : ps2Receiver
  generic map ( REGOUTPUTS => true )
  port map ( rst => rst_i, clk => clk_i, dataRdy => dataRdy, data => data, ps2Clk => ps2_clk_i, ps2Data => ps2_data_i );

  keyScanner: --FSM
  process (rst_i, clk_i, charRdy, data)
    type states is (keyON, keyOFF, keyPressed);
    variable state : states;
  begin
    t_rec_o            <= (others=>'0');
    t_play_o           <= (others=>'0');
    t_mute_o           <= (others=>'0');
    t_solo             <= '0';
    t_clear_o          <= (others=>'0');
    t_sync_o           <= (others=>'0');
    t_hit_tempo_o      <= '0';
    decSel             <= '0';
    incSel             <= '0';
    input_sel          <= (others=>'0');
    output_sel         <= (others=>'0');              
    inc_vol            <= '0';      
    dec_vol            <= '0';      
    inc_looplen        <= '0';  
    dec_looplen        <= '0';
    fx_inc_fx          <= '0';
    fx_dec_fx          <= '0';
    fx_toggle          <= '0';
    fx_enqueue         <= '0';
    fx_clear           <= '0';
    inc_sel_fx         <= '0';
    dec_sel_fx         <= '0';
    t_inc_master_vol_o <= (others=>'0');
    t_dec_master_vol_o <= (others=>'0');
    
    if charRdy = '1' then
      case data is
        when X"16" => t_play_o  <= "00000001";    -- 1
        when X"1e" => t_play_o  <= "00000010";    -- 2
        when X"26" => t_play_o  <= "00000100";    -- 3
        when X"25" => t_play_o  <= "00001000";    -- 4
        when X"2e" => t_play_o  <= "00010000";    -- 5
        when X"36" => t_play_o  <= "00100000";    -- 6
        when X"3d" => t_play_o  <= "01000000";    -- 7
        when X"3e" => t_play_o  <= "10000000";    -- 8
        
        when X"15" => t_mute_o  <= "00000001";    -- q
        when X"1d" => t_mute_o  <= "00000010";    -- w
        when X"24" => t_mute_o  <= "00000100";    -- e
        when X"2d" => t_mute_o  <= "00001000";    -- r
        when X"2c" => t_mute_o  <= "00010000";    -- t
        when X"35" => t_mute_o  <= "00100000";    -- y
        when X"3c" => t_mute_o  <= "01000000";    -- u
        when X"43" => t_mute_o  <= "10000000";    -- i

        when X"1c" => t_rec_o   <= "00000001";    -- a
        when X"1b" => t_rec_o   <= "00000010";    -- s
        when X"23" => t_rec_o   <= "00000100";    -- d
        when X"2b" => t_rec_o   <= "00001000";    -- f
        when X"34" => t_rec_o   <= "00010000";    -- g
        when X"33" => t_rec_o   <= "00100000";    -- h
        when X"3b" => t_rec_o   <= "01000000";    -- j
        when X"42" => t_rec_o   <= "10000000";    -- k

        when X"1a" => t_clear_o <= "00000001";    -- z
        when X"22" => t_clear_o <= "00000010";    -- x
        when X"21" => t_clear_o <= "00000100";    -- c
        when X"2a" => t_clear_o <= "00001000";    -- v
        when X"32" => t_clear_o <= "00010000";    -- b
        when X"31" => t_clear_o <= "00100000";    -- n
        when X"3a" => t_clear_o <= "01000000";    -- m
        when X"41" => t_clear_o <= "10000000";    -- ,
          
        when X"4D" => t_inc_master_vol_o(0) <= '1'; -- p
        when X"4C" => t_dec_master_vol_o(0) <= '1'; -- �
        when X"54" => t_inc_master_vol_o(1) <= '1'; -- `
        when X"52" => t_dec_master_vol_o(1) <= '1'; -- �

        when X"29" => t_hit_tempo_o <= '1';    -- space

        when X"70" => decSel <= '1';    -- 0 numkeypad decSel
        when X"71" => incSel <= '1';    -- . numkeypad incSel

--      when X"69" => t_input_sel_o(0) <= '1';    -- 1 numkeypad incSel
        when X"72" => fx_dec_fx <= '1';     -- 2 numkeypad incSel
--      when X"7A" => t_input_sel_o(8) <= '1';    -- 3 numkeypad incSel
        when X"6B" => dec_sel_fx <= '1';    -- 4 numkeypad incSel
        when X"73" => fx_toggle <= '1';     -- 5 numkeypad incSel
        when X"74" => inc_sel_fx <= '1';    -- 6 numkeypad incSel
        when X"6C" => fx_enqueue <= '1';    -- 7 numkeypad incSel
        when X"75" => fx_inc_fx <= '1';     -- 8 numkeypad incSel
        when X"7D" => fx_clear <= '1';      -- 9 numkeypad incSel

        when X"05" => input_sel(0) <= '1';     -- F1 
        when X"06" => input_sel(1) <= '1';     -- F2 
        when X"04" => output_sel(0) <= '1';    -- F3 
        when X"0C" => output_sel(1) <= '1';    -- F4  
        
        when X"03" => t_sync_o <= (others=>'1'); -- F5 sync
        when X"0B" => t_solo <= '1';             -- F6 solo
        when X"83" => output_sel(0) <= '1';      -- F7 
        when X"0A" => output_sel(1) <= '1';      -- F8 

        when X"07" => inc_vol <= '1';       -- F12 incVol
        when X"78" => dec_vol <= '1';       -- F11 decVol
        when X"09" => inc_looplen <= '1';   -- F10 incLen
        when X"01" => dec_looplen <= '1';   -- F9  ecLen

        when others =>
          t_rec_o         <= (others=>'0');
          t_play_o        <= (others=>'0');
          t_mute_o        <= (others=>'0');
          t_solo          <= '0';
          t_clear_o       <= (others=>'0');
          t_sync_o        <= (others=>'0');
          t_hit_tempo_o   <= '0';
          decSel          <= '0';
          incSel          <= '0';
          input_sel       <= (others=>'0');
          output_sel      <= (others=>'0');            
          inc_vol         <= '0';      
          dec_vol         <= '0';      
          inc_looplen     <= '0';  
          dec_looplen     <= '0';
          fx_inc_fx       <= '0';
          fx_dec_fx       <= '0';
          fx_toggle       <= '0';
          fx_enqueue      <= '0';
          fx_clear        <= '0';
           
      end case;
    end if;
  
    if rst_i='1' then
      state   := keyON;
    elsif rising_edge(clk_i) then
        case state is
          when keyON =>
            if dataRdy='1' then -- process data from ps2
              case data is
                when X"F0" => 
                  state := keyOFF; -- On key release go to keyOn to see what depressed
                when X"E0" => 
                when others =>
                  charRdy <= '1';
                  state := keyPressed;
              end case;
            end if;
          when keyPressed =>
            charRdy <= '0';
            state := keyON;
          when keyOFF =>
            if dataRdy='1' then 
              state := keyON; -- returns to initial state
            end if;
          when others => 
        end case;
    end if;
  end process;
  
  -- processes for selecting changes
  selecCounter :
  process (rst_i, clk_i, incSel, decSel)
  begin
    if rst_i='1' then
      sel <= 0;
    elsif rising_edge(clk_i) then 
      if incSel = '1' and sel < 7 then
        sel <= sel + 1;
      elsif decSel = '1' and sel > 0 then
        sel <= sel - 1;
      end if;
    end if;
  end process;
  
  track_select_o <= "00000001" when sel = 0 else
                    "00000010" when sel = 1 else
                    "00000100" when sel = 2 else
                    "00001000" when sel = 3 else
                    "00010000" when sel = 4 else
                    "00100000" when sel = 5 else
                    "01000000" when sel = 6 else
                    "10000000";
  
  volControl :
  process (rst_i, clk_i, inc_vol, dec_vol)
  begin
    if rst_i='1' then
      t_inc_vol_o <= (others=>'0');
      t_dec_vol_o <= (others=>'0');
    elsif rising_edge(clk_i) then
      t_inc_vol_o <= (others=>'0');
      t_dec_vol_o <= (others=>'0');
      if inc_vol = '1' then
        t_inc_vol_o(sel) <= '1';
      elsif dec_vol = '1' then
        t_dec_vol_o(sel) <= '1';
      end if;
    end if;
  end process;
  
  lenControl :
  process (rst_i, clk_i, inc_looplen, dec_looplen)
  begin
    if rst_i='1' then
      t_inc_looplen_o <= (others=>'0');
      t_dec_looplen_o <= (others=>'0');
    elsif rising_edge(clk_i) then
      t_inc_looplen_o <= (others=>'0');
      t_dec_looplen_o <= (others=>'0');
      if inc_looplen = '1' and sel < 8 then
        t_inc_looplen_o(sel) <= '1';
      elsif dec_looplen = '1' and sel < 8 then
        t_dec_looplen_o(sel) <= '1';
      end if;
    end if;
  end process;

  inoutControl :
  process (rst_i, clk_i)
  begin
    if rst_i='1' then
      t_input_sel_o  <= (others=>'0');
      t_output_sel_o <= (others=>'0');
    elsif rising_edge(clk_i) then
      t_input_sel_o  <= (others=>'0');
      t_output_sel_o <= (others=>'0');
      if input_sel(0) = '1' and sel < 8 then
        t_input_sel_o(sel*2) <= '1';
      elsif input_sel(1) = '1' and sel < 8 then
        t_input_sel_o((sel*2)+1) <= '1';
      end if;
      if output_sel(0) = '1' and sel < 8 then
        t_output_sel_o(sel*2) <= '1';
      elsif output_sel(1) = '1' and sel < 8 then
        t_output_sel_o((sel*2)+1) <= '1';
      end if;
    end if;
  end process;
  
  -- process para solo
  soloControl :
  process (rst_i, clk_i, t_solo, sel)
  begin
    t_solo_o <= (others=>'0');
    if t_solo = '1' and sel < 8 then
      t_solo_o(sel) <= '1';
    end if;
  end process;
  
  -- process para efectos
  fx_control : process (rst_i, clk_i)
  begin
    if rst_i = '1' then
      t_fx_toggle_o   <= (others=>'0');
      t_fx_enqueue_o  <= (others=>'0');
      t_fx_inc_fx_o   <= (others=>'0');
      t_fx_dec_fx_o   <= (others=>'0');
      t_fx_clear_o    <= (others=>'0');
      sel_fx <= 0;
    elsif rising_edge(clk_i) then
      t_fx_toggle_o   <= (others=>'0');
      t_fx_enqueue_o  <= (others=>'0');
      t_fx_inc_fx_o   <= (others=>'0');
      t_fx_dec_fx_o   <= (others=>'0');
      t_fx_clear_o    <= (others=>'0');
      
      if inc_sel_fx = '1' and sel_fx < FX_NO-1 then
        sel_fx <= sel_fx + 1;
      elsif dec_sel_fx = '1' and sel_fx > 0 then
        sel_fx <= sel_fx - 1;
      end if;
      
      if fx_inc_fx = '1' and sel < 8 then
        t_fx_inc_fx_o(sel_fx+(sel*FX_NO)) <= '1';
      elsif fx_dec_fx = '1' and sel < 8 then
        t_fx_dec_fx_o(sel_fx+(sel*FX_NO)) <= '1';
      end if;
      
      if fx_toggle = '1' and sel < 8 then
        t_fx_toggle_o(sel_fx+(sel*FX_NO)) <= '1';
      end if;
      
      if fx_enqueue = '1' and sel < 8 then
        t_fx_enqueue_o(sel_fx+(sel*FX_NO)) <= '1';
      end if;

      if fx_clear = '1' and sel < 8 then
        t_fx_clear_o(sel) <= '1';
      end if;
    end if;
  end process;

end syn;

