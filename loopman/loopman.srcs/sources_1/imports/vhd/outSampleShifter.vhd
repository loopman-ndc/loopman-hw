-- outSampleShifter.vhd ----------------------------------------------------------------------
--
-- Serializes output samples for I2S protocol.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity outSampleShifter is
  generic (
    WIDTH   : natural := 24 -- sample width
  );
  port ( 
    -- Control
    rst_i       : in  std_logic;  -- async reset
    clk_75_i    : in  std_logic;  -- clock      
    -- IO
    output0_i   : in  std_logic_vector(WIDTH-1 downto 0); -- sample to AudioCodec   
    output1_i   : in  std_logic_vector(WIDTH-1 downto 0); -- sample to AudioCodec 
    -- IIS
    channel_i   : in  std_logic;             -- channel of serial input
    bit_num_i   : in  unsigned(4 downto 0);  -- bit being sent
    clk_cycle_i : in  unsigned(4 downto 0);  -- cycle number for current bit
    sdout_o     : out std_logic              -- serial data to DAC
  );
end outSampleShifter;

---------------------------------------------------------------------

architecture syn of outSampleShifter is

begin

 shifter : process (rst_i, clk_75_i, bit_num_i, channel_i)
    variable sample_r : std_logic_vector(23 downto 0);
    variable sample_l : std_logic_vector(23 downto 0);
  begin
    if bit_num_i = 0 and bit_num_i >= 25 then
      sdout_o <= '0';
    elsif channel_i = '0' then
      sdout_o <= sample_r(23);
    else
      sdout_o <= sample_l(23);
    end if;
    
    if rst_i = '1' then
      sample_r := (others=>'0');
      sample_l := (others=>'0');
    elsif rising_edge(clk_75_i) then
      if bit_num_i = 0 and clk_cycle_i = 23 then
        if channel_i = '0' then
          -- Reads samples to send throught I2S
          sample_r(23 downto 24-WIDTH) := output0_i;
          sample_l(23 downto 24-WIDTH) := output1_i;
        end if;
      elsif bit_num_i > 0 and bit_num_i < 25 and clk_cycle_i = 23 then
        -- Shifts for serialization
        if channel_i = '0' then
          sample_r := sample_r(22 downto 0) & '0';
        else
          sample_l := sample_l(22 downto 0) & '0';
        end if;
      end if;
    end if;
  end process;

end syn;
