-- trackInputMixer.vhd -----------------------------------------------------------------------
--
-- Mixes audio inputs, so each track receives the set of inputs selected on its control
-- signal.
-- 4 input channels produces 26 bit wide mix at max, for 8 inputs it would be 27 bit wide.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.common.all;

entity trackInputMixer is
  generic ( 
    INPUTS : natural := 2
  );
  port (
    -- System
    rst_i       : in  std_logic;
    clk_i       : in  std_logic;
    -- Audio outputs 
    mix_o       : out int_sample(7 downto 0);
    mix_rdy_o   : out std_logic_vector(7 downto 0);
    -- Audio inputs (audio to mix)
    input_i     : in  ext_sample(INPUTS-1 downto 0);
    input_rdy_i : in std_logic; -- new samples from audio codec
    -- Tracks (input selection)
    in_sel_i    : in  std_logic_vector((INPUTS*8)-1 downto 0)
  );
end trackInputMixer;

architecture syn of trackInputMixer is

  component audioMixer is
    generic ( 
      INPUTS : natural := 2
    );
    port (
      -- System
      rst_i       : in std_logic;
      clk_i       : in std_logic;
      -- Control
      input_rdy_i : in  std_logic;
      mix_rdy_o   : out std_logic;
      clipping_o  : out std_logic;
      sel_i       : in  std_logic_vector(INPUTS-1 downto 0);
      -- Audio
      input_i     : in  int_sample(INPUTS-1 downto 0);
      mix_o       : out int_sample(0 downto 0)
    );
  end component;

  signal int_input : int_sample(INPUTS-1 downto 0);
  
  type mix_set_type is array (integer range <>) of int_sample (0 downto 0);
  signal mix_set : mix_set_type(7 downto 0);

begin

  -- Convert 24 bit samples to 32 bit samples
  ext_to_int_input : process
  begin
    for i in 0 to INPUTS-1 loop
      if input_i(i)(23) = '0' then
        int_input(i) <= "0000" & input_i(i) & "0000";
      else
        int_input(i) <= "1111" & input_i(i) & "1111";
      end if;
    end loop;
  end process;
  
  process
  begin
    for i in 0 to 7 loop
      mix_o(i) <= mix_set(i)(0);
    end loop;
  end process;
  
  -- Mixers
  mixers : for i in 0 to 7 generate
    mixer : audioMixer
    generic map (INPUTS => INPUTS)
    port map (
      -- System
      rst_i       => rst_i,
      clk_i       => clk_i,
      -- Control
      input_rdy_i => input_rdy_i,
      mix_rdy_o   => mix_rdy_o(i),
      clipping_o  => open,
      sel_i       => in_sel_i(((i+1)*INPUTS)-1 downto i*INPUTS),
      -- Audio
      input_i     => int_input,
      mix_o       => mix_set(i)
    );
  end generate;

end syn;
