-- fxUnitLPF.vhd ---------------------------------------------------------------------------
--
-- Low pass filter
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;
use work.hammingCoefs.all;

entity fxUnitLPF is
  generic (
    ENABLED : boolean := true
  );
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitLPF;

architecture syn of fxUnitLPF is
  
  type   ram_type is array (0 to N_TAPS-1) of std_logic_vector(31 downto 0);
  signal tap_buffer : ram_type;
  
  signal rom_out : std_logic_vector(31 downto 0);
  
  signal ram_write     : std_logic;
  signal ram_ena       : std_logic;
  signal ram_dout      : std_logic_vector(31 downto 0);
  signal rom_ena       : std_logic;
  
  signal ram_rd_pointer : natural range 0 to N_TAPS-1;
  signal ram_wr_pointer : natural range 0 to N_TAPS-1;
  signal ram_rd_start   : natural range 0 to N_TAPS-1;
  signal rom_pointer    : natural range 0 to N_TAPS-1;
  
  signal ram_out : int_sample(0 downto 0);
  
  -- MAC
  signal mac_in0 : signed(31 downto 0);
  signal mac_in1 : signed(31 downto 0);
  signal mac_out : signed(63 downto 0);
  signal mac_reg : signed(63 downto 0);
  
  signal high_cut     : int_sample(0 downto 0);
  signal high_cut_rdy : std_logic;
  
begin

  output_o <= high_cut when st_fx_i /= "0000" and ENABLED else input_i;

  ram_access : process(clk_i, rst_i)
  begin
    if rising_edge(clk_i) then
      if ram_ena = '1' then
        if ram_write = '1' then
          tap_buffer(ram_wr_pointer) <= input_i(0);
        end if;
        ram_out(0) <= tap_buffer(ram_rd_pointer);
      end if;
    end if;
  end process;
  
  rom_access : process(clk_i, rst_i)
  begin
    if rising_edge(clk_i) then
      if rom_ena = '1' then
        if    st_fx_i(3 downto 1) = "001" or st_fx_i(3 downto 1) = "000" then
          rom_out <= coeflp_15k_rom(rom_pointer);
        elsif st_fx_i(3 downto 1) = "010" then
          rom_out <= coeflp_15k_rom(rom_pointer);
        elsif st_fx_i(3 downto 1) = "100" or st_fx_i(3 downto 1) = "011" then
          rom_out <= coeflp_3dot5k_rom(rom_pointer);
        elsif st_fx_i(3 downto 1) = "101" then
          rom_out <= coeflp_1k_rom(rom_pointer);
        elsif st_fx_i(3 downto 1) = "110" then
          rom_out <= coeflp_400_rom(rom_pointer);
        elsif st_fx_i(3 downto 1) = "111" then
          rom_out <= coeflp_220_rom(rom_pointer);
        end if;
      end if;
    end if;
  end process;
  
  -- MAC
  mac_out <= mac_reg + (mac_in0*mac_in1);
  
  fx_main_control : process(clk_i, rst_i)
      type fx_states is (idle, -- Waits for input adding 1 cycle delay
                         wr,   -- Writes ram
                         lo_filter,  
                         ready); 
      variable state : fx_states;
      
      variable coef_reverse : boolean;
    begin
      ram_ena <= '0';
      ram_write <= '0';
      rom_ena <= '0';
      output_rdy_o <= '0';
      
      mac_in0 <= (others=>'0');
      mac_in1 <= (others=>'0');
      
      case state is
        when idle =>
          if input_rdy_i = '1' and st_fx_i = "0000" then
            output_rdy_o <= '1';
          end if;
                    
        when wr =>
          ram_ena <= '1';
          ram_write <= '1';
        when lo_filter =>
          if ram_rd_pointer + 1 /= ram_rd_start then
            ram_ena <= '1';
            rom_ena <= '1';
          end if;
          
          mac_in0 <= signed(ram_out(0));
          mac_in1 <= signed(rom_out);
        when ready =>
          output_rdy_o <= '1';
        when others =>
      end case;
    
      if rst_i = '1' then
        ram_wr_pointer <= 0;
        ram_rd_pointer <= 0;
        ram_rd_start   <= 0;
        rom_pointer    <= 0;
        mac_reg        <= (others=>'0');
        high_cut(0)     <= (others=>'0');
        high_cut_rdy    <= '0';
        coef_reverse   := false;
      
        state := idle;
  
      elsif rising_edge(clk_i) then
        high_cut_rdy <= '0';
      
        case state is
          when idle =>
            if input_rdy_i = '1' and st_fx_i /= "0000" then
              mac_reg        <= (others=>'0');
              state := wr;
            end if;
            
          when wr =>
            ram_wr_pointer <= ram_wr_pointer + 1;
            ram_rd_pointer <= ram_rd_start;
            rom_pointer    <= 0;
            coef_reverse   := false;
            
            state := lo_filter;
            
          when lo_filter =>
            if ram_rd_pointer + 1 = ram_rd_start then
              -- Last
              ram_rd_start <= ram_rd_start + 1;
              high_cut(0) <= std_logic_vector(mac_out(46 downto 15));
              high_cut_rdy <= '1';
              
              state := ready;
            elsif ram_rd_pointer = ram_rd_start then
              -- First
              ram_rd_pointer <= ram_rd_pointer + 1;
              rom_pointer <= rom_pointer + 1;
            else
              ram_rd_pointer <= ram_rd_pointer + 1;
              
              if coef_reverse = false and rom_pointer = (((N_TAPS+1)/2))-1 then
                coef_reverse := true;
                rom_pointer <= rom_pointer - 1;
              elsif coef_reverse = false then
                rom_pointer <= rom_pointer + 1;
              else
                rom_pointer <= rom_pointer - 1;
              end if;
              
              mac_reg <= mac_out;
            end if;
            
          when ready =>
            state := idle;
            
          when others =>
            state := idle;
        end case;
      end if;
    end process;

end syn;
