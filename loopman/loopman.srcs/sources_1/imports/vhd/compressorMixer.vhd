-- compressorMixer.vhd -----------------------------------------------------------------------
--
-- Digital mixer with saturation control. It also has a built in compressor and a volume 
-- booster.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity compressorMixer is
  generic ( 
    INPUTS : natural := 2
  );
  port (
    -- System
    rst_i        : in std_logic;
    clk_i        : in std_logic;
    -- Control
    compressor_i : in  std_logic_vector(3 downto 0);
    volume_i     : in  std_logic_vector(3 downto 0);
    sel_i        : in  std_logic_vector(INPUTS-1 downto 0);
    compressed_o : out std_logic;
    -- Audio
    input_i      : in  int_sample(INPUTS-1 downto 0);
    input_rdy_i  : in  std_logic; 
    mix_o        : out int_sample(0 downto 0);
    mix_rdy_o    : out std_logic
  );
end compressorMixer;

architecture syn of compressorMixer is

  -- Compressor constants
  constant max_clip        : std_logic_vector(31 downto 0) :=  "01111111111111111111111111111111";
  constant min_clip        : std_logic_vector(31 downto 0) :=  "10000000000000000000000000000000";
  
  constant threshold_max   : natural := 1_000_000_000;
  constant threshold_step  : natural := 65_000_000;
  
  constant comp_max_window : unsigned(9 downto 0) := '0' & to_unsigned(255, 9); --127

  -- Having 8 (32 bit) inputs as max, result wont exceed 35 bits wide
  signal sample_to_mix     : std_logic_vector(31 downto 0); -- Comes from component input
  signal sample_to_mix_aux : std_logic_vector(34 downto 0); -- Extended component input
  signal mixed_sample      : std_logic_vector(34 downto 0); -- Accumulated addition
  signal added_sample      : std_logic_vector(34 downto 0); -- Addition (combinational output)
  
  signal compressor        : natural; -- Sanple clipped (combinational output) 
  
  signal output            : int_sample(0 downto 0); -- intermediate signal
  
  signal vol_factor        : signed(5 downto 0);
  signal volume_aux        : std_logic_vector(5 downto 0);
  signal cval              : signed(5 downto 0);
  
  signal vol_set_mix_combi : signed(40 downto 0); -- intermediate signal
  signal vol_set_mix       : signed(40 downto 0); -- intermediate signal
  
  signal over_threshold    : std_logic;
  signal clipping          : std_logic;

begin

  mix_o <= output;
  
  process (clk_i, rst_i)
    type states is (idle, mixing, set_vol, feedback, ready);
    variable state : states;
    variable sample_sel : natural range 0 to INPUTS-1;
    
    variable comp_sample_count, comp_over_count : unsigned(9 downto 0);
  begin
    -- Ready bit
    mix_rdy_o <= '0';

    -- Selected sample
    if sel_i(sample_sel) = '1' then
      sample_to_mix <= input_i(sample_sel);
    else
      sample_to_mix <= (others=>'0');
    end if;
    
    case state is
      when idle =>
      when mixing =>
      when set_vol =>
      when feedback =>
      when ready =>
        mix_rdy_o <= '1';
      when others =>
    end case;
    
    if rst_i = '1' then
      sample_sel := 0;
      sample_to_mix <= (others=>'0');
      mixed_sample <= (others=>'0');
      compressed_o <= '0';
      
      comp_sample_count := (others=>'0');
      comp_over_count := (others=>'0');
      
      state := idle;
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          sample_sel := 0;
          
          if input_rdy_i = '1' then
            -- Leave output loaded as is, especially compressed_o (if ends up
            -- connected to a LED... it might be useful)
            mixed_sample <= (others=>'0');
            
            state := mixing;
          end if;
          
        when mixing =>
          if sample_sel = INPUTS-1 then 
            -- Copy last mix to output and goes on to vol/compression
          
            state := set_vol;
          end if;
          mixed_sample <= added_sample;
    
          sample_sel := sample_sel + 1;
        
        when set_vol =>
          vol_set_mix <= vol_set_mix_combi;
          
          state := feedback;
        
        when feedback =>
          -- Checks if final sample needs compression, based on input parameter
          if comp_sample_count >= comp_max_window then
            if comp_over_count > to_unsigned(50, 9) and cval < "001111"  then --10
              -- More compression
              cval <= cval + 1;
            elsif comp_over_count = 0 and cval > "000000" then
              -- Less compression
              cval <= cval - 1;
            end if;
            
            comp_sample_count := (others=>'0');
            comp_over_count := (others=>'0');
          else
            if over_threshold = '1' then
              comp_over_count := comp_over_count + 1;
            end if;
            comp_sample_count := comp_sample_count + 1;
          end if;
          
          if unsigned(volume_aux) = 0 then
            vol_factor <= (others=>'0');
          elsif unsigned(cval) > unsigned(volume_aux) then
            vol_factor <= "000001";
          else
            vol_factor <= signed(volume_aux) - cval + 1;
          end if;
          
          if cval = 0 then
            compressed_o <= '0';
          else
            compressed_o <= '1';
          end if;
          
          state := ready;
              
        when ready =>
          -- Moore ready bit set
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
  compressor <= to_integer(unsigned(compressor_i));
  
  -- Threshold preset constant easy assignment to detect signals over threshold. Combinational
  process
    type natural_vector is array (integer range <>) of natural;
    variable threshold_preset : natural_vector(15 downto 0);
    variable output_val : natural;
    
  begin
    over_threshold <= '0';
    if output(0)(31) = '0' then
      output_val := to_integer(signed(output(0)));
    else
      output_val := to_integer(-signed(output(0)));
    end if;
  
    for i in 0 to 15 loop
      threshold_preset(i) := threshold_max-(i*threshold_step);
      
      if compressor = i then
        if output_val > threshold_preset(i) then
          over_threshold <= '1';
        end if;
      end if;
    end loop;
  end process;
  
  -- Volume/compression set
  volume_aux <= "00" & volume_i;
  vol_set_mix_combi <= signed(mixed_sample) * vol_factor;
  
  -- Clipping mechanism (to preserve output level) 32 bit output
  -- Checks if sample is clipped after doing all operations, as while mixing,
  -- overflow may appear and disappear depending on samples and volume value
  output(0) <= max_clip when vol_set_mix(40) = '0' and vol_set_mix(40 downto 31) /= "0000000000" else
               min_clip when vol_set_mix(40) = '1' and vol_set_mix(40 downto 31) /= "1111111111" else
               std_logic_vector(vol_set_mix(31 downto 0));
--               std_logic_vector(vol_set_mix(30 downto 0)) & "0" when vol_set_mix(40) = '0'       else
--               std_logic_vector(vol_set_mix(30 downto 0)) & "1";
               
  clipping <= '1' when vol_set_mix(40 downto 31) /= "0000000000" or  vol_set_mix(40 downto 31) /= "1111111111" else
              '0';

  -- Extend input sample from 32 to 35 bits
  sample_to_mix_aux <= "111" & sample_to_mix when sample_to_mix(31) = '1' else
                       "000" & sample_to_mix;

  -- Add. Will never overflow, as samples are 32 bits wide
  added_sample <= std_logic_vector(signed(sample_to_mix_aux) + signed(mixed_sample));
    
end syn;