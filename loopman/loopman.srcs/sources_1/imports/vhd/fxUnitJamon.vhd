-- fxUnitJamon.vhd --------------------------------------------------------------------------
--
-- Audio effect that imitates Hammond organ with rotary speaker.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
---------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitJamon is
  generic (
    BRAM_SIZE    : natural := (2**14)
  );
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitJamon;

architecture syn of fxUnitJamon is

  constant BRAM_ADDRESS_LEN : natural := log2(BRAM_SIZE);
  
  type ram_type is array (0 to BRAM_SIZE-1) of std_logic_vector(31 downto 0);
  signal br : ram_type;
  attribute ram_style : string;
  attribute ram_style of br : signal is "block";
  
  signal st_fx     : natural;
  signal st_fx_aux : unsigned(12 downto 0);
  
  signal current_past_sample : int_sample(0 downto 0);
  signal mixer_output        : int_sample(0 downto 0);
  signal half_input          : int_sample(0 downto 0);
  signal half_past_sample    : int_sample(0 downto 0);
  
  signal br_write : std_logic;
  signal br_ena   : std_logic;
  signal br_dout  : std_logic_vector(31 downto 0);
  
  signal rd_pointer : natural range 0 to BRAM_SIZE-1;
  signal wr_pointer : natural range 0 to BRAM_SIZE-1;
  signal br_pointer : natural range 0 to BRAM_SIZE-1;
  
begin
  
  -- True bypass
  output_o <= mixer_output when st_fx_i /= (st_fx_i'range=>'0') else input_i;
  
  -- Value control
  st_fx_aux <= unsigned(st_fx_i) & "000000000";
  st_fx <= to_integer(st_fx_aux);

  block_ram_access : process(clk_i, rst_i)
  begin
    if rising_edge(clk_i) then
      if br_ena = '1' then
        if br_write = '1' then
          br(wr_pointer) <= input_i(0);
        end if;
        current_past_sample(0) <= br(rd_pointer);
      end if;
    end if;
  end process;

  -- Mixer
  half_input(0) <= "0" & input_i(0)(31 downto 1) when input_i(0)(31) = '0' else
                   "1" & input_i(0)(31 downto 1);
                   
  half_past_sample(0) <= "0" & current_past_sample(0)(31 downto 1) when current_past_sample(0)(31) = '0' else
                         "1" & current_past_sample(0)(31 downto 1);

  mixer : audioMixerCombi
    port map (
      -- Control
      clipping_o  => open,
      -- Audio
      input0_i    => half_input,
      input1_i    => half_past_sample,
      mix_o       => mixer_output
    );
  
  fx_main_control : process(clk_i, rst_i)
    type fx_mix_states is (idle, -- Waits for input adding 1 cycle delay
                           ram,  -- Reads and writes ram
                           mix); -- Mixes and sends out
    variable state : fx_mix_states;
    
    variable reverse : boolean;
  begin
    output_rdy_o <= '0';
    br_write     <= '0';
    br_ena       <= '0';
    
    case state is
      when idle =>
      when ram =>
        br_write <= '1';
        br_ena   <= '1';
      when mix =>
        output_rdy_o <= '1';
      when others =>
    end case;
  
    if rst_i = '1' then
      rd_pointer <= 0;
      wr_pointer <= 0;
      reverse := false;
    
      state := idle;
      
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if input_rdy_i = '1' then
            state := ram;
            
          end if;
          
        when ram =>
          --  Circular buffer, wr pointer goes forward.
          if wr_pointer < st_fx - 1 then
            wr_pointer <= wr_pointer + 1;
          else
            wr_pointer <= 0;
          end if;
        
          --  Rd goes back and forth
          if rd_pointer /= wr_pointer and
             rd_pointer /= (wr_pointer-1) and
             reverse = false then
            if rd_pointer >= st_fx-1 then
              rd_pointer <= 0;
            else
              rd_pointer <= rd_pointer + 2;
            end if;
          elsif rd_pointer /= wr_pointer and
                rd_pointer /= (wr_pointer+1) and
                reverse then
            if rd_pointer = 0 then
              rd_pointer <= st_fx-1;
            else
              rd_pointer <= rd_pointer - 1;
            end if;
          elsif reverse = false then
            if rd_pointer = 0 then
              rd_pointer <= st_fx-1;
            else
              rd_pointer <= rd_pointer - 1;
            end if;
            reverse := not reverse;
          else
            if rd_pointer >= st_fx-1 then
              rd_pointer <= 0;
            else
              rd_pointer <= rd_pointer + 2;
            end if;
            reverse := not reverse;
          end if;

          state := mix;
          
        when mix =>
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;

end syn;
