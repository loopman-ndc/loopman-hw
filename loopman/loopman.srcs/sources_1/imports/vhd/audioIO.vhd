-- audioIO.vhd -------------------------------------------------------------------------------
--
-- Manages input/output for I2S serial buses. Each stereo channel is converted to dual mono
-- lines.
-- Resolution: 24 bits 48.8 KHz.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity audioIO is
  generic (
    INPUTS  : natural := 2;
    OUTPUTS : natural := 2;
    WIDTH   : natural := 24 -- sample width
  );
  port ( 
    -- System
    rst_i        : in  std_logic;   -- async reset
    clk_75_i     : in  std_logic;   -- clock
    -- Control
    input_rdy_o  : out std_logic;   -- sets to high for one cycle when new data is received
    output_req_o : out std_logic;   -- sets to high for one cycle when data to send is requested
    -- IO
    output_i     : in  ext_sample(OUTPUTS-1 downto 0);   -- sample to AudioCodec
    input_o      : out ext_sample(INPUTS-1 downto 0);    -- sample from AudioCodec
    -- IIS
    mclk_o       : out std_logic;   -- master clock, 256fs
    sclk_o       : out std_logic;   -- serial bit clocl, 64fs
    lrck_o       : out std_logic;   -- left-right clock, fs
    sdout_o      : out std_logic_vector((OUTPUTS/2)-1 downto 0); -- serial data to DAC
    sdin_i       : in  std_logic_vector((INPUTS/2)-1 downto 0)   -- serial data from ADC
  );
end audioIO;

---------------------------------------------------------------------

architecture syn of audioIO is
  
  component outSampleShifter is
    generic (
      WIDTH   : natural := 24 -- sample width
    );
    port ( 
      -- Control
      rst_i       : in  std_logic;  -- async reset
      clk_75_i    : in  std_logic;  -- clock      
      -- IO
      output0_i   : in  std_logic_vector(WIDTH-1 downto 0);   -- sample to AudioCodec   
      output1_i   : in  std_logic_vector(WIDTH-1 downto 0);   -- sample to AudioCodec 
      -- IIS
      channel_i   : in  std_logic;             -- channel of serial input
      bit_num_i   : in  unsigned(4 downto 0);  -- bit being sent
      clk_cycle_i : in  unsigned(4 downto 0);  -- cycle number for current bit
      sdout_o     : out std_logic              -- serial data to DAC
    );
  end component;
  
  component inSampleShifter is
    generic (
      WIDTH   : natural := 24 -- sample width
    );
    port ( 
      -- Control
      rst_i       : in  std_logic;  -- async reset
      clk_75_i    : in  std_logic;  -- clock      
      -- IO
      input0_o    : out std_logic_vector(WIDTH-1 downto 0);  -- sample from AudioCodec   
      input1_o    : out std_logic_vector(WIDTH-1 downto 0);  -- sample from AudioCodec 
      -- IIS
      channel_i   : in  std_logic;              -- channel of serial input       
      bit_num_i   : in  unsigned(4 downto 0);   -- bit being received            
      clk_cycle_i : in  unsigned(4 downto 0);   -- cycle number for current bit  
      sdin_i      : in  std_logic               -- serial data from ADC          
    );
  end component;
  
  signal mclk_cnt  : unsigned(8 downto 0);
  signal clk_cycle : unsigned(4 downto 0);
  signal bit_num   : unsigned(4 downto 0);

begin

  mclkGenCounter:
  process (rst_i, clk_75_i)
    variable mclk_gen : unsigned(1 downto 0);
  begin
    if rst_i = '1' then
      mclk_gen  := (others=>'0');
      mclk_cnt  <= (others=>'0');
      clk_cycle <= (others=>'0');
    elsif rising_edge(clk_75_i) then
      -- 25 MHz tick generation
      if mclk_gen < 2 then
        mclk_gen := mclk_gen + 1;
      else
        mclk_gen := (others=>'0');
        -- Freq divider counter
        mclk_cnt <= mclk_cnt + 1;
      end if;
      
      -- Cycle count (75 Mhz)
      if clk_cycle < 23 then
        clk_cycle <= clk_cycle + 1;
      else
        clk_cycle <= (others=>'0');
      end if;
    end if;
  end process;
  
  mclk_o <= mclk_cnt(0); -- 12,5 MHz
  sclk_o <= mclk_cnt(2); -- 3,125 MHz
  lrck_o <= mclk_cnt(8); -- 48,828125 KHz
  
  bit_num <= mclk_cnt(7 downto 3);

  -------------
  -- sample sender
  
  -- sample requested one cycle before processing starts
  output_req_o <= '1' when bit_num = 0 and clk_cycle = 23 and mclk_cnt(8) = '0' else '0';
 
  output_shifter : for i in 0 to (OUTPUTS/2)-1 generate
    output_shifter_i : outSampleShifter
    generic map ( WIDTH => WIDTH )
    port map (
      -- Control
      rst_i       => rst_i,
      clk_75_i    => clk_75_i,
      -- IO
      output0_i   => output_i(i*2),
      output1_i   => output_i((i*2)+1),
      -- IIS
      channel_i   => mclk_cnt(8),
      bit_num_i   => bit_num,
      clk_cycle_i => clk_cycle,
      sdout_o     => sdout_o(i)
    );
  end generate;
  
  -------------
  -- sample receiver

  -- Sets to high for one cycle, when bit_num = 25 (1-24 is receiving)
  input_rdy_o <= '1' when bit_num = 25 and clk_cycle = 0 and mclk_cnt(8) = '0' else '0';

  input_shifter : for i in 0 to (INPUTS/2)-1 generate
    input_shifter_i : inSampleShifter
    generic map ( WIDTH => WIDTH )
    port map (
      -- Control
      rst_i       => rst_i,
      clk_75_i    => clk_75_i,
      -- IO
      input0_o   => input_o(i*2),    
      input1_o   => input_o((i*2)+1),
      -- IIS
      channel_i   => mclk_cnt(8),
      bit_num_i   => bit_num,
      clk_cycle_i => clk_cycle,
      sdin_i      => sdin_i(i)
    );
  end generate;

end syn;
