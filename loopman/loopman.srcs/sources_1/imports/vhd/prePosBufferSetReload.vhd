-- prePosBufferSetReload.vhd ---------------------------------------------------------
--
-- Toggles commit and reaload status bits as requested.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
--------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity prePosBufferSetReload is
port (-- System
      clk_i           : in    std_logic;
      rst_i           : in    std_logic;
      -- Control
      rel_on_pre_buf  : in std_logic; -- turn on buffer reload
      com_on_pos_buf  : in std_logic; -- turn on buffer commit
      rel_off_pre_buf : in std_logic; -- turn on buffer reload
      com_off_pos_buf : in std_logic; -- turn on buffer commit
      -- Reload/Commit status
      com_pos_buf     : out std_logic; -- buffer commit
      rel_pre_buf     : out std_logic -- buffer reload
      );
end prePosBufferSetReload;

architecture syn of prePosBufferSetReload is

-- System
signal clk : std_logic;
signal rst : std_logic;

begin

clk <= clk_i;
rst <= rst_i;

prebuffer_set_reload : process(clk, rst)
begin
 if rst = '1' then
   rel_pre_buf <= '0';
   com_pos_buf <= '0';
   
 elsif rising_edge(clk) then
   -- buffer state change
   if rel_on_pre_buf = '1' then
     rel_pre_buf <= '1';
   elsif rel_off_pre_buf = '1' then
     rel_pre_buf <= '0';
   end if;
   
   if com_on_pos_buf = '1' then
     com_pos_buf <= '1';
   elsif com_off_pos_buf = '1' then
     com_pos_buf <= '0';
   end if;
 end if;
end process;

end syn;
