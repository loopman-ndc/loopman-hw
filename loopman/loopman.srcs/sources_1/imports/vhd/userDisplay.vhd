-- userDisplay.vhd ---------------------------------------------------------------------------
--
-- Wrapper for components that display information to the user.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity userDisplay is
  generic (
    OUTPUTS : natural := 2;
    INPUTS  : natural := 2;
    FX_NO   : natural := 4
  );
  port (
    -- System
    clk_i                  : in  std_logic;
    rst_i                  : in  std_logic;
    -- VGA
    hs_o                   : out std_logic;
    vs_o                   : out std_logic;
    color_o                : out std_logic_vector(11 downto 0);
    -- Bluetooth
    bt_tx_o                : out std_logic;
    -- System status
    -- Tempo
    tempo_value_i          : in  std_logic_vector(15 downto 0);
    t_ch_tempo_value_i     : in  std_logic;
    t_tempo_i              : in  std_logic;
    -- Master
    st_master_vol_i        : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
    t_ch_master_vol_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
    st_master_comp_i       : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
    t_ch_master_comp_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
    st_master_outlevel_i   : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
    st_master_clipping_i   : in  std_logic_vector(OUTPUTS-1 downto 0);
    t_ch_master_outlevel_i : in  std_logic_vector(OUTPUTS-1 downto 0);
    -- Track status
    st_rec_i               : in  std_logic_vector(7 downto 0);
    st_play_i              : in  std_logic_vector(7 downto 0);
    st_mute_i              : in  std_logic_vector(7 downto 0);
    st_solo_i              : in  std_logic_vector(7 downto 0);
    st_sync_i              : in  std_logic_vector(7 downto 0);
    st_auto_rec_i          : in  std_logic_vector(7 downto 0);
    t_ch_status_i          : in  std_logic_vector(7 downto 0);
    st_beat_i              : in  std_logic_vector((8*5)-1 downto 0);
    t_ch_beat_i            : in  std_logic_vector(7 downto 0);
    st_looplen_i           : in  std_logic_vector((8*5)-1 downto 0);
    t_ch_looplen_i         : in  std_logic_vector(7 downto 0);
    st_vol_i               : in  std_logic_vector((8*4)-1 downto 0);
    t_ch_vol_i             : in  std_logic_vector(7 downto 0);
    st_odub_i              : in  std_logic_vector((8*4)-1 downto 0);
    t_ch_odub_i            : in  std_logic_vector(7 downto 0);
    st_outlevel_i          : in  std_logic_vector((8*4)-1 downto 0);
    t_ch_outlevel_i        : in  std_logic_vector(7 downto 0);
    in_sel_i               : in  std_logic_vector((8*INPUTS)-1 downto 0);
    t_ch_in_sel_i          : in  std_logic_vector(7 downto 0);
    out_sel_i              : in  std_logic_vector((8*OUTPUTS)-1 downto 0);
    t_ch_out_sel_i         : in  std_logic_vector(7 downto 0);
    track_select_i         : in  std_logic_vector(7 downto 0);
    -- FX status
    st_on_i                : in  std_logic_vector((8*FX_NO)-1 downto 0);  
    st_queued_i            : in  std_logic_vector((8*FX_NO)-1 downto 0);  
    st_fx_i                : in  std_logic_vector(8*(FX_NO*4)-1 downto 0);
    t_ch_fx_i              : in  std_logic_vector((8*FX_NO)-1 downto 0)   
  );
end userDisplay;

architecture syn of userDisplay is

  component vgaController is
    generic ( OUTPUTS : natural := 2 );
    port (
      rst_i                : in  std_logic;
      clk_i                : in  std_logic;
      -- VGA
      hs_o                 : out std_logic;
      vs_o                 : out std_logic;
      color_o              : out std_logic_vector(11 downto 0);
      -- Track status
      st_rec_i             : in std_logic_vector(7 downto 0);      
      st_play_i            : in std_logic_vector(7 downto 0);      
      st_mute_i            : in std_logic_vector(7 downto 0);      
      st_solo_i            : in std_logic_vector(7 downto 0);      
      st_sync_i            : in std_logic_vector(7 downto 0);      
      st_beat_i            : in std_logic_vector((8*5)-1 downto 0);
      st_looplen_i         : in std_logic_vector((8*5)-1 downto 0);
      st_vol_i             : in std_logic_vector((8*4)-1 downto 0);
      st_odub_i            : in std_logic_vector((8*4)-1 downto 0);
      st_outlevel_i        : in std_logic_vector((8*4)-1 downto 0);
      t_tempo_i            : in std_logic;
      st_master_vol_i      : in std_logic_vector((OUTPUTS*4)-1 downto 0);
      st_master_outlevel_i : in std_logic_vector((OUTPUTS*4)-1 downto 0);
      st_master_clipping_i : in  std_logic_vector(OUTPUTS-1 downto 0);
      track_select_i       : in  std_logic_vector(7 downto 0)
    );
  end component;
  
  component btTransmitter is
    generic (
      OUTPUTS : natural := 2;
      INPUTS  : natural := 2;
      FX_NO   : natural := 4
    );
    port (
      -- System
      clk_i                  : in  std_logic;
      rst_i                  : in  std_logic;
      -- Bluetooth
      bt_tx_o                : out std_logic;
      -- System status
      -- Tempo
      tempo_value_i          : in  std_logic_vector(15 downto 0);
      t_ch_tempo_value_i     : in  std_logic;
      t_tempo_i              : in  std_logic;
      -- Master
      st_master_vol_i        : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
      t_ch_master_vol_i      : in  std_logic_vector(OUTPUTS-1 downto 0);
      st_master_comp_i       : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
      t_ch_master_comp_i     : in  std_logic_vector(OUTPUTS-1 downto 0);
      st_master_outlevel_i   : in  std_logic_vector((OUTPUTS*4)-1 downto 0);
      st_master_clipping_i   : in  std_logic_vector(OUTPUTS-1 downto 0);
      t_ch_master_outlevel_i : in  std_logic_vector(OUTPUTS-1 downto 0);
      -- Track status
      st_rec_i               : in  std_logic_vector(7 downto 0);
      st_play_i              : in  std_logic_vector(7 downto 0);
      st_mute_i              : in  std_logic_vector(7 downto 0);
      st_solo_i              : in  std_logic_vector(7 downto 0);
      st_sync_i              : in  std_logic_vector(7 downto 0);
      st_auto_rec_i          : in  std_logic_vector(7 downto 0);
      t_ch_status_i          : in  std_logic_vector(7 downto 0);
      st_beat_i              : in  std_logic_vector((8*5)-1 downto 0);
      t_ch_beat_i            : in  std_logic_vector(7 downto 0);
      st_looplen_i           : in  std_logic_vector((8*5)-1 downto 0);
      t_ch_looplen_i         : in  std_logic_vector(7 downto 0);
      st_vol_i               : in  std_logic_vector((8*4)-1 downto 0);
      t_ch_vol_i             : in  std_logic_vector(7 downto 0);
      st_odub_i              : in  std_logic_vector((8*4)-1 downto 0);
      t_ch_odub_i            : in  std_logic_vector(7 downto 0);
      st_outlevel_i          : in  std_logic_vector((8*4)-1 downto 0);
      t_ch_outlevel_i        : in  std_logic_vector(7 downto 0);
      in_sel_i               : in  std_logic_vector((8*INPUTS)-1 downto 0);
      t_ch_in_sel_i          : in  std_logic_vector(7 downto 0);
      out_sel_i              : in  std_logic_vector((8*OUTPUTS)-1 downto 0);
      t_ch_out_sel_i         : in  std_logic_vector(7 downto 0);
      -- FX status
      st_on_i                : in  std_logic_vector((8*FX_NO)-1 downto 0);  
      st_queued_i            : in  std_logic_vector((8*FX_NO)-1 downto 0);  
      st_fx_i                : in  std_logic_vector(8*(FX_NO*4)-1 downto 0);
      t_ch_fx_i              : in  std_logic_vector((8*FX_NO)-1 downto 0)   
    );
  end component;
  
begin

  vga : vgaController
  generic map ( OUTPUTS => OUTPUTS)
  port map (
    rst_i                => rst_i,
    clk_i                => clk_i,
    -- VGA               
    hs_o                 => hs_o,
    vs_o                 => vs_o,
    color_o              => color_o,
    -- Track status
    st_rec_i             => st_rec_i,
    st_play_i            => st_play_i,
    st_mute_i            => st_mute_i,
    st_solo_i            => st_solo_i,
    st_sync_i            => st_sync_i,
    st_beat_i            => st_beat_i,
    st_looplen_i         => st_looplen_i,
    st_vol_i             => st_vol_i,
    st_odub_i            => st_odub_i,
    st_outlevel_i        => st_outlevel_i,
    t_tempo_i            => t_tempo_i,
    st_master_vol_i      => st_master_vol_i,
    st_master_outlevel_i => st_master_outlevel_i,
    st_master_clipping_i => st_master_clipping_i,
    track_select_i       => track_select_i
  );
  
  bluetooth : btTransmitter
    generic map (
      OUTPUTS => OUTPUTS,
      INPUTS  => INPUTS,
      FX_NO   => FX_NO
    )
    port map (
      -- System
      clk_i                  => clk_i,
      rst_i                  => rst_i,
      -- Bluetooth          
      bt_tx_o                => bt_tx_o,
      -- System status
      -- Tempo
      tempo_value_i          => tempo_value_i,
      t_ch_tempo_value_i     => t_ch_tempo_value_i,
      t_tempo_i              => t_tempo_i,
      -- Master
      st_master_vol_i        => st_master_vol_i,
      t_ch_master_vol_i      => t_ch_master_vol_i,
      st_master_comp_i       => st_master_comp_i,
      t_ch_master_comp_i     => t_ch_master_comp_i,
      st_master_outlevel_i   => st_master_outlevel_i,
      st_master_clipping_i   => st_master_clipping_i,
      t_ch_master_outlevel_i => t_ch_master_outlevel_i,
      -- Track status   
      st_rec_i               => st_rec_i,
      st_play_i              => st_play_i,
      st_mute_i              => st_mute_i,
      st_solo_i              => st_solo_i,
      st_sync_i              => st_sync_i,
      st_auto_rec_i          => st_auto_rec_i,
      t_ch_status_i          => t_ch_status_i,
      st_beat_i              => st_beat_i,
      t_ch_beat_i            => t_ch_beat_i,
      st_looplen_i           => st_looplen_i,
      t_ch_looplen_i         => t_ch_looplen_i,
      st_vol_i               => st_vol_i,
      t_ch_vol_i             => t_ch_vol_i,
      st_odub_i              => st_odub_i,
      t_ch_odub_i            => t_ch_odub_i,
      st_outlevel_i          => st_outlevel_i,
      t_ch_outlevel_i        => t_ch_outlevel_i,
      in_sel_i               => in_sel_i,
      t_ch_in_sel_i          => t_ch_in_sel_i,
      out_sel_i              => out_sel_i,
      t_ch_out_sel_i         => t_ch_out_sel_i, 
      -- FX status
      st_on_i                => st_on_i,    
      st_queued_i            => st_queued_i,
      st_fx_i                => st_fx_i,     
      t_ch_fx_i              => t_ch_fx_i  
  );
  
end syn;
