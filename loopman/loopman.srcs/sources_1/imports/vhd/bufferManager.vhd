-- bufferManager.vhd --------------------------------------------------------------------
--
-- Reloads read buffer from prebuffer and commits write buffer to posbuffer when
-- necessary. Also manages overdub, mixing audio to write and provides access to read
-- data.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
-----------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity bufferManager is
  port (-- System
        clk_i          : in    std_logic;
        rst_i          : in    std_logic;
        -- Control
        rel_on_pre_buf : out std_logic; -- turn on buffer reload
        com_on_pos_buf : out std_logic; -- turn on buffer commit
        won_i          : in  std_logic; -- enable writing
        op_i           : in  std_logic; -- enable operation
        odub_i         : in  std_logic_vector(3 downto 0); -- overdub (amount of old sample in new sample)
       
        addr_i         : in  std_logic_vector(21 downto 0); -- address input 22 bits to select 32 bit sample
        addr_buf_o     : out  std_logic_vector(19 downto 0); -- buffer current address
        data_i         : in  std_logic_vector(31 downto 0); -- data to write
        pre_buf_i      : in  std_logic_vector(127 downto 0); -- data from prebuffer
        data_o         : out std_logic_vector(31 downto 0); -- data output
        pos_buf_o      : out  std_logic_vector(127 downto 0) -- data from posbuffer
        );
end bufferManager;

architecture syn of bufferManager is

component overdubber is
  port (
    new_sample_i : in  std_logic_vector(31 downto 0);
    old_sample_i : in  std_logic_vector(31 downto 0);
    overdub      : in  std_logic_vector(3 downto 0);
    mix_o        : out std_logic_vector(31 downto 0)
  );
end component;

-- System
signal clk : std_logic;
signal rst : std_logic;

-- Buffer address
signal addr_buf : std_logic_vector(19 downto 0);

-- Read buffer
signal rd_buf : std_logic_vector(127 downto 0);

-- Read data
signal rd_data : std_logic_vector(31 downto 0);

-- Write buffer
signal wr_buf : std_logic_vector(127 downto 0);

-- Postbuffer
signal pos_buf : std_logic_vector(127 downto 0);

-- Overdubbed mix 
signal mix : std_logic_vector(31 downto 0);
signal mixer_ext_in : std_logic_vector(31 downto 0);

-- Input keepers
signal won_int  : std_logic;
signal addr_int : std_logic_vector(21 downto 0);

begin

clk <= clk_i;
rst <= rst_i;

mixer0 : overdubber
port map(
  new_sample_i => mixer_ext_in,--data_i,
  old_sample_i => rd_data,
  overdub      => odub_i,
  mix_o        => mix
);

-- Output signals
data_o <= rd_data;
addr_buf_o <= addr_buf;
pos_buf_o <= pos_buf;

-- Data output from read buffer (ready 1 cycle after op_i is set to '1')
rd_data <= rd_buf(31 downto 0)  when addr_int(1 downto 0) = "00" else
           rd_buf(63 downto 32) when addr_int(1 downto 0) = "01" else
           rd_buf(95 downto 64) when addr_int(1 downto 0) = "10" else
           rd_buf(127 downto 96); -- when addr0_i(2 downto 0) = "11"

buffer_reload : process (clk, rst, op_i, addr_buf, addr_i, won_i)
  -- For buffer operations
  type buffer_state is (idle,      -- Waits for external request
                        operation, -- begin
                        done);     -- done
  variable buffer_reload_state : buffer_state;
begin
  rel_on_pre_buf <= '0';
  com_on_pos_buf <= '0';
  
  case buffer_reload_state is
    when idle =>
      if op_i = '1' then
        -- operation requested
        if addr_buf /= addr_i(21 downto 2) then
          -- read buffer reload needed
          rel_on_pre_buf <= '1';
          if won_i = '1' then
            -- write buffer commit needed
            com_on_pos_buf <= '1';
          end if;
        end if;
      end if;
    when operation =>
    when others =>
  end case;
  
  if rst = '1' then
    rd_buf <= (others=>'0');
    wr_buf <= (others=>'0');
    pos_buf <= (others=>'0');
    addr_buf <= (others=>'0');
    mixer_ext_in <= (others=>'0');
    
    buffer_reload_state := idle;
  elsif rising_edge(clk) then
    case buffer_reload_state is
      when idle =>
        if op_i = '1' then
        -- operation requested
          if addr_buf /= addr_i(21 downto 2) then
            addr_buf <= addr_i(21 downto 2);
            -- read buffer reload needed
            rd_buf <= pre_buf_i;
            if won_i = '1' then
              -- write buffer commit needed
              pos_buf <= wr_buf;
            end if;
          end if;
          
          -- Keep won_i value for operation state
          -- If its writing on next state, load mixer input to avoid timing violations
          if won_i = '1' then
            won_int <= '1';
            mixer_ext_in <= data_i;
          else
            won_int <= '0';
          end if;
          -- Keep addr_i value for operation state
          addr_int <= addr_i;
          
          buffer_reload_state := operation;
        end if;
      when operation =>
        if won_int = '1' then
          case addr_int(1 downto 0) is
            when "00" => wr_buf(31 downto 0)   <= mix;
            when "01" => wr_buf(63 downto 32)  <= mix;
            when "10" => wr_buf(95 downto 64)  <= mix;
            when "11" => wr_buf(127 downto 96) <= mix;
            when others =>
          end case;
        end if;
      
        buffer_reload_state := idle;
      when others =>
        buffer_reload_state := idle;
    end case;
  end if;
end process;

end syn;
