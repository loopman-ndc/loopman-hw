-- pedalController.vhd -----------------------------------------------------------------------
--
-- Cleans (synchronizes, debounces...) input from control pedals and generates control ticks.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity pedalController is
 port (
    -- System
    clk_i          : in std_logic;
    rst_i          : in std_logic;
    -- Input
    rec_encoder_i  : in std_logic_vector(2 downto 0);
    rec_gs_i       : in std_logic;
    play_encoder_i : in std_logic_vector(2 downto 0);
    play_gs_i      : in std_logic;
    aux_encoder_i  : in std_logic_vector(2 downto 0);
    aux_gs_i       : in std_logic;
    -- Control ticks
    t_rec_o        : out std_logic_vector(7 downto 0);
    t_play_o       : out std_logic_vector(7 downto 0);
    t_aux_o        : out std_logic_vector(7 downto 0)
  );
end pedalController;

architecture syn of pedalController is
  
  signal rec_gs_syn, rec_gs_deb, rec_gs_fall    : std_logic;
  signal play_gs_syn, play_gs_deb, play_gs_fall : std_logic;
  signal aux_gs_syn, aux_gs_deb, aux_gs_fall    : std_logic;
  
  signal rec_encoder_syn, rec_encoder_deb   : std_logic_vector(2 downto 0);
  signal play_encoder_syn, play_encoder_deb : std_logic_vector(2 downto 0);
  
begin

  -- Rec gs treatment
  rec_gs_synchronizer : synchronizer
    generic map (STAGES => 2, INIT => '1')
    port map (rst => rst_i, clk => clk_i, x => rec_gs_i, xSync => rec_gs_syn);
   
  rec_gs_debouncer : debouncer
    generic map (FREQ => 30_000, BOUNCE => 50)
    port map (rst => rst_i, clk => clk_i, x_n => rec_gs_syn, xdeb_n => rec_gs_deb);
   
  rec_gs_edge_detector : edgeDetector
    port map (rst => rst_i, clk => clk_i, x_n => rec_gs_deb, xFall => rec_gs_fall, xRise => open);
    
    
  -- Rec encoded treatment
  rec_encoder_synchronizers :
  for i in 0 to 2 generate
    rec_encoder_synchronizer : synchronizer
      generic map (STAGES => 2, INIT => '1')
      port map (rst => rst_i, clk => clk_i, x => rec_encoder_i(i), xSync => rec_encoder_syn(i));
  end generate;
  
  rec_encoder_debouncers :
    for i in 0 to 2 generate
      rec_encoder_debouncer : debouncer
        generic map (FREQ => 30_000, BOUNCE => 50)
        port map (rst => rst_i, clk => clk_i, x_n => rec_encoder_syn(i), xdeb_n => rec_encoder_deb(i));
    end generate;
    
    
  -- Play gs treatment
  play_gs_synchronizer : synchronizer
    generic map (STAGES => 2, INIT => '1')
    port map (rst => rst_i, clk => clk_i, x => play_gs_i, xSync => play_gs_syn);
   
  play_gs_debouncer : debouncer
    generic map (FREQ => 30_000, BOUNCE => 50)
    port map (rst => rst_i, clk => clk_i, x_n => play_gs_syn, xdeb_n => play_gs_deb);
   
  play_gs_edge_detector : edgeDetector
    port map (rst => rst_i, clk => clk_i, x_n => play_gs_deb, xFall => play_gs_fall, xRise => open);


  -- Play encoded treatment
  play_encoder_synchronizers :
  for i in 0 to 2 generate
    play_encoder_synchronizer : synchronizer
      generic map (STAGES => 2, INIT => '1')
      port map (rst => rst_i, clk => clk_i, x => play_encoder_i(i), xSync => play_encoder_syn(i));
  end generate;
  
  play_encoder_debouncers :
    for i in 0 to 2 generate
      play_encoder_debouncer : debouncer
        generic map (FREQ => 30_000, BOUNCE => 50)
        port map (rst => rst_i, clk => clk_i, x_n => play_encoder_syn(i), xdeb_n => play_encoder_deb(i));
    end generate;



  -- Main control
  rec_control_fsm : process
    type states is (idle, generate_ticks);
    variable state : states;
  begin
    t_rec_o <= (others=>'0');
    
    case state is
      when idle =>
      when generate_ticks =>
        t_rec_o(to_integer(unsigned(rec_encoder_deb))) <= '1';
      when others =>
    end case;

    if rst_i = '1' then
      state := idle;
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if rec_gs_fall = '1' then
            state := generate_ticks;
          end if;
        when generate_ticks =>
          state := idle;
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
  play_control_fsm : process
    type states is (idle, generate_ticks);
    variable state : states;
  begin
    t_play_o <= (others=>'0');
    
    case state is
      when idle =>
      when generate_ticks =>
        t_play_o(to_integer(unsigned(play_encoder_deb))) <= '1';
      when others =>
    end case;

    if rst_i = '1' then
      state := idle;
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if play_gs_fall = '1' then
            state := generate_ticks;
          end if;
        when generate_ticks =>
          state := idle;
        when others =>
          state := idle;
      end case;
    end if;
  end process;

end syn;
