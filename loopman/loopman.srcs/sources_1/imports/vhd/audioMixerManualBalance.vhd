-- audioMixerManualBalance.vhd ---------------------------------------------------------------
--
-- Digital secuential mixer with two inputs. Mixes two samples being able to set wich sample
-- will have more presence in final result. e.g 50-50, 25-75, 0-100
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity audioMixerManualBalance is
  port (
    -- System
    rst_i       : in std_logic;
    clk_i       : in std_logic;
    -- Control
    input_rdy_i : in  std_logic;
    mix_rdy_o   : out std_logic;
    clipping_o  : out std_logic;
    balance_i   : in  std_logic_vector(3 downto 0); -- Higher -> input0. Lower -> input1
    -- Audio
    input0_i    : in  int_sample(0 downto 0);
    input1_i    : in  int_sample(0 downto 0);
    mix_o       : out int_sample(0 downto 0)
  );
end audioMixerManualBalance;

architecture syn of audioMixerManualBalance is

  signal fixed_op_0   : signed(5 downto 0);
  signal fixed_op_1   : signed(5 downto 0);
  
  signal aux_input0   : signed(31 downto 0);
  signal aux_input1   : signed(31 downto 0);
  
  signal mixable0     : int_sample(0 downto 0);
  signal aux_mixable0 : signed(37 downto 0);
  signal mixable1     : int_sample(0 downto 0);
  signal aux_mixable1 : signed(37 downto 0);
  
  signal mixer_out    : int_sample(0 downto 0);
  
  signal balance      : unsigned(3 downto 0);

begin
  
  fixed_op_0 <= "000000" when balance = "0000" else
                "010000" when balance = "1111" else
                signed("00" & (balance+1));
  fixed_op_1 <= "010000" when balance = "0000" else
                "000000" when balance = "1111" else
                signed("00" & (not(balance)+1));
  
  aux_input0 <= signed(input0_i(0));
  aux_input1 <= signed(input1_i(0));
  
  mixable0(0) <= std_logic_vector(aux_mixable0(35 downto 4));
  mixable1(0) <= std_logic_vector(aux_mixable1(35 downto 4));
  
  mixer : audioMixerCombi 
  port map (
    -- Control
    clipping_o  => clipping_o,
    -- Audio
    input0_i    => mixable0,
    input1_i    => mixable1,
    mix_o       => mixer_out
  );
  
  mixer_flow : process (clk_i, rst_i)
    type states is (idle, operate, mix, ready);
    variable state : states;
  begin
    mix_rdy_o <= '0';
    
    case state is
      when idle =>
      when operate =>
      when mix =>
      when ready =>
        mix_rdy_o <= '1';
      when others =>
    end case;
    
    if rst_i = '1' then
      balance <= (others=>'0');
      aux_mixable0 <= (others=>'0');
      aux_mixable1 <= (others=>'0');
      mix_o(0) <= (others=>'0');
      
      state := idle;
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          -- Waits for input
          if input_rdy_i = '1' then
            balance <= unsigned(balance_i);
            
            state := operate;
          end if;
          
        when operate =>
          -- Multiply inpus (fixed point operation)
          aux_mixable0 <= fixed_op_0 * aux_input0;
          aux_mixable1 <= fixed_op_1 * aux_input1;
          
          state := mix;
          
        when mix =>
          -- Combinational mixer mixes combinatonally
          mix_o <= mixer_out;
          
          state := ready;
          
        when ready =>
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;
  
end syn;
