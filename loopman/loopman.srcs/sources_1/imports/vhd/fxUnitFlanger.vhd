-- fxUnitFlanger.vhd --------------------------------------------------------------------------
--
-- Flanger audio effect. Mixes original and slightly delayed signal. Delay varies over time.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitFlanger is
  generic (
    BRAM_SIZE    : natural := (2**7)
  );
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitFlanger;

architecture syn of fxUnitFlanger is

  constant BRAM_ADDRESS_LEN : natural := log2(BRAM_SIZE);
  
  constant min_delay : natural := 0;
  
  type ram_type is array (0 to BRAM_SIZE-1) of std_logic_vector(31 downto 0);
  signal br : ram_type;
  attribute ram_style : string;
  attribute ram_style of br : signal is "block";
  
  signal st_fx     : natural;
  signal st_fx_aux : unsigned(8 downto 0);
  
  signal current_past_sample      : int_sample(0 downto 0);
  signal half_input               : int_sample(0 downto 0);
  signal half_current_past_sample : int_sample(0 downto 0);
  
  signal br_write : std_logic;
  signal br_ena   : std_logic;
  signal br_dout  : std_logic_vector(31 downto 0);
  
  signal rd_pointer : natural range 0 to BRAM_SIZE-1;
  signal wr_pointer : natural range 0 to BRAM_SIZE-1;
  signal br_pointer : natural range 0 to BRAM_SIZE-1;
 
begin
  
  -- Value control
  st_fx_aux <= '0' & unsigned(not st_fx_i) & "1111";
  st_fx <= to_integer(st_fx_aux);

  block_ram_access : process(clk_i, rst_i)
  begin
    if rising_edge(clk_i) then
      if br_ena = '1' then
        if br_write = '1' then
          br(wr_pointer) <= input_i(0);
        end if;
        current_past_sample(0) <= br(rd_pointer);
      end if;
    end if;
  end process;

  -- Mixing
  half_input(0) <= "0" & input_i(0)(31 downto 1) when input_i(0)(31) = '0' else
                   "1" & input_i(0)(31 downto 1);
  half_current_past_sample(0) <= "0" & current_past_sample(0)(31 downto 1) when current_past_sample(0)(31) = '0' else
                                 "1" & current_past_sample(0)(31 downto 1);

  mixer : audioMixerCombi
    port map (
      -- Control
      clipping_o  => open,
      -- Audio
      input0_i    => half_input,
      input1_i    => half_current_past_sample,
      mix_o       => output_o
    );
  
  fx_main_control : process(clk_i, rst_i)
    type fx_mix_states is (idle, -- Waits for input adding 1 cycle delay
                           ram,  -- Reads and writes ram
                           mix); -- Mixes and sends out
    variable state : fx_mix_states;
    variable delay_value, delay_count : natural;
    variable reverse : boolean;
  begin
    output_rdy_o <= '0';
    br_write     <= '0';
    br_ena       <= '0';
    
    case state is
      when idle =>
      when ram =>
        br_write <= '1';
        br_ena   <= '1';
      when mix =>
        output_rdy_o <= '1';
      when others =>
    end case;
  
    if rst_i = '1' then
      rd_pointer <= 0;
      wr_pointer <= 0;
      delay_value := 0;
      delay_count := 0;
      reverse := true;
    
      state := idle;
      
    elsif rising_edge(clk_i) then
      case state is
        when idle =>
          if input_rdy_i = '1' then
            state := ram;
            
            -- Update delay value counter
            if delay_count > st_fx then
              delay_count := 0;
              
              -- Update delay value "slowly"
              if delay_value < BRAM_SIZE-1 and delay_value > min_delay then
                if reverse then
                   delay_value := delay_value-1;
                 else
                   delay_value := delay_value+1;
                 end if;
              else
                if reverse then
                   delay_value := delay_value+1;
                   reverse := false;
                 else
                   delay_value := delay_value-1;
                   reverse := true;
                 end if;
              end if;
            else
              delay_count := delay_count + 1;
            end if;
          end if;
          
        when ram =>
          --  Circular buffer
          wr_pointer <= wr_pointer + 1;
          rd_pointer <= wr_pointer - delay_value;
          
          state := mix;
          
        when mix =>
          state := idle;
          
        when others =>
          state := idle;
      end case;
    end if;
  end process;

end syn;