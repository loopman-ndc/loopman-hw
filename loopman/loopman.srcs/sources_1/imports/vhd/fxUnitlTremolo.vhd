-- fxUnitTremolo.vhd -------------------------------------------------------------------------
--
-- Tremolo effect.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitTremolo is
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitTremolo;

architecture syn of fxUnitTremolo is
  
  signal st_fx_n   : std_logic_vector(3 downto 0);
  signal st_fx     : natural;
  signal st_fx_aux : unsigned(21 downto 0);
  
  signal tremolo   : unsigned(3 downto 0);
  
  signal fx       : int_sample(0 downto 0);
  signal fx_combi : int_sample(0 downto 0);
  
begin
  
  -- True bypass
  output_o <= fx when st_fx_i /= (st_fx_i'range=>'0') else input_i;
  output_rdy_o <= input_rdy_i;
  
  -- Value control
  st_fx_n <= not(st_fx_i);
  st_fx_aux <= unsigned(st_fx_n) & "111111111111111111";
  st_fx <= to_integer(st_fx_aux);
    
  delay_dimmer : audioDimmer
    port map (
      -- Control
      volume_i => std_logic_vector(tremolo),
      -- Audi
      input_i  => input_i,
      output_o => fx
    );

  volumeTremolized :
  process (rst_i, clk_i)
    type states is (up, down);
    variable state : states;
    variable count : natural;
  begin
    if rst_i = '1' then
      tremolo <= "1111";
      count := 0;
      state := down;
    elsif rising_edge(clk_i) then
      if count < st_fx then
        count := count + 1;
      else
        count := 0;
        case state is
          when up =>
            if tremolo < "1111" then
               tremolo <= tremolo + 1;
            else
              state := down;
            end if;
          when down =>
            if tremolo > "0011" then -- Tremolo intensity
              tremolo <= tremolo - 1;
            else
              state := up;
            end if;
         end case;
      end if;
    end if;
  end process;

end syn;
