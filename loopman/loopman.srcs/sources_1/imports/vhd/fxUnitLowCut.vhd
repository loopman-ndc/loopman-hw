-- fxUnitLowCut.vhd -----------------------------------------------------------------------
--
-- High pass filter effect.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
-------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.common.all;

entity fxUnitLowCut is
  port (
    -- System
    rst_i        : in  std_logic;
    clk_i        : in  std_logic;
    -- Audio output
    output_o     : out int_sample(0 downto 0);
    output_rdy_o : out std_logic;
    -- Audio input
    input_i      : in  int_sample(0 downto 0);
    input_rdy_i  : in  std_logic;
    -- Control
    st_fx_i      : in  std_logic_vector(3 downto 0)
  );
end fxUnitLowCut;

architecture syn of fxUnitLowCut is
    
  constant N_TAPS : natural := 511;
  constant BRAM_ADDRESS_LEN : natural := log2(N_TAPS+1);
  
  type   ram_type is array (0 to N_TAPS-1) of std_logic_vector(31 downto 0);
  signal tap_buffer : ram_type;
  
  type   rom_type is array (0 to (((N_TAPS+1)/2))-1) of std_logic_vector(31 downto 0);
  signal coef_rom : rom_type :=(
    X"00000003", X"00000003", X"00000003", X"00000003", X"00000003", X"00000003", X"00000003", X"00000003",
    X"00000003", X"00000004", X"00000004", X"00000004", X"00000004", X"00000004", X"00000004", X"00000004",
    X"00000004", X"00000004", X"00000004", X"00000003", X"00000003", X"00000003", X"00000003", X"00000003",
    X"00000003", X"00000003", X"00000003", X"00000003", X"00000002", X"00000002", X"00000002", X"00000002",
    X"00000001", X"00000001", X"00000001", X"00000001", X"00000000", X"00000000", X"00000000", X"FFFFFFFF",
    X"FFFFFFFF", X"FFFFFFFE", X"FFFFFFFE", X"FFFFFFFE", X"FFFFFFFD", X"FFFFFFFD", X"FFFFFFFC", X"FFFFFFFC",
    X"FFFFFFFB", X"FFFFFFFB", X"FFFFFFFA", X"FFFFFFFA", X"FFFFFFF9", X"FFFFFFF9", X"FFFFFFF8", X"FFFFFFF8",
    X"FFFFFFF7", X"FFFFFFF7", X"FFFFFFF6", X"FFFFFFF6", X"FFFFFFF6", X"FFFFFFF5", X"FFFFFFF5", X"FFFFFFF5",
    X"FFFFFFF4", X"FFFFFFF4", X"FFFFFFF4", X"FFFFFFF4", X"FFFFFFF3", X"FFFFFFF3", X"FFFFFFF3", X"FFFFFFF3",
    X"FFFFFFF3", X"FFFFFFF3", X"FFFFFFF3", X"FFFFFFF4", X"FFFFFFF4", X"FFFFFFF4", X"FFFFFFF5", X"FFFFFFF5",
    X"FFFFFFF6", X"FFFFFFF6", X"FFFFFFF7", X"FFFFFFF8", X"FFFFFFF8", X"FFFFFFF9", X"FFFFFFFA", X"FFFFFFFB",
    X"FFFFFFFC", X"FFFFFFFD", X"FFFFFFFE", X"00000000", X"00000001", X"00000002", X"00000003", X"00000005",
    X"00000006", X"00000008", X"00000009", X"0000000B", X"0000000C", X"0000000E", X"00000010", X"00000011",
    X"00000013", X"00000014", X"00000016", X"00000017", X"00000019", X"0000001A", X"0000001C", X"0000001D",
    X"0000001F", X"00000020", X"00000021", X"00000022", X"00000023", X"00000024", X"00000025", X"00000026",
    X"00000026", X"00000027", X"00000027", X"00000027", X"00000027", X"00000027", X"00000027", X"00000027",
    X"00000026", X"00000026", X"00000025", X"00000024", X"00000022", X"00000021", X"0000001F", X"0000001E",
    X"0000001C", X"0000001A", X"00000018", X"00000015", X"00000012", X"00000010", X"0000000D", X"0000000A",
    X"00000007", X"00000003", X"00000000", X"FFFFFFFC", X"FFFFFFF8", X"FFFFFFF4", X"FFFFFFF1", X"FFFFFFED",
    X"FFFFFFE8", X"FFFFFFE4", X"FFFFFFE0", X"FFFFFFDC", X"FFFFFFD8", X"FFFFFFD3", X"FFFFFFCF", X"FFFFFFCB",
    X"FFFFFFC7", X"FFFFFFC3", X"FFFFFFBF", X"FFFFFFBB", X"FFFFFFB7", X"FFFFFFB3", X"FFFFFFB0", X"FFFFFFAC",
    X"FFFFFFA9", X"FFFFFFA6", X"FFFFFFA3", X"FFFFFFA1", X"FFFFFF9E", X"FFFFFF9C", X"FFFFFF9A", X"FFFFFF99",
    X"FFFFFF98", X"FFFFFF97", X"FFFFFF97", X"FFFFFF97", X"FFFFFF97", X"FFFFFF98", X"FFFFFF99", X"FFFFFF9A",
    X"FFFFFF9C", X"FFFFFF9F", X"FFFFFFA2", X"FFFFFFA5", X"FFFFFFA9", X"FFFFFFAD", X"FFFFFFB2", X"FFFFFFB7",
    X"FFFFFFBD", X"FFFFFFC3", X"FFFFFFCA", X"FFFFFFD1", X"FFFFFFD8", X"FFFFFFE0", X"FFFFFFE9", X"FFFFFFF2",
    X"FFFFFFFC", X"00000005", X"00000010", X"0000001B", X"00000026", X"00000031", X"0000003D", X"0000004A",
    X"00000056", X"00000063", X"00000071", X"0000007E", X"0000008C", X"0000009A", X"000000A9", X"000000B7",
    X"000000C6", X"000000D5", X"000000E4", X"000000F3", X"00000102", X"00000111", X"00000120", X"0000012F",
    X"0000013E", X"0000014D", X"0000015B", X"0000016A", X"00000179", X"00000187", X"00000195", X"000001A2",
    X"000001B0", X"000001BD", X"000001CA", X"000001D6", X"000001E2", X"000001ED", X"000001F8", X"00000203",
    X"0000020D", X"00000216", X"0000021F", X"00000227", X"0000022F", X"00000236", X"0000023D", X"00000242",
    X"00000248", X"0000024C", X"00000250", X"00000253", X"00000256", X"00000257", X"00000258", X"00000259"
  );

  signal rom_out : std_logic_vector(31 downto 0);
  
  signal ram_write     : std_logic;
  signal ram_ena       : std_logic;
  signal ram_dout      : std_logic_vector(31 downto 0);
  signal rom_ena       : std_logic;
  
  signal ram_rd_pointer : natural range 0 to N_TAPS-1;
  signal ram_wr_pointer : natural range 0 to N_TAPS-1;
  signal ram_rd_start   : natural range 0 to N_TAPS-1;
  signal rom_pointer    : natural range 0 to N_TAPS-1;
  
  signal ram_out : int_sample(0 downto 0);
  
  -- MAC
  signal mac_in0 : signed(31 downto 0);
  signal mac_in1 : signed(31 downto 0);
  signal mac_out : signed(63 downto 0);
  signal mac_reg : signed(63 downto 0);
  
  signal mix         : int_sample(0 downto 0);
  signal mix_rdy     : std_logic;
  signal low_cut     : int_sample(0 downto 0);
  signal low_cut_rdy : std_logic;
  
begin

  output_o <= mix when st_fx_i /= "0000" else input_i;
  
  -- Mixes wet signal with input at selected amount
  mixer : audioMixerManualBalance
  port map (
    -- System
    rst_i       => rst_i,
    clk_i       => clk_i,
    -- Control 
    input_rdy_i => low_cut_rdy,
    mix_rdy_o   => mix_rdy,
    clipping_o  => open,
    balance_i   => st_fx_i,
    -- Audio  
    input0_i    => low_cut,
    input1_i    => input_i,
    mix_o       => mix
  );

  ram_access : process(clk_i, rst_i)
  begin
    if rising_edge(clk_i) then
      if ram_ena = '1' then
        if ram_write = '1' then
          tap_buffer(ram_wr_pointer) <= input_i(0);
        end if;
        ram_out(0) <= tap_buffer(ram_rd_pointer);
      end if;
    end if;
  end process;
  
  rom_access : process(clk_i, rst_i)
  begin
    if rising_edge(clk_i) then
      if rom_ena = '1' then
        rom_out <= coef_rom(rom_pointer);
      end if;
    end if;
  end process;
  
  -- MAC
  mac_out <= mac_reg + (mac_in0*mac_in1);
  
  fx_main_control : process(clk_i, rst_i)
      type fx_states is (idle, -- Waits for input adding 1 cycle delay
                         wr,   -- Writes ram
                         lo_filter,  
                         mix); 
      variable state : fx_states;
      
      variable coef_reverse : boolean;
    begin
      ram_ena <= '0';
      ram_write <= '0';
      rom_ena <= '0';
      output_rdy_o <= '0';
      
      mac_in0 <= (others=>'0');
      mac_in1 <= (others=>'0');
      
      case state is
        when idle =>
          if input_rdy_i = '1' and st_fx_i = "0000" then
            output_rdy_o <= '1';
          end if;
                    
        when wr =>
          ram_ena <= '1';
          ram_write <= '1';
        when lo_filter =>
          if ram_rd_pointer + 1 /= ram_rd_start then
            ram_ena <= '1';
            rom_ena <= '1';
          end if;
          
          mac_in0 <= signed(ram_out(0));
          mac_in1 <= signed(rom_out);
        when mix =>
          if mix_rdy = '1' then
            output_rdy_o <= '1';
          end if;
        when others =>
      end case;
    
      if rst_i = '1' then
        ram_wr_pointer <= 0;
        ram_rd_pointer <= 0;
        ram_rd_start   <= 0;
        rom_pointer    <= 0;
        mac_reg        <= (others=>'0');
        low_cut(0)     <= (others=>'0');
        low_cut_rdy    <= '0';
        coef_reverse   := false;
      
        state := idle;
  
      elsif rising_edge(clk_i) then
        low_cut_rdy <= '0';
      
        case state is
          when idle =>
            if input_rdy_i = '1' and st_fx_i /= "0000" then
              mac_reg        <= (others=>'0');
              state := wr;
            end if;
            
          when wr =>
            ram_wr_pointer <= ram_wr_pointer + 1;
            ram_rd_pointer <= ram_rd_start;
            rom_pointer    <= 0;
            coef_reverse   := false;
            
            state := lo_filter;
            
          when lo_filter =>
            if ram_rd_pointer + 1 = ram_rd_start then
              -- Last
              ram_rd_start <= ram_rd_start + 1;
              low_cut(0) <= std_logic_vector(mac_out(46 downto 15));
              low_cut_rdy <= '1';
              
              state := mix;
            elsif ram_rd_pointer = ram_rd_start then
              -- First
              ram_rd_pointer <= ram_rd_pointer + 1;
              rom_pointer <= rom_pointer + 1;
            else
              ram_rd_pointer <= ram_rd_pointer + 1;
              
              if coef_reverse = false and rom_pointer = (((N_TAPS+1)/2))-1 then
                coef_reverse := true;
                rom_pointer <= rom_pointer - 1;
              elsif coef_reverse = false then
                rom_pointer <= rom_pointer + 1;
              else
                rom_pointer <= rom_pointer - 1;
              end if;
              
              mac_reg <= mac_out;
            end if;
            
          when mix =>
            if mix_rdy = '1' then
              state := idle;
            end if;
            
          when others =>
            state := idle;
        end case;
      end if;
    end process;

end syn;

