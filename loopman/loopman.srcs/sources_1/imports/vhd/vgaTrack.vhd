-- vgaTrack.vhd ------------------------------------------------------------------------------
--
-- Generates graphics for a recordable track.
--
-- Borja Morcillo Salgado - LoopMAN - NDC - fdi Madrid 2020
----------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity vgaTrack is
  generic(
    PLACE  : natural  -- position over origin (in pixels)
  );
  port (
    -- VGA
    line  : in   unsigned(9 downto 0); -- line being sweep   
    pixel : in   unsigned(9 downto 0); -- pixel being sweep  
    color : out  std_logic_vector(11 downto 0);
    -- Estado
    isOn    : in std_logic;
    isRec   : in std_logic;
    isMute  : in std_logic;
    isSolo  : in std_logic;
    isLen   : in std_logic_vector(4 downto 0);
    isBeat  : in std_logic_vector(4 downto 0);
    isVol   : in std_logic_vector(3 downto 0);
    isLevel : in std_logic_vector(3 downto 0);

    isSel   : in std_logic -- Wether track is selected
  );
end vgaTrack;

architecture syn of vgaTrack is

  signal border   : std_logic_vector(11 downto 0);
  signal volumen  : std_logic_vector(11 downto 0);
  signal loopLen  : std_logic_vector(11 downto 0);
  signal beat     : std_logic_vector(11 downto 0);
  signal salida   : std_logic_vector(11 downto 0);

  signal stOnC  : std_logic_vector(11 downto 0);
  signal recC   : std_logic_vector(11 downto 0);
  signal muteC  : std_logic_vector(11 downto 0);
  signal soloC  : std_logic_vector(11 downto 0);

  signal stOn  : std_logic_vector(11 downto 0);
  signal rec   : std_logic_vector(11 downto 0);
  signal mute  : std_logic_vector(11 downto 0);
  signal solo  : std_logic_vector(11 downto 0);

  signal pixel2 : unsigned(8 downto 0);
  signal line2  : unsigned(8 downto 0);

  signal auxVolHeight : unsigned(8 downto 0);
  signal auxLevel : unsigned(8 downto 0);

begin

  pixel2 <= pixel(9 downto 1);
  line2  <= line(9 downto 1);

  auxVolHeight <= '0' & unsigned(not isVol) & "0000"; -- (208+(7-unsigned(isVol))*32)
  auxLevel <= '0' & unsigned(not isLevel) & "1111";

  color <= border or volumen or loopLen or beat or stOnC or stOn or rec or recC or mute or muteC or solo or soloC or salida;


  border   <= "1111" & "1111" & "1111" when (((pixel = (PLACE + 8) or pixel = (PLACE + 56)) and (line >= 8 and line <= 472)) or ((line = 8 or line = 472) and pixel >= (PLACE+8) and pixel <= (PLACE+56))) and isSel = '1' else
              "1100" & "0100" & "0100" when (((pixel = (PLACE + 8) or pixel = (PLACE + 56)) and (line >= 8 and line <= 472)) or ((line = 8 or line = 472) and pixel >= (PLACE+8) and pixel <= (PLACE+56))) and isRec = '1' else
              "0100" & "1100" & "0100" when (((pixel = (PLACE + 8) or pixel = (PLACE + 56)) and (line >= 8 and line <= 472)) or ((line = 8 or line = 472) and pixel >= (PLACE+8) and pixel <= (PLACE+56))) and isOn  = '1' else
              "0100" & "0100" & "0100" when  ((pixel = (PLACE + 8) or pixel = (PLACE + 56)) and (line >= 8 and line <= 472)) or ((line = 8 or line = 472) and pixel >= (PLACE+8) and pixel <= (PLACE+56)) else
              "0000" & "0000" & "0000";

  volumen  <= "0000" & "1111" & "0111" when pixel > (PLACE + 32) and pixel < (PLACE + 52) and line < 464 and line > (208+auxVolHeight) and line(3) = '1' and isMute = '0' else
              "0000" & "0000" & "1011" when pixel > (PLACE + 32) and pixel < (PLACE + 52) and line < 464 and line > (208+auxVolHeight) and line(3) = '1' and isMute = '1' else
              "0000" & "0000" & "0000";

  loopLen  <= "0100" & "0100" & "1111" when (pixel > (PLACE + 12) and pixel < (PLACE + 28)) and (line < 464 and line > 208+(31-unsigned(isLen))*8) and line(2) = '1' else
              "0000" & "0000" & "0000";

  beat     <= "1111" & "0000" & "1111" when (pixel > (PLACE + 12) and pixel < (PLACE + 28)) and (line < 464 and line > 208+(31-unsigned(isBeat))*8) and line(2) = '1' else
              "0000" & "0000" & "0000";

  stOnC    <= "0000" & "1111" & "0000" when ((pixel2 = (PLACE + 12)/2 or pixel2 = (PLACE + 50)/2) and (line2 > 16/2 and line2 < (16+48)/2)) or ((line2 = 16/2 or line2 = (16+48)/2) and pixel2 > (PLACE + 12)/2 and pixel2 < (PLACE + 50)/2) else
              "0000" & "0000" & "0000";

  stOn     <= "0000" & "1011" & "0000" when (pixel > PLACE + 13 and pixel < PLACE + 51 and line > 16 and line < 16+48) and isOn = '1' else
              "0000" & "0000" & "0000";

  muteC    <= "0000" & "0000" & "1111" when ((pixel2 = (PLACE + 12)/2 or pixel2 = (PLACE + 30)/2) and (line2 > (64+16)/2 and line2 < (16+48+64)/2)) or ((line2 = (64+16)/2 or line2 = (16+48+64)/2) and pixel2 > (PLACE + 12)/2 and pixel2 < (PLACE + 30)/2) else
              "0000" & "0000" & "0000";
              
  soloC    <= "0000" & "1111" & "1111" when ((pixel2 = (PLACE + 32)/2 or pixel2 = (PLACE + 50)/2) and (line2 > (64+16)/2 and line2 < (16+48+64)/2)) or ((line2 = (64+16)/2 or line2 = (16+48+64)/2) and pixel2 > (PLACE + 32)/2 and pixel2 < (PLACE + 50)/2) else
              "0000" & "0000" & "0000";

  mute     <= "0000" & "0000" & "1011" when (pixel > PLACE + 13 and pixel < PLACE + 31 and line > (64+16) and line < (16+48+64)) and isMute = '1' else
              "0000" & "0000" & "0000";
              
  solo     <= "0000" & "1011" & "1011" when (pixel > PLACE + 33 and pixel < PLACE + 51 and line > (64+16) and line < (16+48+64)) and isSolo = '1' else
              "0000" & "0000" & "0000";

  recC     <= "1111" & "0000" & "0000" when ((pixel2 = (PLACE + 12)/2 or pixel2 = (PLACE + 50)/2) and (line2 > (128+16)/2 and line2 < (16+48+128)/2)) or ((line2 = (128+16)/2 or line2 = (16+48+128)/2) and pixel2 > (PLACE + 12)/2 and pixel2 < (PLACE + 50)/2) else
              "0000" & "0000" & "0000";

  rec      <= "1101" & "0000" & "0000" when (pixel > PLACE + 13 and pixel < PLACE + 51 and line > (128+16) and line < (16+48+128)) and isRec = '1' else
              "0000" & "0000" & "0000";

  salida   <= "1111" & "1000" & "0000" when (pixel > (PLACE + 34) and pixel < (PLACE + 50)) and line < 464 and line > (208+auxLevel) and line(1) = '1' else
              "0000" & "0000" & "0000";

end syn;

