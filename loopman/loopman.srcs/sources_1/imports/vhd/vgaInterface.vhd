-- vgaInterface.vhd --------------------------------------------------------------------------
--
-- Generates color and sync signals of a VGA interface with 640x420 resolution.
-- For frecuencies of 50 MHz and on in multiples of 25 MHz.
--
-- (c) J.M. Mendias
-- Dise�o Autom�tico de Sistemas
-- Facultad de Inform�tica. Universidad Complutense de Madrid
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity vgaInterface is
  generic(
    FREQ      : natural;  -- Operation frecuency
    SYNCDELAY : natural   -- Number of pixels to delay sync signals over position
  );
  port ( 
    -- host side
    rst   : in  std_logic;   -- async reset
    clk   : in  std_logic;   -- clock
    line  : out std_logic_vector(9 downto 0);   -- line being sweep
    pixel : out std_logic_vector(9 downto 0);   -- pixel being sweep
    R     : in  std_logic_vector(3 downto 0);   -- red pixel intensity
    G     : in  std_logic_vector(3 downto 0);   -- green pixel intensity
    B     : in  std_logic_vector(3 downto 0);   -- blue pixel intensity
    -- VGA side
    hSync : out std_logic;   -- horizontal sync
    vSync : out std_logic;   -- vertical sync
    RGB   : out std_logic_vector(11 downto 0)   -- color channels
  );
end vgaInterface;

---------------------------------------------------------------------

library ieee;
use ieee.numeric_std.all;

architecture syn of vgaInterface is
  
  constant CYCLESxPIXEL : natural  := FREQ/25_000;
  constant PIXELSxLINE  : natural  := 800; 
  constant LINESxFRAME  : natural  := 525;
  
  signal hSyncInt, vSyncInt : std_logic;

  signal pixelCnt : unsigned(pixel'range);
  signal lineCnt  : unsigned(line'range);
  signal blanking : boolean;
 
begin

  counters:
  process (rst, clk)
    variable cycleCnt : natural range 0 to CYCLESxPIXEL-1;
  begin
    if rising_edge(clk) then
      if rst='1' then
        cycleCnt := 0;
        pixelCnt <= to_unsigned(0, pixel'length);
        lineCnt <= to_unsigned(0, line'length);
      else
        if cycleCnt=CYCLESxPIXEL-1 then
          cycleCnt := 0;
          if pixelCnt=PIXELSxLINE-1 then
            pixelCnt <= to_unsigned(0, pixel'length);
            if lineCnt=LINESxFRAME-1 then
              lineCnt <= to_unsigned(0, line'length);
            else
              lineCnt <= lineCnt + 1;
            end if;
          else
            pixelCnt <= pixelCnt + 1;
          end if;
        else
          cycleCnt := cycleCnt + 1;
        end if;
      end if;
    end if;
  end process;

  pixel <= std_logic_vector(pixelCnt);
  line  <= std_logic_vector(lineCnt);
  
  hSyncInt <= '0' when ((pixelCnt >= 656+SYNCDELAY) and (pixelCnt < 752+SYNCDELAY)) else '1';
  vSyncInt <= '0' when ((lineCnt >= 494) and (lineCnt < 496)) else '1';
  
  blanking <= true when ((pixelCnt < SYNCDELAY) or (pixelCnt >= 640+SYNCDELAY) or (lineCnt >= 480)) else false;
   
  outputRegisters:
  process (rst, clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        hSync <= '0';
        vSync <= '0';
        RGB <= (others=>'0');
      else
        hSync <= hSyncInt;
        vSync <= vSyncInt;
        if not blanking then
          RGB(11 downto 8) <= R;
          RGB(7 downto 4) <= G;
          RGB(3 downto 0) <= B;
        end if;
      end if;
    end if;
  end process;
    
end syn;
