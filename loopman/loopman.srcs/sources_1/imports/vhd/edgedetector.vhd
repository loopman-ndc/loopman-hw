-- edgedetector.vhd --------------------------------------------------------------------------
--
-- Detects edges on slow binary input. Idle signal should be '1'.
--
-- (c) J.M. Mendias
-- Dise�o Autom�tico de Sistemas
-- Facultad de Inform�tica. Universidad Complutense de Madrid
----------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity edgeDetector is
  port (
    rst   : in  std_logic;   -- async reset
    clk   : in  std_logic;   -- clock
    x_n   : in  std_logic;   -- binary input with edges to detect (idle is lows)
    xFall : out std_logic;   -- activates for one cyvle when a rising edge is detected in x
    xRise : out std_logic    -- activates for one cyvle when a falling edge is detected in x
  );
end edgeDetector;

-------------------------------------------------------------------

architecture syn of edgeDetector is 
begin

  process (rst, clk)
    variable aux1, aux2: std_logic;
  begin
    xFall <= (not aux1) and aux2;
    xRise <= aux1 and (not aux2);
    if rst='1' then
      aux1 := '1';
      aux2 := '1';
    elsif rising_edge(clk) then
      aux2 := aux1;
      aux1 := x_n;           
    end if;
  end process;

end syn;
